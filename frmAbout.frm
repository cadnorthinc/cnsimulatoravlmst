VERSION 5.00
Begin VB.Form frmAbout_Old 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "About MyApp"
   ClientHeight    =   2595
   ClientLeft      =   7800
   ClientTop       =   7365
   ClientWidth     =   5070
   ClipControls    =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1784.527
   ScaleMode       =   0  'User
   ScaleWidth      =   4761.642
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox picIcon 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      Height          =   480
      Left            =   240
      Picture         =   "frmAbout.frx":0000
      ScaleHeight     =   323.838
      ScaleMode       =   0  'User
      ScaleWidth      =   323.838
      TabIndex        =   1
      Top             =   240
      Width           =   480
   End
   Begin VB.CommandButton cmdOK 
      Cancel          =   -1  'True
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   345
      Left            =   3708
      TabIndex        =   0
      Top             =   2124
      Width           =   1260
   End
   Begin VB.Label lblDescription 
      Caption         =   "App Description"
      ForeColor       =   &H00000000&
      Height          =   1140
      Left            =   1050
      TabIndex        =   2
      Top             =   960
      Width           =   3885
   End
   Begin VB.Label lblTitle 
      Caption         =   "Application Title"
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   1050
      TabIndex        =   3
      Top             =   240
      Width           =   3885
   End
   Begin VB.Label lblVersion 
      Caption         =   "Version"
      Height          =   225
      Left            =   1050
      TabIndex        =   4
      Top             =   600
      Width           =   3885
   End
End
Attribute VB_Name = "frmAbout_Old"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOK_Click()
  Unload frmAbout
End Sub

Private Sub Form_Load()

    frmAbout.Left = frmMain.Left + ((frmMain.Width - frmAbout.Width) / 2)
    frmAbout.Top = frmMain.Top + ((frmMain.Height - frmAbout.Height) / 2)

    Me.Caption = "About " & App.Title
    lblVersion.Caption = "Version " & App.Major & "." & App.Minor & " (build " & App.Revision & ")"
    lblTitle.Caption = App.Title
    frmAbout.lblDescription.Caption = "Copyright (c) 2001-" & Year(Now) & " Frozen Frog Software Inc." & vbCrLf & _
                                        "http://www.frozenfrog.com" & vbCrLf & vbCrLf & _
                                        "Modified for the University of Toronto" & vbCrLf & _
                                        "by Brimac Systems Inc. 2003"

End Sub

