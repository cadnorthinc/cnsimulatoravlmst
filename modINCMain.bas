Attribute VB_Name = "modINCMain"
Private Type tCADIncident
    Master_Incident_Number As String
    Confirmation_Number As String
    Response_Date As Variant
    Agency_Type As String
    AgencyTypeID As Long
    Jurisdiction As String
    Division As String
    Battalion As String
    Response_Area As String
    Response_Plan As String
    Incident_Type As String
    Problem As String
    Priority_Number As Integer
    Priority_Description As String
    Location_Name As String
    Address As String
    Apartment As String
    City As String
    State As String
    Postal_Code As String
    County As String
    Location_Type As String
    Latitude As Double
    Longitude As Double
    Map_Info As String
    Cross_Street As String
    MethodOfCallRcvd As String
    Caller_Name As String
    Time_PhonePickUp As String
    Fixed_Time_PhonePickUp As String
    Time_FirstCallTakingKeystroke As Variant
    Time_CallEnteredQueue As Variant
    Fixed_Time_CallEnteredQueue As Variant
    command_channel As String
    primary_tac_channel As String
    alternate_tac_channel As String
    Call_Is_Active As Integer
    Transfer_Return_Flag As Boolean
    SentToBilling_Flag As Boolean
    WhichQueue As String
    CallTaking_Performed_By As String
    Call_Back_Phone As String
    Call_Back_Phone_Ext As String
    Caller_Type As String
    Caller_Location_Name As String
    Building As String
    Elapsed_CallRcvd2InQueue As Variant
    Response_Time_Criteria As String
    Certification_Level As String
    ProQa_CaseNumber As String
    CIS_USED As Variant
    EMD_USED As Variant
    Determinant As String
    Street_ID As String
    num_aniali_calls As Variant
    ClockStartTime As Variant
    House_Number As String
    Prefix_Directional As String
    Name_Component As String
    Street_Type As String
    Post_Directional As String
    House_Number_Suffix As String
    Elapsed_CallRcvd2CalTakDone As Variant
    CreatedByPrescheduleModule As String
    Base_Response_Number As String
    Call_Disposition As String
    Call_Source As String
    HomeSectorID As Long
    CurrentSectorID As Long
    CurrentDivision As String
    ResponsePlanType As Variant
    DispatchLevel As String
End Type

Private Type tPreSchedule
    Master_Incident_ID As Long
    PreScheduled_ID As Long
    Time_CallTaken As Variant
    Time_PickupPromised As Variant
    Time_PickupRequested As Variant
    CallTaking_Performed_By As String
    Time_Advanced_Sched As Variant
    Time_Appointment As Variant
    Physician As String
    ReferringPhysician As String
    Special_Equipment As String
    Need_IV As Integer
    Need_EKG As Integer
    Need_Oxygen As Integer
    Need_Family_Rider As Integer
    Need_Paramedic As Integer
    Need_WheelChair As Integer
    Need_Restraint As Integer
    Need_Suction As Integer
    Authorization_Number As String
    Name_First As String
    Name_Last As String
    Name_MI As String
    Call_Status As String
    Patient_Info_ID As Long
    Call_Sequence As Integer
    Return_Master_Incident_ID As Long
    Escort As String
End Type

Private Type tTransport
    Master_Incident_ID As Long
    Sequence_Number As Integer
    Patient_Info_ID As Long
    Location_Name As String
    Address As String
    Address2 As String
    Apartment As String
    City As String
    State As String
    Postal_Code As String
    County As String
    Location_Type As String
    Longitude As Double
    Latitude As Double
    Phone As String
    Map_Info As String
    Building As String
    Pickup_Address As String
    Pickup_Address2 As String
    Pickup_Apartment As String
    Pickup_City As String
    Pickup_State As String
    Pickup_Postal_Code As String
    Pickup_County As String
    Pickup_Location_Type As String
    Pickup_Longitude As Double
    Pickup_Latitude As Double
    Pickup_Phone As String
    Pickup_Map_Info As String
    Pickup_Building As String
    Time_PickupPromised As Variant
    Time_PickupRequested As Variant
    Time_Appointment As Variant
    Pickup_Location_Name As String
    Phone_Ext As String
    Pickup_Phone_Ext As String
    Trip_Type As String
    Name_First As String
    Name_Last As String
    Name_MI As String
    Escort As String
End Type

Private Type tActivity
    Date_Time As Variant
    Activity As String
    Location As String
    Comment As String
    Latitude As Long
    Longitude As Long
    Radio_Name As String
    Dispatcher_Init As String
    Agency_ID As Integer
    Jurisdiction_ID As Integer
    Division_ID As Integer
    Battalion_ID As Integer
    Station_ID As Integer
    Terminal As Integer
    Radio_Code As Integer
    Master_Incident_ID As Long
    Vehicle_ID As Integer
    Personnel_ID As Integer
End Type

Private Type tANIALI
    Master_Incident_ID As Long
    PhoneNumber As String
    TimeCallReceived As Variant
    HouseNumber As Integer
    Direction As String
    Address As String
    City As String
    State As String
    ESN As String
    ESNText As String
    ClassOfService As String
    CustomerName As String
    PilotPhone As String
    LocationInfo As String
    OriginalTextMessage As Variant
    PSAP As String
    JobNumber As String
    AptNumber As String
    Building As String
    Longitude As Double
    Latitude As Double
    Altitude As Long
    Caller_Type As String
    LinkID As Long
End Type

Public Const NP_READ_VEHICLE = 20
Public Const NP_READ_RESPONSE_MASTER_INCIDENT = 37
Public Const NP_READ_RESPONSE_VEHICLESASSIGN = 45
Public Const NP_READ_RESPONSE_TRANSPORT = 46
Public Const NP_READ_RESPONSE_TRANSPORTLEGS = 47
Public Const NP_READ_MULTI_ASSIGN_VEHICLE = 234
Public Const NP_NEW_RESPONSE = 35
Public Const NP_READ_RESPONSE_LATE = 122
Public Const NP_XML244 = 244
Public Const NP_XML245 = 245
Public Const NP_READ_RESPONSE_COMMENTS = 66
Public Const NP_ADD_DISPLAY_COMMENT_NOTE = 155
Public Const NP_READ_ANIALI = 3001
Public Const NP_BRIMAC_MESSAGE_CLASS = 9750

Public Const CN_CADNORTH_IPC_REQUEST = 9710
Public Const CN_ADD_COMMENT_TO_INC = 66

Public Const NP_READ_RESPONSE_FIELDS = 123                          '   new with ~v4.5
Public Const NP_READ_RESPONSE_EDIT = 125                            '   new with ~v4.5
Public Const NP_CHANGED_RESPONSE_PROBLEMNATURECHANGE = 236          '   new with ~v4.5
Public Const NP_READ_RESPONSE_CHANGED_RESPONSEPLAN = 330            '   new with ~v4.5

Public Const NP_BSI_OLDINC_NEWINC_XREF = 1
Public Const NP_BSI_INITIALIZATION_COMPLETE = 2

Public Const K_CHR_EOT = 4          '   marker for start of XML portion of message

Option Explicit

Function INCMain(aIncID As Variant) As Boolean
    Dim nOldIncID As Long
    Dim nNewIncID As Long
    Dim lPreSched As Boolean
    Dim lExists As Boolean
    Dim tCADInc As tCADIncident
    
    On Error GoTo ErrHandler
    
    nOldIncID = aIncID(1)
    
    '   Create a duplicate of a selected RMI.ID
    
    tCADInc = GetCADIncident(nOldIncID, lExists, lPreSched)
    
    If lExists Then
        
        nNewIncID = InsertIncident(tCADInc, nOldIncID, Now, lPreSched)
        
        '   Update Caution Notes and Premise History Flags
        
        Call UpdatePremiseCaution(nNewIncID)
        
        If Not lPreSched Then
            '   Create new RMI Incident Number for new incident
            '   Call NewCreateIncNumberBySP(nNewIncID, tCADInc)
            Call NewCreateFormattedNumberBySP("I", nNewIncID, tCADInc.Jurisdiction)
            '   Call CreateIncNumber(nNewIncID, tCadInc)
        Else
            '   Create Confirmation Number for new incident
            Call CreateConfirmationNumber(nNewIncID, tCADInc)
            '   Copy presched info, destination information (if any)
            Call CopyPrescheduledInformation(nNewIncID, nOldIncID)
            Call CopyResponseTransports(nNewIncID, nOldIncID)
            '   Set Activation Trigger
            '   Call ActivateCADMon(nNewIncID)
        End If
        
        '   Copy comments (if any)
        
        Call CopyIncidentComments(nNewIncID, nOldIncID)
        
        '   We need to add USP_CADMonMarkWaitingCallInQueue    @MasterIncidentID, '1'
        '   We need to add USP_CADMonSchedWaitingCallInQueueWarning    @MasterIncidentID
        '   We need to add USP_CADMonSchedWaitingCallLate    @MasterIncidentID
        
        Call CADMonMarkWaitingCallInQueue(nNewIncID)
        Call CADMonSchedWaitingCallInQueueWarning(nNewIncID)
        Call CADMonSchedWaitingCallLate(nNewIncID)
        
        '   Now tell Simulator the old vs. new incident numbers
        
        If gUseXMLPort Then
            Call goCADServer.SendMessageXML(NP_BRIMAC_MESSAGE_CLASS, Format(NP_BSI_OLDINC_NEWINC_XREF, "0000") & "|" _
                                                & Trim(Str(nOldIncID)) & "|" & Trim(Str(nNewIncID)), False, False)
            
            '   Now tell everyone that the new incident is ready for viewing
            
            '    Call goCADServer.SendMessageXML(NP_READ_RESPONSE_MASTER_INCIDENT, Trim(Str(nNewIncID)), False, False)
            '    Call goCADServer.SendMessageXML(NP_READ_RESPONSE_COMMENTS, Trim(Str(nNewIncID)), False, False)
            '    Call goCADServer.SendMessageXML(NP_ADD_DISPLAY_COMMENT_NOTE, Trim(Str(nNewIncID)), False, False)
            '    Call goCADServer.SendMessageXML(NP_NEW_RESPONSE, Trim(Str(nNewIncID)), False, False)
            Call goCADServer.SendMessageXML(NP_NEW_RESPONSE, Trim(Str(nNewIncID)), False, False)
            Call goCADServer.SendMessageXML(NP_READ_RESPONSE_EDIT, Trim(Str(nNewIncID)), False, False)
            '   Call goCADServer.SendMessageXML(NP_READ_RESPONSE_FIELDS, FormatXML_NP_READ_RESPONSE_FIELDS(nNewIncID, True), False, False)
            '   Call goCADServer.SendMessage(NP_READ_RESPONSE_FIELDS, Trim(Str(nNewIncID), True) & ",Problem;" & Trim(Str(nNewIncID)) & ",Response_Master_Incident,Response_Plan,ResponsePlanType,Priority_Description,Priority_Number", False, False)
            Call goCADServer.SendMessageXML(NP_READ_RESPONSE_EDIT, Trim(Str(nNewIncID)), False, False)
            Call goCADServer.SendMessageXML(NP_READ_RESPONSE_CHANGED_RESPONSEPLAN, Trim(Str(nNewIncID)), False, False)
            Call goCADServer.SendMessageXML(NP_CHANGED_RESPONSE_PROBLEMNATURECHANGE, Trim(Str(nNewIncID)), False, False)
        Else
            Call goCADServer.SendMessage(NP_BRIMAC_MESSAGE_CLASS, Format(NP_BSI_OLDINC_NEWINC_XREF, "0000") & "|" _
                                                & Trim(Str(nOldIncID)) & "|" & Trim(Str(nNewIncID)), False, False)
            
            '   Now tell everyone that the new incident is readay for viewing
            
            '    Call goCADServer.SendMessage(NP_READ_RESPONSE_MASTER_INCIDENT, Trim(Str(nNewIncID)), False, False)
            '    Call goCADServer.SendMessage(NP_READ_RESPONSE_COMMENTS, Trim(Str(nNewIncID)), False, False)
            '    Call goCADServer.SendMessage(NP_ADD_DISPLAY_COMMENT_NOTE, Trim(Str(nNewIncID)), False, False)
            '    Call goCADServer.SendMessage(NP_NEW_RESPONSE, Trim(Str(nNewIncID)), False, False)
            Call goCADServer.SendMessage(NP_NEW_RESPONSE, Trim(Str(nNewIncID)), False, False)
            Call goCADServer.SendMessage(NP_READ_RESPONSE_EDIT, Trim(Str(nNewIncID)), False, False)
            '   Call goCADServer.SendMessage(NP_READ_RESPONSE_FIELDS, Trim(Str(nNewIncID), True) & ",Problem;" & Trim(Str(nNewIncID)) & ",Response_Master_Incident,Response_Plan,ResponsePlanType,Priority_Description,Priority_Number", False, False)
            Call goCADServer.SendMessage(NP_READ_RESPONSE_EDIT, Trim(Str(nNewIncID)), False, False)
            Call goCADServer.SendMessage(NP_READ_RESPONSE_CHANGED_RESPONSEPLAN, Trim(Str(nNewIncID)), False, False)
            Call goCADServer.SendMessage(NP_CHANGED_RESPONSE_PROBLEMNATURECHANGE, Trim(Str(nNewIncID)), False, False)
        End If
        
        Call AVLDebug("INCMain: Retrieving ID " & Str(nOldIncID) & ". Requested Incident created.", True)
    
    Else
    
        Call AVLDebug("INCMain: Error Retrieving ID " & Str(nOldIncID) & ". Requested Incident NOT created.", True)
        
    End If
    
    Exit Function
    
ErrHandler:
    Call AVLDebug("ERROR in INCMain: nOldIncID =" & Str(nOldIncID) & " nNewIncID =" & Str(nNewIncID), True)
    Call AVLDebug("ERROR: " & Str(Err.Number) & " " & Err.Description, True)
End Function

Function GetCADIncident(nOldIncID As Long, ByRef lIncidentExists As Boolean, Optional PreScheduled As Boolean = False) As tCADIncident
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim tOldInc As tCADIncident
    Dim nError As Integer
    
    On Error GoTo ErrorHandler
    
    cSQL = "Select Master_Incident_Number, Confirmation_Number, Response_Date, Agency_Type, " _
            & " Jurisdiction, Division, Battalion, Response_Area, Response_Plan, Incident_Type, " _
            & " Problem, Priority_Number, Priority_Description, Location_Name, Address, Apartment, City, " _
            & " State, Postal_Code, County, Location_Type, Latitude, Longitude, Map_Info, Cross_Street, " _
            & " MethodOfCallRcvd, Caller_Name, Time_PhonePickUp, Fixed_Time_PhonePickUp, " _
            & " Time_FirstCallTakingKeystroke, Time_CallEnteredQueue, Fixed_Time_CallEnteredQueue, command_channel, " _
            & " primary_tac_channel, alternate_tac_channel, Call_Is_Active, Transfer_Return_Flag, SentToBilling_Flag, " _
            & " WhichQueue, CallTaking_Performed_By, Call_Back_Phone, Call_Back_Phone_Ext, Caller_Type, " _
            & " Caller_Location_Name, Building, Elapsed_CallRcvd2InQueue, Response_Time_Criteria, " _
            & " Certification_Level, proqa_casenumber, CIS_USED, EMD_USED, determinant, Street_ID, " _
            & " num_aniali_calls, ClockStartTime, House_Number, Prefix_Directional, Name_Component, " _
            & " Street_Type, Post_Directional, House_Number_Suffix, " _
            & " Elapsed_CallRcvd2CalTakDone, CreatedByPrescheduleModule, Base_Response_Number, " _
            & " Call_Disposition, Call_Source, CurrentDivision, ResponsePlanType "
            
    If glIsNewFOURVersion Then
        cSQL = cSQL & ", DispatchLevel "
    End If
    
    If glIsCD Then
        cSQL = cSQL & ", HomeSectorID, CurrentSectorID "
        Call AVLDebug("GetCADIncident: Getting HomeSectorID and CurrentSectorID", gDetailedDebugging)
    Else
        Call AVLDebug("GetCADIncident: NOT Getting HomeSectorID and CurrentSectorID", gDetailedDebugging)
    End If
    
    cSQL = cSQL & " FROM Response_Master_Incident " _
            & " WHERE ID = " & Str(nOldIncID)

    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
        nError = 0
        
        lIncidentExists = True
        
        tOldInc.Master_Incident_Number = IIf(IsNull(aSQL(0, 0)), "", aSQL(0, 0))
        nError = nError + 1
        tOldInc.Confirmation_Number = IIf(IsNull(aSQL(1, 0)), "", aSQL(1, 0))
        nError = nError + 1
        tOldInc.Response_Date = IIf(IsNull(aSQL(2, 0)), "", aSQL(2, 0))
        nError = nError + 1
        tOldInc.Agency_Type = IIf(IsNull(aSQL(3, 0)), "", aSQL(3, 0))
        nError = nError + 1
        tOldInc.Jurisdiction = IIf(IsNull(aSQL(4, 0)), "", aSQL(4, 0))
        nError = nError + 1
        tOldInc.Division = IIf(IsNull(aSQL(5, 0)), "", aSQL(5, 0))
        nError = nError + 1
        tOldInc.Battalion = IIf(IsNull(aSQL(6, 0)), "", aSQL(6, 0))
        nError = nError + 1
        tOldInc.Response_Area = IIf(IsNull(aSQL(7, 0)), "", aSQL(7, 0))
        nError = nError + 1
        tOldInc.Response_Plan = IIf(IsNull(aSQL(8, 0)), "", aSQL(8, 0))
        nError = nError + 1
        tOldInc.Incident_Type = IIf(IsNull(aSQL(9, 0)), "", aSQL(9, 0))
        nError = nError + 1
        tOldInc.Problem = IIf(IsNull(aSQL(10, 0)), "", aSQL(10, 0))
        nError = nError + 1
        tOldInc.Priority_Number = IIf(IsNull(aSQL(11, 0)), 0, CInt(aSQL(11, 0)))
        nError = nError + 1
        tOldInc.Priority_Description = IIf(IsNull(aSQL(12, 0)), "", aSQL(12, 0))
        nError = nError + 1
        tOldInc.Location_Name = IIf(IsNull(aSQL(13, 0)), "", aSQL(13, 0))
        nError = nError + 1
        tOldInc.Address = IIf(IsNull(aSQL(14, 0)), "", aSQL(14, 0))
        nError = nError + 1
        tOldInc.Apartment = IIf(IsNull(aSQL(15, 0)), "", aSQL(15, 0))
        nError = nError + 1
        tOldInc.City = IIf(IsNull(aSQL(16, 0)), "", aSQL(16, 0))
        nError = nError + 1
        tOldInc.State = IIf(IsNull(aSQL(17, 0)), "", aSQL(17, 0))
        nError = nError + 1
        tOldInc.Postal_Code = IIf(IsNull(aSQL(18, 0)), "", aSQL(18, 0))
        nError = nError + 1
        tOldInc.County = IIf(IsNull(aSQL(19, 0)), "", aSQL(19, 0))
        nError = nError + 1
        tOldInc.Location_Type = IIf(IsNull(aSQL(20, 0)), "", aSQL(20, 0))
        nError = nError + 1
        tOldInc.Latitude = IIf(IsNull(aSQL(21, 0)), "", aSQL(21, 0))
        nError = nError + 1
        tOldInc.Longitude = IIf(IsNull(aSQL(22, 0)), "", aSQL(22, 0))
        nError = nError + 1
        tOldInc.Map_Info = IIf(IsNull(aSQL(23, 0)), "", aSQL(23, 0))
        nError = nError + 1
        tOldInc.Cross_Street = IIf(IsNull(aSQL(24, 0)), "", aSQL(24, 0))
        nError = nError + 1
        tOldInc.MethodOfCallRcvd = IIf(IsNull(aSQL(25, 0)), "", aSQL(25, 0))
        nError = nError + 1
        tOldInc.Caller_Name = IIf(IsNull(aSQL(26, 0)), "", aSQL(26, 0))
        nError = nError + 1
        tOldInc.Time_PhonePickUp = IIf(IsNull(aSQL(27, 0)), "", aSQL(27, 0))
        nError = nError + 1
        tOldInc.Fixed_Time_PhonePickUp = IIf(IsNull(aSQL(28, 0)), "", aSQL(28, 0))
        nError = nError + 1
        tOldInc.Time_FirstCallTakingKeystroke = IIf(IsNull(aSQL(29, 0)), "", aSQL(29, 0))
        nError = nError + 1
        tOldInc.Time_CallEnteredQueue = IIf(IsNull(aSQL(30, 0)), "", aSQL(30, 0))
        nError = nError + 1
        tOldInc.Fixed_Time_CallEnteredQueue = IIf(IsNull(aSQL(31, 0)), "", aSQL(31, 0))
        nError = nError + 1
        tOldInc.command_channel = IIf(IsNull(aSQL(32, 0)), "", aSQL(32, 0))
        nError = nError + 1
        tOldInc.primary_tac_channel = IIf(IsNull(aSQL(33, 0)), "", aSQL(33, 0))
        nError = nError + 1
        tOldInc.alternate_tac_channel = IIf(IsNull(aSQL(34, 0)), "", aSQL(34, 0))
        nError = nError + 1
        tOldInc.Call_Is_Active = IIf(IsNull(aSQL(35, 0)), "", aSQL(35, 0))
        nError = nError + 1
        tOldInc.Transfer_Return_Flag = IIf(IsNull(aSQL(36, 0)), "", aSQL(36, 0))
        nError = nError + 1
        tOldInc.SentToBilling_Flag = IIf(IsNull(aSQL(37, 0)), "", aSQL(37, 0))
        nError = nError + 1
        tOldInc.WhichQueue = IIf(IsNull(aSQL(38, 0)), "", aSQL(38, 0))
        nError = nError + 1
        tOldInc.CallTaking_Performed_By = IIf(IsNull(aSQL(39, 0)), "", aSQL(39, 0))
        nError = nError + 1
        tOldInc.Call_Back_Phone = IIf(IsNull(aSQL(40, 0)), "", aSQL(40, 0))
        nError = nError + 1
        tOldInc.Call_Back_Phone_Ext = IIf(IsNull(aSQL(41, 0)), "", aSQL(41, 0))
        nError = nError + 1
        tOldInc.Caller_Type = IIf(IsNull(aSQL(42, 0)), "", aSQL(42, 0))
        nError = nError + 1
        tOldInc.Caller_Location_Name = IIf(IsNull(aSQL(43, 0)), "", aSQL(43, 0))
        nError = nError + 1
        tOldInc.Building = IIf(IsNull(aSQL(44, 0)), "", aSQL(44, 0))
        nError = nError + 1
        tOldInc.Elapsed_CallRcvd2InQueue = IIf(IsNull(aSQL(45, 0)), "", aSQL(45, 0))
        nError = nError + 1
        tOldInc.Response_Time_Criteria = IIf(IsNull(aSQL(46, 0)), "", aSQL(46, 0))
        nError = nError + 1
        tOldInc.Certification_Level = IIf(IsNull(aSQL(47, 0)), "", aSQL(47, 0))
        nError = nError + 1
        tOldInc.ProQa_CaseNumber = IIf(IsNull(aSQL(48, 0)), "", aSQL(48, 0))
        nError = nError + 1
        tOldInc.CIS_USED = IIf(IsNull(aSQL(49, 0)), "", aSQL(49, 0))
        nError = nError + 1
        tOldInc.EMD_USED = IIf(IsNull(aSQL(50, 0)), "", aSQL(50, 0))
        nError = nError + 1
        tOldInc.Determinant = IIf(IsNull(aSQL(51, 0)), "", aSQL(51, 0))
        nError = nError + 1
        tOldInc.Street_ID = IIf(IsNull(aSQL(52, 0)), "", aSQL(52, 0))
        nError = nError + 1
        tOldInc.num_aniali_calls = IIf(IsNull(aSQL(53, 0)), "", aSQL(53, 0))
        nError = nError + 1
        tOldInc.ClockStartTime = IIf(IsNull(aSQL(54, 0)), "", aSQL(54, 0))
        nError = nError + 1
        tOldInc.House_Number = IIf(IsNull(aSQL(55, 0)), "", aSQL(55, 0))
        nError = nError + 1
        tOldInc.Prefix_Directional = IIf(IsNull(aSQL(56, 0)), "", aSQL(56, 0))
        nError = nError + 1
        tOldInc.Name_Component = IIf(IsNull(aSQL(57, 0)), "", aSQL(57, 0))
        nError = nError + 1
        tOldInc.Street_Type = IIf(IsNull(aSQL(58, 0)), "", aSQL(58, 0))
        nError = nError + 1
        tOldInc.Post_Directional = IIf(IsNull(aSQL(59, 0)), "", aSQL(59, 0))
        nError = nError + 1
        tOldInc.House_Number_Suffix = IIf(IsNull(aSQL(60, 0)), "", aSQL(60, 0))
        nError = nError + 1
        tOldInc.Elapsed_CallRcvd2CalTakDone = IIf(IsNull(aSQL(61, 0)), "", aSQL(61, 0))
        nError = nError + 1
        tOldInc.CreatedByPrescheduleModule = IIf(IsNull(aSQL(62, 0)), "N", aSQL(62, 0))
        nError = nError + 1
        tOldInc.Base_Response_Number = IIf(IsNull(aSQL(63, 0)), "", aSQL(63, 0))
        nError = nError + 1
        tOldInc.Call_Disposition = IIf(IsNull(aSQL(64, 0)), "", aSQL(64, 0))
        nError = nError + 1
        tOldInc.Call_Source = IIf(IsNull(aSQL(65, 0)), "", aSQL(65, 0))
        nError = nError + 1
        tOldInc.CurrentDivision = IIf(IsNull(aSQL(66, 0)), "", aSQL(66, 0))
        nError = nError + 1
        tOldInc.ResponsePlanType = IIf(IsNull(aSQL(67, 0)), 0, aSQL(67, 0))
        
        Call AVLDebug("NOTE: Base Copy Complete", gDetailedDebugging)
        
        If glIsNewFOURVersion Then
            nError = nError + 1
            tOldInc.DispatchLevel = IIf(IsNull(aSQL(68, 0)), "Default", aSQL(68, 0))
            If glIsCD Then
'                tOldInc.HomeSectorID = IIf(IsNumeric(aSQL(69, 0)), aSQL(69, 0), 0)
'                tOldInc.CurrentSectorID = IIf(IsNumeric(aSQL(70, 0)), aSQL(70, 0), 0)
                nError = nError + 1
                tOldInc.HomeSectorID = IIf(IsNull(aSQL(69, 0)), 0, Val(aSQL(69, 0)))
                nError = nError + 1
                tOldInc.CurrentSectorID = IIf(IsNull(aSQL(70, 0)), 0, Val(aSQL(70, 0)))
                Call AVLDebug("NOTE: Copy Sector Information", gDetailedDebugging)
            Else
                Call AVLDebug("NOTE: Skipping Sector Copy", gDetailedDebugging)
            End If
        Else
            If glIsCD Then
'                tOldInc.HomeSectorID = IIf(IsNumeric(aSQL(68, 0)), aSQL(68, 0), 0)
'                tOldInc.CurrentSectorID = IIf(IsNumeric(aSQL(69, 0)), aSQL(69, 0), 0)
                nError = nError + 1
                tOldInc.HomeSectorID = IIf(IsNull(aSQL(69, 0)), 0, Val(aSQL(69, 0)))
                nError = nError + 1
                tOldInc.CurrentSectorID = IIf(IsNull(aSQL(70, 0)), 0, Val(aSQL(70, 0)))
                Call AVLDebug("NOTE: Copy Sector Information", gDetailedDebugging)
            Else
                Call AVLDebug("NOTE: Skipping Sector Copy", gDetailedDebugging)
            End If
        End If
        
        If tOldInc.CreatedByPrescheduleModule = "Y" Then
            PreScheduled = True
            Call AVLDebug("Function GetCADIncident: PreScheduled Incident retrieved:" & Str(nOldIncID), gDetailedDebugging)
        Else
            PreScheduled = False
            Call AVLDebug("Function GetCADIncident: Regular Incident retrieved:" & Str(nOldIncID), gDetailedDebugging)
        End If
    
        Call AVLDebug("GetCADIncident: Getting Agency_Type ID", gDetailedDebugging)
        cSQL = "Select ID from AgencyTypes where Agency_Type = '" & tOldInc.Agency_Type & "' "
        
        nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
        
        If nRows > 0 Then
            nError = nError + 1
            tOldInc.AgencyTypeID = aSQL(0, 0)
        End If
        
    Else
        lIncidentExists = False
        Call AVLDebug("ERROR - Function GetCADIncident: Incident NOT retrieved:" & Str(nOldIncID), gDetailedDebugging)
    End If
    
    GetCADIncident = tOldInc
    
    Exit Function

ErrorHandler:
    lIncidentExists = False
    Call AVLDebug("ERROR - Function GetCADIncident (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description & " @ FIELD #" & Str(nError) & vbCrLf & "  > " & cSQL, True)
End Function

Function InsertIncident(tOldInc As tCADIncident, nOldIncID As Long, StartTime As Variant, Optional lPreSched As Boolean = False) As Long
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim nNewIncID As Long
    
    On Error GoTo ErrorHandler
    
    '   Insert fields from the selected record into a new incident record
    
    cSQL = "BEGIN TRANSACTION "
    cSQL = cSQL & "INSERT INTO RESPONSE_MASTER_INCIDENT "
    cSQL = cSQL & "(Master_Incident_Number, Confirmation_Number, Response_Date, Agency_Type, " _
                & " Jurisdiction, Division, Battalion, Response_Area, Response_Plan, Incident_Type, " _
                & " Problem, Priority_Number, Priority_Description, Location_Name, Address, Apartment, City, " _
                & " State, Postal_Code, County, Location_Type, Latitude, Longitude, Map_Info, Cross_Street, " _
                & " MethodOfCallRcvd, Caller_Name, Time_PhonePickUp, Fixed_Time_PhonePickUp, " _
                & " Time_FirstCallTakingKeystroke, Time_CallEnteredQueue, Time_CallTakingComplete, Fixed_Time_CallEnteredQueue, command_channel, " _
                & " primary_tac_channel, alternate_tac_channel, Call_Is_Active, Transfer_Return_Flag, SentToBilling_Flag, " _
                & " WhichQueue, CallTaking_Performed_By, Call_Back_Phone, Call_Back_Phone_Ext, Caller_Type, " _
                & " Caller_Location_Name, Building, Elapsed_CallRcvd2InQueue, Elapsed_CallRcvd2CalTakDone, Response_Time_Criteria, " _
                & " Certification_Level, proqa_casenumber, CIS_USED, EMD_USED, determinant, Street_ID, " _
                & " num_aniali_calls, ClockStartTime, House_Number, Prefix_Directional, Name_Component, " _
                & " Street_Type, Post_Directional, House_Number_Suffix, Fixed_Time_CallTakingComplete, " _
                & " CreatedByPrescheduleModule, Base_Response_Number, " _
                & " CurrentDivision, ResponsePlanType "
                
    If glIsNewFOURVersion Then
        cSQL = cSQL & ", DispatchLevel "
    End If
    
    If glIsCD Then
        cSQL = cSQL & ", HomeSectorID, CurrentSectorID) "
    Else
        cSQL = cSQL & " ) "
    End If
    
    cSQL = cSQL & " Values ('', " _
                & " '', " _
                & " '" & Format(DateAdd("S", -45, StartTime), "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & tOldInc.Agency_Type & "', " _
                & " '" & tOldInc.Jurisdiction & "', " _
                & " '" & tOldInc.Division & "', " _
                & " '" & tOldInc.Battalion & "', " _
                & " '" & tOldInc.Response_Area & "', " _
                & " '" & tOldInc.Response_Plan & "', " _
                & " '" & tOldInc.Incident_Type & "', " _
                & " '" & bsiCleanString(tOldInc.Problem) & "', " _
                & " '" & tOldInc.Priority_Number & "', " _
                & " '" & bsiCleanString(tOldInc.Priority_Description) & "', " _
                & " '" & bsiCleanString(tOldInc.Location_Name) & "', " _
                & " '" & bsiCleanString(tOldInc.Address) & "', " _
                & " '" & tOldInc.Apartment & "', " _
                & " '" & bsiCleanString(tOldInc.City) & "', " _
                & " '" & tOldInc.State & "', " _
                & " '" & tOldInc.Postal_Code & "', " _
                & " '" & bsiCleanString(tOldInc.County) & "', " _
                & " '" & bsiCleanString(tOldInc.Location_Type) & "', " _
                & " '" & tOldInc.Latitude & "', " _
                & " '" & tOldInc.Longitude & "', " _
                & " '" & tOldInc.Map_Info & "', " _
                & " '" & bsiCleanString(tOldInc.Cross_Street) & "', "
    cSQL = cSQL & " '" & bsiCleanString(tOldInc.MethodOfCallRcvd) & "', " _
                & " '" & bsiCleanString(tOldInc.Caller_Name) & "', " _
                & " '" & Format(DateAdd("S", -45, StartTime), "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & Format(DateAdd("S", -45, StartTime), "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & Format(DateAdd("S", -45, StartTime), "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & Format(StartTime, "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & Format(StartTime, "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & Format(StartTime, "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & tOldInc.command_channel & "', " _
                & " '" & tOldInc.primary_tac_channel & "', " _
                & " '" & tOldInc.alternate_tac_channel & "', " _
                & " '1', " _
                & IIf(tOldInc.Transfer_Return_Flag, 1, 0) & ", " _
                & IIf(tOldInc.SentToBilling_Flag, 1, 0) & ", " _
                & " '" & IIf(lPreSched, "W", "W") & "', " _
                & " 'Simulator', " _
                & " '" & tOldInc.Call_Back_Phone & "', " _
                & " '" & tOldInc.Call_Back_Phone_Ext & "', " _
                & " '" & tOldInc.Caller_Type & "', " _
                & " '" & bsiCleanString(tOldInc.Caller_Location_Name) & "', " _
                & " '" & tOldInc.Building & "', "
    cSQL = cSQL & " '00:00:45', " _
                & " '00:00:45', " _
                & " '00:00:00', " _
                & " '" & tOldInc.Certification_Level & "', " _
                & " '" & tOldInc.ProQa_CaseNumber & "', " _
                & " '" & tOldInc.CIS_USED & "', " _
                & " '" & tOldInc.EMD_USED & "', " _
                & " '" & tOldInc.Determinant & "', " _
                & " '" & tOldInc.Street_ID & "', " _
                & " '" & tOldInc.num_aniali_calls & "', " _
                & " '" & Format(DateAdd("S", -45, StartTime), "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & tOldInc.House_Number & "', " _
                & " '" & tOldInc.Prefix_Directional & "', " _
                & " '" & bsiCleanString(tOldInc.Name_Component) & "', " _
                & " '" & tOldInc.Street_Type & "', " _
                & " '" & tOldInc.Post_Directional & "', " _
                & " '" & tOldInc.House_Number_Suffix & "', " _
                & " '" & Format(StartTime, "MMM DD YYYY HH:NN:SS") & "', " _
                & IIf(tOldInc.CreatedByPrescheduleModule = "", "NULL", "'" & tOldInc.CreatedByPrescheduleModule & "'") & ", " _
                & " '" & tOldInc.Base_Response_Number & "', " _
                & " '" & bsiCleanString(tOldInc.CurrentDivision) & "', " _
                & IIf(tOldInc.ResponsePlanType = "", "NULL", tOldInc.ResponsePlanType)              '   fixed SQL error when ResponsePlanType was NULL string   BM 2017.05.02
                
    If glIsNewFOURVersion Then
        cSQL = cSQL & ", '" & bsiCleanString(tOldInc.DispatchLevel) & "'"
    End If
    
    If glIsCD Then
        cSQL = cSQL & ", " & Str(tOldInc.HomeSectorID) & ", " _
                    & Str(tOldInc.CurrentSectorID) & ") "
    Else
        cSQL = cSQL & " ) "
    End If
    
    cSQL = cSQL & " Select @@IDENTITY x IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION "

    nRows = FFADOPut(cSQL, aSQL, nNewIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    If nRows > 0 Then
        Call AVLDebug("Function InsertIncident: Incident Inserted:" & Str(nNewIncID), gDetailedDebugging)
    Else
        Call AVLDebug("ERROR - Function InsertIncident: Incident NOT Inserted from original incident:" & Str(nOldIncID), gDetailedDebugging)
    End If
    
    cSQL = "BEGIN TRANSACTION " _
            & " UPDATE Response_Master_Incident " _
            & " SET MultiAgency_Ptr = " & Str(nNewIncID) _
            & " WHERE ID = " & Str(nNewIncID) & " IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
    
    nRows = FFADOPut(cSQL, aSQL, nNewIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    InsertIncident = nNewIncID
    
    Call AVLDebug("Function InsertIncident: Incident Updated:" & Str(nNewIncID), gDetailedDebugging)
    
    Exit Function

ErrorHandler:
    Call AVLDebug("ERROR - Function InsertIncident: cSQL = " & cSQL, True)
    Call AVLDebug("ERROR - Function InsertIncident (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
End Function

Function UpdatePremiseCaution(nIncID As Long) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim nNewIncID As Long
    Dim nPremiseID As Long
    Dim lUnreadInc As Boolean
    Dim lCaution As Boolean
    Dim lPremiseHist As Boolean
    Dim AddressField As String
    Dim StreetName As String
    Dim StreetNumber As String
    
    On Error GoTo ErrorHandler
    
    Call AVLDebug("Function UpdatePremiseCaution: Begin Process:" & Str(nIncID), gDetailedDebugging)
    
    lUnreadInc = False
    lCaution = False
    lPremiseHist = False
    
    cSQL = "SELECT Address, Apartment, City, State, Postal_Code, PremiseID "
    If glIs110 Then
        cSQL = cSQL & ", UnreadIncident "
    End If
    cSQL = cSQL & " FROM Response_Master_Incident WHERE ID = " & Str(nIncID)
    
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    Call AVLDebug("Function UpdatePremiseCaution: Step 1 Complete:" & Str(nIncID), gDetailedDebugging)
    
    If nRows > 0 Then
        AddressField = IIf(IsNull(aSQL(0, 0)), "", aSQL(0, 0))       '   Address
        nPremiseID = Val(aSQL(5, 0))                                '   Premise ID
        
        If glIs110 Then
            lUnreadInc = Val(aSQL(6, 0))                            '   Unread Incident Flag
        End If
        
        cSQL = "SET ROWCOUNT 2 " _
                & " SELECT ID FROM Response_Master_Incident With (NOLOCK) " _
                & " WHERE Address = '" & IIf(IsNull(aSQL(0, 0)), "", aSQL(0, 0)) & "' " _
                & " AND Apartment = '" & IIf(IsNull(aSQL(1, 0)), "", aSQL(1, 0)) & "' " _
                & " AND City = '" & IIf(IsNull(aSQL(2, 0)), "", aSQL(2, 0)) & "' " _
                & " AND State = '" & IIf(IsNull(aSQL(3, 0)), "", aSQL(3, 0)) & "' " _
                & " AND Postal_Code = '" & IIf(IsNull(aSQL(4, 0)), "", aSQL(4, 0)) & "' " _
                & " AND Time_CallEnteredQueue IS NOT NULL " _
                & " AND ID NOT IN (SELECT Master_Incident_ID FROM Response_Prescheduled_Info With (NOLOCK)) " _
                & " SET ROWCOUNT 0 "

        nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
        
        Call AVLDebug("Function UpdatePremiseCaution: Step 2 Complete: nRows =" & Str(nRows), gDetailedDebugging)
        
        If nRows > 0 Then
            lPremiseHist = True
            If IsNumeric(Left(AddressField, 1)) Then                '   trim off the numbers in the address
                StreetName = Right(AddressField, Len(AddressField) - InStr(1, AddressField, " "))
                StreetNumber = Left(AddressField, InStr(1, AddressField, " ") - 1)
            Else
                StreetName = AddressField
                StreetNumber = 0
            End If
            
            cSQL = "SELECT CautionNote_ID From CautionNotes_Link, Streets" _
                    & " Where Street_Name = '" & StreetName & "' " _
                    & " And ((CautionNotes_Link.Low_Block_Range = 0 and CautionNotes_Link.High_Block_Range = 0) " _
                    & " Or (CautionNotes_Link.Low_Block_Range <= " & StreetNumber _
                    & " And  CautionNotes_Link.High_Block_Range >= " & StreetNumber & "))" _
                    & " And getdate() >= Start_Date and getdate() <= End_Date " _
                    & " And CautionNotes_Link.Street_Id = Streets.Street_ID "
        
            nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gStreetsDBName, , , False, , ghErrorLog)
            
            Call AVLDebug("Function UpdatePremiseCaution: Step 3 Complete: nRows =" & Str(nRows), gDetailedDebugging)
            
            If nRows > 0 Then
                lCaution = True
            End If
            
        End If
        
        cSQL = "BEGIN TRANSACTION "
        cSQL = cSQL & " Update Response_Master_Incident " _
                    & " SET CautionNote = " & IIf(lCaution, 1, 0) & ", " _
                    & " HazmatNote = 0, " _
                    & " PremiseHistory = " & IIf(lPremiseHist, 1, 0)
                    
        If glIs110 Then
            cSQL = cSQL & ", UnreadIncident = " & IIf(True, 1, 0) & " "                 '   always set UnreadIncident Flag to True on new incidents
        End If
        
        cSQL = cSQL & " Where ID = " & Str(nIncID) _
                    & " IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION "

        nRows = FFADOPut(cSQL, aSQL, nNewIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
        Call AVLDebug("Function UpdatePremiseCaution: Step 4 Complete: nRows =" & Str(nRows), gDetailedDebugging)
        
'        If nRows > 0 Then
            Call AVLDebug("Function UpdatePremiseCaution: Flags updated in new incident:" & Str(nIncID), gDetailedDebugging)
'        Else
'            Call AVLDebug("ERROR - Function UpdatePremiseCaution: Unable to update flags for new incident:" & Str(nIncID), True)
'        End If
        
    Else
        Call AVLDebug("ERROR - Function UpdatePremiseCaution: No rows returned for new incident:" & Str(nIncID), True)
    End If
    
    Exit Function

ErrorHandler:
    Call AVLDebug("ERROR - Function UpdatePremiseCaution: cSQL = " & cSQL, True)
    Call AVLDebug("ERROR - Function UpdatePremiseCaution (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
End Function

Function CreateIncNumber(nIncID As Long, tCADInc As tCADIncident) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim nNewIncID As Long
    
    ' DEPRICATED - See NewCreateFormattedNumberBySP below

    On Error GoTo ErrorHandler
    
    cSQL = "Set NoCount On "
    cSQL = cSQL & " DECLARE @Counter int, " _
                & " @sCounter varchar(30), " _
                & " @len int " _
                & " Begin Transaction " _
                & " Select @Counter = Counter + 1 From Number_Setup Where ID = " & Str(tCADInc.AgencyTypeID) _
                & " Update Number_Setup " _
                & " SET Counter = @Counter Where ID = " & Str(tCADInc.AgencyTypeID) _
                & " And Counter = @Counter - 1 " _
                & " if ((@@rowcount =1) and (@@error=0)) begin " _
                & " SELECT @sCounter = convert(varchar(30),@Counter) " _
                & " select @len=datalength(@scounter) " _
                & " declare @format1 varchar(30) " _
                & " if @len<4 select @format1= replicate('0',4-@len) + @scounter " _
                & " else if @len> 4 select @format1=right(@scounter,@len-4) " _
                & " Else select @format1=@scounter " _
                & " Update response_master_incident " _
                & " set master_incident_number ='" & Format(Now, "YY") & "' + '" & Str(DateDiff("D", "JAN 1", Now) + 1) & "' + '-' + @format1 " _
                & " Where ID = " & Str(nIncID) & " and ISNULL(RTRIM(master_incident_number),'')='' " _
                & " and ISNULL(RTRIM(fixed_Time_CallEnteredQueue),'')<>'' " _
                & " if ((@@rowcount =1) and (@@error=0)) " _
                & " begin commit transaction " _
                & " set nocount off " _
                & " SELECT counter =  '" & Format(Now, "YY") & "' + '" & Str(DateDiff("D", "JAN 1", Now) + 1) & "' + '-' + @format1 End " _
                & " Else begin rollback transaction SELECT counter ='' End End " _
                & " Else begin rollback transaction SELECT counter ='' End "

    nRows = FFADOPut(cSQL, aSQL, nNewIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    If nRows > 0 Then
        Call AVLDebug("Function CreateIncNumber: Incident Number created in new incident:" & Str(nIncID), gDetailedDebugging)
    Else
        Call AVLDebug("ERROR - Function CreateIncNumber: Unable to create Incident Number for new incident:" & Str(nIncID), gDetailedDebugging)
    End If
    
    Exit Function

ErrorHandler:
    Call AVLDebug("ERROR - Function CreateIncNumber: cSQL = " & cSQL, True)
    Call AVLDebug("ERROR - Function CreateIncNumber: (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
End Function

Function NewCreateFormattedNumberBySP(cType As String, nRecID As Long, cAreaName As String) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim nNewIncID As Long
    Dim aParams() As Variant
    Dim aParamType() As Integer
    Dim cReturnValue As Variant
    Dim n As Integer
    Dim nNumberPattern As Long
    Dim cFormattedNumber As String
    
    '
    '   This function calls the TRiTech Stored procedure dbo.USP_FormatNumberFromPattern
    '
    '   which takes the following parameters:
    '
    '       Identifier          - defines the number type within VisiCAD - see below
    '       LinkTo_ID           - points to the specified ID for the number format required
    '                           - for Incident Number, type is "I", pass the Jurisdiction ID
    '                           - for Confirmation Number, type is "C", pass the <?> ID
    '                           - for Response Number, type is "R", pass the Division ID
    '                           - for Patient Number, type is "P", pass the <?> ID
    '                           - for Case Number, type is "N", pass the <?> ID
    '                           - for Service Provider Area, type is "T", pass the <?> ID
    '
    '       MasterIncidentID    - the RMI.ID of the current incident
    '
    '   and returns:
    '
    '       ReturnNumber        - formatted string containing, in this case, the Master_Incident_Number
    '
    '   AND NEW WITH Version 5.x, A NEW PARAMETER!!!      @PrimaryKey (int)       Discovered 2014.07.29 BM
    '                           - Place holder for the ID of the row from the number_setup table
    '

    On Error GoTo NewCreateFormattedNumberBySP_ERH
    
    Select Case cType
        Case "I"                '   Master Incident Number
            cSQL = "SELECT ID FROM Jurisdiction j WHERE j.Name = '" & Trim(cAreaName) & "'"
            Call AVLDebug("INFO - Function NewCreateResponseNumberBySP: Getting Jurisdiction ID to create Master_Incident_Number for new INC:" & Str(nRecID), gDetailedDebugging)
            Call AVLDebug("          cSQL = >" & cSQL & "<", gDetailedDebugging)
        Case "R"                '   Response Number
            cSQL = "SELECT ID FROM Division d WHERE d.DivName = '" & Trim(cAreaName) & "'"
            Call AVLDebug("INFO - Function NewCreateResponseNumberBySP: Getting Division ID to create Master_Incident_Number for new RVA:" & Str(nRecID), gDetailedDebugging)
            Call AVLDebug("          cSQL = >" & cSQL & "<", gDetailedDebugging)
        Case Else
            NewCreateFormattedNumberBySP = False
            Exit Function
    End Select
    
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
        nNumberPattern = aSQL(0, 0)
        
        cSQL = "dbo.USP_FormatNumberFromPattern"
        ReDim aParams(0 To 4)
        ReDim aParamType(0 To 4)
        aParams(0) = cType
        aParamType(0) = 1
        aParams(1) = nNumberPattern
        aParamType(1) = 1
        aParams(2) = nRecID
        aParamType(2) = 1
        aParams(3) = Space(20)
        aParamType(3) = 2                       '   return value required
        
        If Val(gcCADVersion) >= 5# Then         '   required for version 5.0 and above
            aParams(4) = 0                      '   @PrimaryKey - new parameter with v5.0   -   2014.07.29 BM
            aParamType(4) = 2                   '   return value required
        End If
        
        nRows = FFADOPut(cSQL, aSQL, nNewIncID, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog, , True, True, aParams, aParamType, cReturnValue)
        
        If nRows > 0 Then
            cFormattedNumber = Trim(cReturnValue(0))
            
            Select Case cType
                Case "I"
                    cSQL = "Begin Transaction Update response_master_incident" _
                            & " set master_incident_number ='" & Trim(cFormattedNumber) & "'" _
                            & " Where ID =" & Str(nRecID) _
                            & " if ((@@rowcount=1) and (@@error=0)) commit transaction" _
                            & " else rollback transaction"
                Case "R"
                    cSQL = "Begin Transaction Update response_vehicles_assigned" _
                            & " set response_number ='" & Trim(cFormattedNumber) & "'" _
                            & " Where ID =" & Str(nRecID) _
                            & " if ((@@rowcount=1) and (@@error=0)) commit transaction" _
                            & " else rollback transaction"
            End Select

            nRows = FFADOPut(cSQL, aSQL, nNewIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
            
            If nRows >= 0 Then
                NewCreateFormattedNumberBySP = True
                Call AVLDebug("Success - Function NewCreateFormattedNumberBySP: " & cType & " Number created for RecID=" & Str(nRecID) & " (" & cFormattedNumber & ")", gDetailedDebugging)
                For n = 0 To UBound(aParams, 1)
                    Call AVLDebug("          aParams(" & n & ") = " & aParams(n), gDetailedDebugging)
                Next n
                For n = 0 To UBound(cReturnValue, 1)
                    Call AVLDebug("          cReturnValue(" & n & ") = " & cReturnValue(n), gDetailedDebugging)
                Next n
            Else
                Call AVLDebug("ERROR - Function NewCreateFormattedNumberBySP: Unable to create Formatted Number (type=" & cType & ") for Rec:" & Str(nRecID), gDetailedDebugging)
            End If
        Else
            Call AVLDebug("ERROR - Function NewCreateFormattedNumberBySP: Unable to create Formatted Number (type=" & cType & ") for Rec:" & Str(nRecID), gDetailedDebugging)
        End If
    Else
        Call AVLDebug("ERROR - Function NewCreateFormattedNumberBySP: Unable to get Record ID For new Formatted Number (type=" & cType & ") for Rec:" & Str(nRecID) & " cAreaName=" & cAreaName, gDetailedDebugging)
    End If
    
    Exit Function

NewCreateFormattedNumberBySP_ERH:
    NewCreateFormattedNumberBySP = False
    Call AVLDebug("ERROR - Function NewCreateFormattedNumberBySP: cSQL = " & cSQL, True)
    Call AVLDebug("ERROR - Function NewCreateFormattedNumberBySP: (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
End Function

Function NewCreateResponseNumberBySP(nRVAID As Long, cDivName As String) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim nNewIncID As Long
    Dim aParams() As Variant
    Dim aParamType() As Integer
    Dim cReturnValue As Variant
    Dim cResponseNumber As String
    
    '
    '   This function calls the TRiTEch Stored procedure dbo.USP_FormatNumberFromPattern
    '
    '   which takes the following parameters:
    '
    '       Identifier          - defines the number type within VisiCAD, which in this case will always be "R" for Response Number
    '       LinkTo_ID           - points to the ID of the DIVISION for this UNIT
    '       MasterIncidentID    - the RMI.ID of the current incident (not used)
    '
    '   and returns:
    '
    '       ReturnNumber        - formatted string containing, in this case, the Master_Incident_Number
    '

    On Error GoTo NewCreateResponseNumberBySP_ERH
    
    cSQL = "SELECT ID FROM Division d WHERE d.DivName = '" & cDivName & "'"
    
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
        cSQL = "dbo.USP_FormatNumberFromPattern"
        ReDim aParams(0 To 3)
        ReDim aParamType(0 To 3)
        aParams(0) = "R"
        aParamType(0) = 1
        aParams(1) = aSQL(0, 0)     '   Division.ID
        aParamType(1) = 1
        aParams(2) = 0
        aParamType(2) = 1
        aParams(3) = Space(20)
        aParamType(3) = 2       '   return value required
        
        If Val(gcCADVersion) >= 5.6 Then        '   required for version 5.6 and above
            aParams(4) = 0                      '   @PrimaryKey - new parameter with v5.6   -   2014.07.29 BM
            aParamType(4) = 2                   '   return value required
        End If
        
        nRows = FFADOPut(cSQL, aSQL, nNewIncID, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog, , True, True, aParams, aParamType, cReturnValue)
        
        If nRows > 0 Then
            cResponseNumber = Trim(cReturnValue(0))
            
            cSQL = "Begin Transaction Update response_vehicles_assigned" _
                        & " set response_number ='" & Trim(cResponseNumber) & "'" _
                        & " Where ID =" & Str(nRVAID) _
                        & " if ((@@rowcount=1) and (@@error=0)) commit transaction" _
                        & " else rollback transaction"
                    
            nRows = FFADOPut(cSQL, aSQL, nNewIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
            
            If nRows >= 0 Then
                Call AVLDebug("Success - Function NewCreateResponseNumberBySP: Response_Number created for new RVA :" & Str(nRVAID) & " (" & cResponseNumber & ")", gDetailedDebugging)
            Else
                Call AVLDebug("ERROR - Function NewCreateResponseNumberBySP: Unable to create Response_Number for new RVA :" & Str(nRVAID), gDetailedDebugging)
            End If
        Else
            Call AVLDebug("ERROR - Function NewCreateResponseNumberBySP: Unable to create Response_Number for new RVA :" & Str(nRVAID), gDetailedDebugging)
        End If
    Else
        Call AVLDebug("ERROR - Function NewCreateResponseNumberBySP: Unable to get DivisionID For new RVA :" & Str(nRVAID), gDetailedDebugging)
    End If
    
    Exit Function

NewCreateResponseNumberBySP_ERH:
    Call AVLDebug("ERROR - Function NewCreateResponseNumberBySP: cSQL = " & cSQL, True)
    Call AVLDebug("ERROR - Function NewCreateResponseNumberBySP: (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
End Function

Function CreateConfirmationNumber(nIncID As Long, tCADInc As tCADIncident) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim nNewIncID As Long

    On Error GoTo ErrorHandler
    
    cSQL = "Set NoCount On "
    cSQL = cSQL & " DECLARE @Counter int, " _
                & " @sCounter varchar(30), " _
                & " @len int " _
                & " Begin Transaction " _
                & " Select @Counter = Counter + 1 From Number_Setup Where ID = " & Str(tCADInc.AgencyTypeID) _
                & " Update Number_Setup " _
                & " SET Counter = @Counter Where ID = " & Str(tCADInc.AgencyTypeID) _
                & " And Counter = @Counter - 1 " _
                & " if ((@@rowcount =1) and (@@error=0)) begin " _
                & " SELECT @sCounter = convert(varchar(30),@Counter) " _
                & " select @len=datalength(@scounter) " _
                & " declare @format1 varchar(30) " _
                & " if @len<7 select @format1= replicate('0',7-@len) + @scounter " _
                & " else if @len> 7 select @format1=right(@scounter,@len-7) " _
                & " Else select @format1=@scounter " _
                & " Update response_master_incident " _
                & " set Confirmation_Number ='" & Format(Now, "YY") & "' + '-' + @format1 " _
                & " Where ID = " & Str(nIncID) & " and ISNULL(RTRIM(Confirmation_Number),'')='' " _
                & " if ((@@rowcount =1) and (@@error=0)) " _
                & " begin commit transaction " _
                & " set nocount off " _
                & " SELECT counter =  '" & Format(Now, "YY") & "' + '" & Str(DateDiff("D", "JAN 1", Now) + 1) & "' + '-' + @format1 End " _
                & " Else begin rollback transaction SELECT counter ='' End End " _
                & " Else begin rollback transaction SELECT counter ='' End "

    nRows = FFADOPut(cSQL, aSQL, nNewIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    If nRows > 0 Then
        Call AVLDebug("Function CreateConfirmationNumber: Confirmation Number created in new incident:" & Str(nIncID), gDetailedDebugging)
    Else
        Call AVLDebug("ERROR - Function CreateConfirmationNumber: Unable to create Confirmation Number for new incident:" & Str(nIncID), gDetailedDebugging)
    End If
    
    Exit Function

ErrorHandler:
    Call AVLDebug("ERROR - Function CreateConfirmationNumber: cSQL = " & cSQL, True)
    Call AVLDebug("ERROR - Function CreateConfirmationNumber: (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
End Function

Function CopyIncidentComments(nNewIncID As Long, nOldIncID As Long) As Boolean
    Dim cSQL As String
    Dim cSQL2 As String
    Dim aSQL As Variant
    Dim aSQL2 As Variant
    Dim nIncID As Long
    Dim nRows As Long
    Dim n As Long
    Dim nRows2 As Long
    Dim n2 As Long
    Dim nMaxRows As Long
    Dim cComment As String
    Dim TCPMessage As String
    Dim clsINIFile As clsIniTFiles
    
    On Error GoTo CopyIncidentComments_ERH
    
'   This is the OLD WAY
'
'    cSQL = "BEGIN TRANSACTION " _
'        & " DECLARE @ID int DECLARE @Master_Incident_ID int " _
'        & " DECLARE @Other_Reference_ID int " _
'        & " DECLARE @Comment_Type tinyint " _
'        & " DECLARE @Additional_Comment_Group_ID int " _
'        & " DECLARE @Performed_By varchar(25) " _
'        & " DECLARE @Date_Time datetime " _
'        & " DECLARE @Comment varchar(255) " _
'        & " DECLARE @Group_ID int " _
'        & " DECLARE comments_cursor INSENSITIVE CURSOR " _
'        & " FOR " _
'        & "     SELECT ID,Master_Incident_ID,Comment_Type,Additional_Comment_Group_ID,Performed_By,Date_Time,Comment,Other_Reference_ID " _
'        & "     From RESPONSE_COMMENTS Where Master_Incident_ID = " & Str(nOldIncID) & "" _
'        & " OPEN comments_cursor " _
'        & "     FETCH NEXT FROM comments_cursor " _
'        & "     INTO @ID,@Master_Incident_ID,@Comment_Type,@Additional_Comment_Group_ID,@Performed_By,@Date_Time,@Comment,@Other_Reference_ID " _
'        & "     WHILE (@@FETCH_STATUS <> -1) " _
'        & "     BEGIN " _
'        & "     IF (@@FETCH_STATUS <> -2) " _
'        & "         BEGIN " _
'        & "         IF (@ID = @Additional_Comment_Group_ID) " _
'        & "             BEGIN " _
'        & "             INSERT INTO Response_Comments " _
'        & "                 (Master_Incident_ID,Other_Reference_ID,Comment_Type,Additional_Comment_Group_ID,Performed_By,Date_Time,Comment ) " _
'        & "             Values (" & Str(nNewIncID) & ",@Other_Reference_ID,@Comment_Type,@Additional_Comment_Group_ID,@Performed_By,@Date_Time,@Comment) "
'    cSQL = cSQL & "             SELECT @Group_ID = @@IDENTITY " _
'        & "             UPDATE Response_Comments SET Additional_Comment_Group_ID= @@IDENTITY " _
'        & "             WHERE ID = @@IDENTITY " _
'        & "             End " _
'        & "         Else " _
'        & "             BEGIN " _
'        & "             INSERT INTO Response_Comments " _
'        & "                 (Master_Incident_ID,Other_Reference_ID,Comment_Type,Additional_Comment_Group_ID,Performed_By,Date_Time,Comment) " _
'        & "             Values  (" & Str(nNewIncID) & ",@Other_Reference_ID,@Comment_Type,@Group_ID,@Performed_By,@Date_Time,@Comment) " _
'        & "             End " _
'        & "         End " _
'        & "     FETCH NEXT FROM comments_cursor " _
'        & "     INTO @ID,@Master_Incident_ID,@Comment_Type,@Additional_Comment_Group_ID,@Performed_By,@Date_Time,@Comment,@Other_Reference_ID " _
'        & "     End " _
'        & " Close comments_cursor " _
'        & " DEALLOCATE comments_cursor " _
'        & " IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION "
'
'    nRows = FFADOPut(cSQL, aSQL, nIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

'   This is the NEW WAY (via RAPTOR)

    Set clsINIFile = New clsIniTFiles

    clsINIFile.InitFileName = gSystemDirectory & "\Simulator.ini"

    clsINIFile.SectionName = "AVL Settings"
    
    nMaxRows = Val(clsINIFile.ReadKey("MaxComments", "20"))
    
    cSQL = "SELECT ID from Response_Comments where Master_Incident_ID = " & Str(nOldIncID) _
        & "ORDER BY ID"                                                                             '   get all the response_comments from old incident in sequence order

    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
        If nRows > nMaxRows Then nRows = nMaxRows
        For n = 0 To nRows - 1
            cComment = ""
            nIncID = Val(aSQL(0, n))
            cSQL2 = "SELECT Comment FROM Response_Comments WHERE Additional_Comment_Group_ID = " & Str(nIncID)      '   get each comment from old incident (assuming long comments)
            nRows2 = FFADOGet(cSQL2, aSQL2, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
            For n2 = 0 To nRows2 - 1
                cComment = cComment & aSQL2(0, n2)                                                                  '   assemble the comment (assumes long comments)
            Next n2
            
            TCPMessage = Format(CN_ADD_COMMENT_TO_INC, "0000") & "~" & Trim(Str(nNewIncID)) & "~" & cComment & "~Simulator"     '   construct the IPC Message to tell IPCServer to add comment to new incident
            
            If gUseXMLPort Then
                Call goCADServer.SendMessageXML(CN_CADNORTH_IPC_REQUEST, TCPMessage, False, False)        '   Send to IPCServer for add comment to incident
            Else
                Call goCADServer.SendMessage(CN_CADNORTH_IPC_REQUEST, TCPMessage, False, False)           '   Send to IPCServer for add comment to incident
            End If
            
        Next n
    End If
    
    Call AVLDebug("Function CopyIncidentComments: Comments copied to new incident:" & Str(nNewIncID), gDetailedDebugging)

    Exit Function

CopyIncidentComments_ERH:
    Call AVLDebug("ERROR - Function CopyIncidentComments: cSQL = " & cSQL, True)
    Call AVLDebug("ERROR - Function CopyIncidentComments (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
End Function

Function CopyResponseTransports(nNewIncID As Long, nOldIncID As Long) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nIncID As Long
    Dim nRows As Integer
    Dim tTrans As tTransport

    On Error GoTo ErrorHandler
    
    cSQL = "SELECT Master_Incident_ID, Sequence_Number, Patient_Info_ID, Location_Name, Address, " _
            & " Address2, Apartment, City, State, Postal_Code, County, Location_Type, Longitude, " _
            & " latitude, Phone, Map_Info, Building, Pickup_Address, Pickup_Address2, Pickup_Apartment, " _
            & " Pickup_City, Pickup_State, Pickup_Postal_Code, Pickup_County, Pickup_Location_Type, " _
            & " Pickup_Longitude, Pickup_Latitude, Pickup_Phone, Pickup_Map_Info, Pickup_Building, " _
            & " Time_PickupPromised, Time_PickupRequested, Time_Appointment, Pickup_Location_Name, Phone_Ext, " _
            & " Pickup_Phone_Ext, Trip_Type, Name_First, Name_Last, Name_MI, Escort "
    cSQL = cSQL & " FROM Response_Transports " _
                & " WHERE Master_Incident_ID = " & Str(nOldIncID)
                
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
        tTrans.Master_Incident_ID = aSQL(0, 0)
        tTrans.Sequence_Number = aSQL(1, 0)
        tTrans.Patient_Info_ID = aSQL(2, 0)
        tTrans.Location_Name = aSQL(3, 0)
        tTrans.Address = aSQL(4, 0)
        tTrans.Address2 = aSQL(5, 0)
        tTrans.Apartment = aSQL(6, 0)
        tTrans.City = aSQL(7, 0)
        tTrans.State = aSQL(8, 0)
        tTrans.Postal_Code = aSQL(9, 0)
        tTrans.County = aSQL(10, 0)
        tTrans.Location_Type = aSQL(11, 0)
        tTrans.Longitude = aSQL(12, 0)
        tTrans.Latitude = aSQL(13, 0)
        tTrans.Phone = aSQL(14, 0)
        tTrans.Map_Info = aSQL(15, 0)
        tTrans.Building = aSQL(16, 0)
        tTrans.Pickup_Address = aSQL(17, 0)
        tTrans.Pickup_Address2 = aSQL(18, 0)
        tTrans.Pickup_Apartment = aSQL(19, 0)
        tTrans.Pickup_City = aSQL(20, 0)
        tTrans.Pickup_State = aSQL(21, 0)
        tTrans.Pickup_Postal_Code = aSQL(22, 0)
        tTrans.Pickup_County = aSQL(23, 0)
        tTrans.Pickup_Location_Type = aSQL(24, 0)
        tTrans.Pickup_Longitude = aSQL(25, 0)
        tTrans.Pickup_Latitude = aSQL(26, 0)
        tTrans.Pickup_Phone = aSQL(27, 0)
        tTrans.Pickup_Map_Info = aSQL(28, 0)
        tTrans.Pickup_Building = aSQL(29, 0)
        tTrans.Time_PickupPromised = aSQL(30, 0)
        tTrans.Time_PickupRequested = aSQL(31, 0)
        tTrans.Time_Appointment = aSQL(32, 0)
        tTrans.Pickup_Location_Name = aSQL(33, 0)
        tTrans.Phone_Ext = aSQL(34, 0)
        tTrans.Pickup_Phone_Ext = aSQL(35, 0)
        tTrans.Trip_Type = aSQL(36, 0)
        tTrans.Name_First = aSQL(37, 0)
        tTrans.Name_Last = aSQL(38, 0)
        tTrans.Name_MI = aSQL(39, 0)
        tTrans.Escort = aSQL(40, 0)
    Else
        Call AVLDebug("ERROR - Function CopyResponseTransports: No Rows Returned for Incident:" & Str(nOldIncID), True)
        Exit Function
    End If
    
    cSQL = "BEGIN TRANSACTION "
    cSQL = cSQL & " INSERT INTO Response_Transports " _
            & " (Master_Incident_ID, Sequence_Number, Patient_Info_ID, Location_Name, Address, " _
            & " Address2, Apartment, City, State, Postal_Code, County, Location_Type, Longitude, " _
            & " latitude, Phone, Map_Info, Building, Pickup_Address, Pickup_Address2, Pickup_Apartment, " _
            & " Pickup_City, Pickup_State, Pickup_Postal_Code, Pickup_County, Pickup_Location_Type, " _
            & " Pickup_Longitude, Pickup_Latitude, Pickup_Phone, Pickup_Map_Info, Pickup_Building, " _
            & " Time_PickupPromised, Time_PickupRequested, Time_Appointment, Pickup_Location_Name, Phone_Ext, " _
            & " Pickup_Phone_Ext, Trip_Type, Name_First, Name_Last, Name_MI, Escort) "
    cSQL = cSQL & " Values (" _
            & Str(nNewIncID) & ", " _
            & " 0, " _
            & Str(tTrans.Patient_Info_ID) & ", " _
            & " '" & bsiCleanString(tTrans.Location_Name) & "', " _
            & " '" & bsiCleanString(tTrans.Address) & "', " _
            & " '" & bsiCleanString(tTrans.Address2) & "', " _
            & " '" & tTrans.Apartment & "', " _
            & " '" & bsiCleanString(tTrans.City) & "', " _
            & " '" & tTrans.State & "', " _
            & " '" & tTrans.Postal_Code & "', " _
            & " '" & bsiCleanString(tTrans.County) & "', " _
            & " '" & bsiCleanString(tTrans.Location_Type) & "', " _
            & " '" & tTrans.Longitude & "', " _
            & " '" & tTrans.Latitude & "', " _
            & " '" & tTrans.Phone & "', " _
            & " '" & tTrans.Map_Info & "', " _
            & " '" & tTrans.Building & "', " _
            & " '" & bsiCleanString(tTrans.Pickup_Address) & "', " _
            & " '" & bsiCleanString(tTrans.Pickup_Address2) & "', " _
            & " '" & tTrans.Pickup_Apartment & "', " _
            & " '" & bsiCleanString(tTrans.Pickup_City) & "', " _
            & " '" & tTrans.Pickup_State & "', " _
            & " '" & tTrans.Pickup_Postal_Code & "', " _
            & " '" & bsiCleanString(tTrans.Pickup_County) & "', "
    cSQL = cSQL & " '" & bsiCleanString(tTrans.Pickup_Location_Type) & "', " _
            & " '" & tTrans.Pickup_Longitude & "', " _
            & " '" & tTrans.Pickup_Latitude & "', " _
            & " '" & tTrans.Pickup_Phone & "', " _
            & " '" & tTrans.Pickup_Map_Info & "', " _
            & " '" & tTrans.Pickup_Building & "', " _
            & " '" & Format(DateAdd("N", 30, Now), "MMM DD YYYY HH:NN:SS") & "', " _
            & " '" & Format(DateAdd("N", 30, Now), "MMM DD YYYY HH:NN:SS") & "', " _
            & " '" & Format(DateAdd("N", 60, Now), "MMM DD YYYY HH:NN:SS") & "', " _
            & " '" & bsiCleanString(tTrans.Pickup_Location_Name) & "', " _
            & " '" & tTrans.Phone_Ext & "', " _
            & " '" & tTrans.Pickup_Phone_Ext & "', " _
            & " '" & tTrans.Trip_Type & "', " _
            & " '" & bsiCleanString(tTrans.Name_First) & "', " _
            & " '" & bsiCleanString(tTrans.Name_Last) & "', " _
            & " '" & tTrans.Name_MI & "', " _
            & " '" & tTrans.Escort & "') "
    cSQL = cSQL & " SELECT @@IDENTITY x IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"

    nRows = FFADOPut(cSQL, aSQL, nIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    Call AVLDebug("Function CopyResponseTransports: Destinations copied to new incident:" & Str(nNewIncID), gDetailedDebugging)

    Exit Function

ErrorHandler:
    Call AVLDebug("ERROR - Function CopyResponseTransports: cSQL = " & cSQL, True)
    Call AVLDebug("ERROR - Function CopyResponseTransports (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
End Function

Function CopyPrescheduledInformation(nNewIncID As Long, nOldIncID As Long) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nIncID As Long
    Dim nRows As Integer
    Dim tTrans As tPreSchedule

    On Error GoTo ErrorHandler
    
    cSQL = "SELECT Master_Incident_ID, PreScheduled_ID, Time_CallTaken, Time_PickupPromised, Time_PickupRequested, " _
            & " CallTaking_Performed_By, Time_Advanced_Sched, Time_Appointment, Physician, ReferringPhysician, " _
            & " Special_Equipment, Need_IV, Need_EKG, Need_Oxygen, Need_Family_Rider, " _
            & " Need_Paramedic, Need_WheelChair, Need_Restraint, Need_Suction, Authorization_Number, " _
            & " Name_First, Name_Last, Name_MI, Call_Status, patient_info_id, " _
            & " Call_Sequence, Return_Master_Incident_ID, Escort "
    cSQL = cSQL & " FROM Response_Prescheduled_Info " _
                & " WHERE Master_Incident_ID = " & Str(nOldIncID)
                
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
        tTrans.Master_Incident_ID = aSQL(0, 0)
        tTrans.PreScheduled_ID = aSQL(1, 0)
        tTrans.Time_CallTaken = aSQL(2, 0)
        tTrans.Time_PickupPromised = aSQL(3, 0)
        tTrans.Time_PickupRequested = aSQL(4, 0)
        tTrans.CallTaking_Performed_By = aSQL(5, 0)
        tTrans.Time_Advanced_Sched = aSQL(6, 0)
        tTrans.Time_Appointment = aSQL(7, 0)
        tTrans.Physician = aSQL(8, 0)
        tTrans.ReferringPhysician = aSQL(9, 0)
        tTrans.Special_Equipment = aSQL(10, 0)
        tTrans.Need_IV = aSQL(11, 0)
        tTrans.Need_EKG = aSQL(12, 0)
        tTrans.Need_Oxygen = aSQL(13, 0)
        tTrans.Need_Family_Rider = aSQL(14, 0)
        tTrans.Need_Paramedic = aSQL(15, 0)
        tTrans.Need_WheelChair = aSQL(16, 0)
        tTrans.Need_Restraint = aSQL(17, 0)
        tTrans.Need_Suction = aSQL(18, 0)
        tTrans.Authorization_Number = aSQL(19, 0)
        tTrans.Name_First = aSQL(20, 0)
        tTrans.Name_Last = aSQL(21, 0)
        tTrans.Name_MI = aSQL(22, 0)
        tTrans.Call_Status = aSQL(23, 0)
        tTrans.Patient_Info_ID = aSQL(24, 0)
        tTrans.Call_Sequence = aSQL(25, 0)
        tTrans.Escort = IIf(IsNull(aSQL(27, 0)), "", aSQL(27, 0))
    Else
        Call AVLDebug("ERROR - Function CopyPrescheduleInformation: No Rows Returned for id-" & Str(nOldIncID), True)
        Exit Function
    End If
    
    cSQL = "BEGIN TRANSACTION "
    cSQL = cSQL & " INSERT INTO Response_Prescheduled_Info " _
                & " (Master_Incident_ID, PreScheduled_ID, Time_CallTaken, Time_PickupPromised, " _
                & " Time_PickupRequested, CallTaking_Performed_By, Time_Advanced_Sched, Time_Appointment, " _
                & " Physician, ReferringPhysician, Special_Equipment, Need_IV, Need_EKG, Need_Oxygen, " _
                & " Need_Family_Rider, Need_Paramedic, Need_WheelChair, Need_Restraint, Need_Suction, " _
                & " Authorization_Number, Name_First, Name_Last, Name_MI, Call_Status, patient_info_id, " _
                & " Call_Sequence, Return_Master_Incident_ID, Escort) " _
            & " VALUES ("
    
    '   UPDATE: Added actual times from Prescheduled time fields - BM 2023.02.07
    cSQL = cSQL & Str(nNewIncID) & ", " _
                & " 0, " _
                & " '" & Format(tTrans.Time_CallTaken, "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & Format(Now, "MMM DD YYYY") & " " & Format(tTrans.Time_PickupPromised, "HH:NN:SS") & "', " _
                & " '" & Format(Now, "MMM DD YYYY") & " " & Format(tTrans.Time_PickupRequested, "HH:NN:SS") & "', " _
                & " '" & tTrans.CallTaking_Performed_By & "', " _
                & " 'Jan 01 1900 00:00:00', " _
                & " '" & Format(Now, "MMM DD YYYY") & " " & Format(tTrans.Time_Appointment, "HH:NN:SS") & "', " _
                & " '" & bsiCleanString(tTrans.Physician) & "', " _
                & " '" & bsiCleanString(tTrans.ReferringPhysician) & "', " _
                & " '" & bsiCleanString(tTrans.Special_Equipment) & "', " _
                & Str(tTrans.Need_IV) & ", " _
                & Str(tTrans.Need_EKG) & ", " _
                & Str(tTrans.Need_Oxygen) & ", " _
                & Str(tTrans.Need_Family_Rider) & ", " _
                & Str(tTrans.Need_Paramedic) & ", " _
                & Str(tTrans.Need_WheelChair) & ", " _
                & Str(tTrans.Need_Restraint) & ", " _
                & Str(tTrans.Need_Suction) & ", "
    
'   OLD LAZY WAY
'    cSQL = cSQL & Str(nNewIncID) & ", " _
'                & " 0, " _
'                & " '" & Format(Now, "MMM DD YYYY HH:NN:SS") & "', " _
'                & " '" & Format(DateAdd("N", 30, Now), "MMM DD YYYY HH:NN:SS") & "', " _
'                & " '" & Format(DateAdd("N", 30, Now), "MMM DD YYYY HH:NN:SS") & "', " _
'                & " '" & tTrans.CallTaking_Performed_By & "', " _
'                & " 'Jan 01 1900 00:00:00', " _
'                & " '" & Format(DateAdd("N", 60, Now), "MMM DD YYYY HH:NN:SS") & "', " _
'                & " '" & bsiCleanString(tTrans.Physician) & "', " _
'                & " '" & bsiCleanString(tTrans.ReferringPhysician) & "', " _
'                & " '" & bsiCleanString(tTrans.Special_Equipment) & "', " _
'                & Str(tTrans.Need_IV) & ", " _
'                & Str(tTrans.Need_EKG) & ", " _
'                & Str(tTrans.Need_Oxygen) & ", " _
'                & Str(tTrans.Need_Family_Rider) & ", " _
'                & Str(tTrans.Need_Paramedic) & ", " _
'                & Str(tTrans.Need_WheelChair) & ", " _
'                & Str(tTrans.Need_Restraint) & ", " _
'                & Str(tTrans.Need_Suction) & ", "
    cSQL = cSQL & " '" & tTrans.Authorization_Number & "', " _
                & " '" & bsiCleanString(tTrans.Name_First) & "', " _
                & " '" & bsiCleanString(tTrans.Name_Last) & "', " _
                & " '" & bsiCleanString(tTrans.Name_MI) & "', " _
                & " '" & bsiCleanString(tTrans.Call_Status) & "', " _
                & Str(tTrans.Patient_Info_ID) & ", " _
                & Str(tTrans.Call_Sequence) & ", " _
                & " NULL, " _
                & " '" & tTrans.Escort & "') "
    cSQL = cSQL & " SELECT @@IDENTITY x " _
                & " IF @@ERROR = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
    
    nRows = FFADOPut(cSQL, aSQL, nIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    If nRows > 0 Then
        Call AVLDebug("Function CopyPrescheduleInformation: Preschedule Information copied to new incident:" & Str(nNewIncID), gDetailedDebugging)
    Else
        Call AVLDebug("ERROR - Function CopyPrescheduleInformation: Unable to copy Preschedule Information to new incident:" & Str(nNewIncID), True)
    End If

    Exit Function

ErrorHandler:
    Call AVLDebug("ERROR - Function CopyPrescheduleInformation: cSQL = " & cSQL, True)
    Call AVLDebug("ERROR - Function CopyPrescheduleInformation (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
End Function

Private Sub ActivateCADMon(nNewIncID As Long)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim aParams As Variant
    Dim nRows As Long
    Dim nID As Long
    
    On Error GoTo ErrHandler
    
    aParams = Array(nNewIncID)
        
    cSQL = "USP_CADMonSchedWaitingCallLate"
    nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog, , True, , aParams)
    
    Call AVLDebug("Function ActivateCADMon: Preschedule Incident Sent to CADMonitor:" & Str(nNewIncID), gDetailedDebugging)
    
    Exit Sub

ErrHandler:
    Call AVLDebug("ERROR - Function ActivateCADMon: cSQL = " & cSQL, True)
    Call AVLDebug("ERROR - Function ActivateCADMon (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
End Sub

Public Function bsiCleanString(InputString As String) As String
    If InStr(1, InputString, "'") > 0 Then
        bsiCleanString = Replace(InputString, "'", "''")
    Else
        bsiCleanString = InputString
    End If
End Function

Public Function bsiMDTProcessANIALI(ByVal aE911Message As String, Optional bFromANIALITable As Boolean = False) As Long
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim nIncID As Long
    Dim AniAli As String
    Dim aArray As Variant
    Dim lCellular As Boolean
    Dim cPhoneNumber As String
    Dim n As Integer
    
    On Error GoTo ErrHandler
    
    aArray = Split(aE911Message, ",")
    
    cSQL = "Select PhoneNumber, TimeCallReceived, HouseNumber, Direction, " _
            & " Address, City, State, ESN, ESNText, ClassOfService, CustomerName, PilotPhone, " _
            & " LocationInfo, OriginalTextMessage, PSAP, JobNumber, AptNumber, Building, " _
            & " Longitude, Latitude, Altitude, Caller_Type, LinkID "
    
    If Not bFromANIALITable Then            '   we're using the ID from Response_Master_Incident to create the ANIALI record
    
        cSQL = cSQL & " from Response_ANIALI " _
                    & " WHERE Master_Incident_ID = '" & aArray(0) & "' "
        
    Else                                    '   we're using the ID from the Response_ANIALI table to create the ANIALI record
    
        cSQL = cSQL & " from Response_ANIALI " _
                    & " WHERE ID = '" & aArray(0) & "' "
        
    End If
            
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 And UBound(aArray) <= 2 Then
    
        For n = 0 To UBound(aSQL, 1)
            '   Debug.Print n, aSQL(n, 0), VarType(aSQL(n, 0))
            aSQL(n, 0) = FFADOFixString((aSQL(n, 0)))
        Next n
    
        cSQL = "Begin Transaction Insert into Response_ANIALI " _
            & " (PhoneNumber, TimeCallReceived, HouseNumber, Direction, Address, City, State, ESN, ESNText," _
            & "  ClassOfService, CustomerName, PilotPhone, LocationInfo, OriginalTextMessage, PSAP," _
            & "  JobNumber, AptNumber, Building, Longitude, Latitude, Altitude, Caller_Type) " _
            & " Values ('" & IIf(IsNull(aSQL(0, 0)), " ", aSQL(0, 0)) & "', " _
            & " '" & Format(Now, "MMM DD YYYY HH:NN:SS") & "', " _
            & " '" & IIf(IsNull(aSQL(2, 0)), " ", aSQL(2, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(3, 0)), " ", aSQL(3, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(4, 0)), " ", Replace(aSQL(4, 0), "'", "''")) & "', " _
            & " '" & IIf(IsNull(aSQL(5, 0)), " ", aSQL(5, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(6, 0)), " ", aSQL(6, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(7, 0)), " ", aSQL(7, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(8, 0)), " ", aSQL(8, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(9, 0)), " ", aSQL(9, 0)) & "', "
        cSQL = cSQL & " '" & IIf(IsNull(aSQL(10, 0)), " ", aSQL(10, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(11, 0)), " ", aSQL(11, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(12, 0)), " ", aSQL(12, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(13, 0)), " ", aSQL(13, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(14, 0)), " ", aSQL(14, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(15, 0)), " ", aSQL(15, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(16, 0)), " ", aSQL(16, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(17, 0)), " ", aSQL(17, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(18, 0)), " ", aSQL(18, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(19, 0)), " ", aSQL(19, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(20, 0)), " ", aSQL(20, 0)) & "', " _
            & " '" & IIf(IsNull(aSQL(21, 0)), " ", aSQL(21, 0)) & "') " _
            & " Select @@Identity x IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
        
        nRows = FFADOPut(cSQL, aSQL, nIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
        
        If nRows > 0 Then
            AniAli = aSQL(0, 0) & "," & aArray(1) & "," & aArray(2)
            If gUseXMLPort Then
                Call goCADServer.SendMessageXML(NP_READ_ANIALI, AniAli, False, False)
            Else
                Call goCADServer.SendMessage(NP_READ_ANIALI, AniAli, False, False)
            End If
        End If
        
    Else
        
        If UBound(aArray) > 2 Then
            lCellular = True
        Else
            lCellular = False
        End If
        
        cSQL = "Select Address, City, State, Call_Back_Phone, Caller_Name, Apartment " _
            & " From Response_Master_Incident " _
            & " Where ID = " & aArray(0)
        
        nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
        
        If nRows > 0 Then
        
            If lCellular Then
                If IsNull(aSQL(3, 0)) Then
                    cPhoneNumber = ""
                ElseIf aSQL(3, 0) = "" Then
                    cPhoneNumber = ""
                ElseIf Left(aSQL(3, 0), 1) = "(" Then
                    cPhoneNumber = Right(Trim(aSQL(3, 0)), 8) & Left(aSQL(3, 0), 5)
                Else
                    cPhoneNumber = Right(Trim(aSQL(3, 0)), 8) & "(" & Left(aSQL(3, 0), 3) & ")"
                End If
                
                cSQL = "Begin Transaction Insert into Response_ANIALI " _
                    & " (TimeCallReceived, PhoneNumber, Address, City, State, PilotPhone, AptNumber,ClassOfService)" _
                    & " Values ('" & Format(Now, "MMM DD YYYY HH:NN:SS") & "', " _
                    & " '" & aArray(5) & "', " _
                    & " '" & Replace(aArray(3), "'", "''") & "', " _
                    & " '" & IIf(IsNull(aSQL(1, 0)), " ", aSQL(1, 0)) & "', " _
                    & " '" & IIf(IsNull(aSQL(2, 0)), " ", aSQL(2, 0)) & "', " _
                    & " '" & IIf(IsNull(aSQL(3, 0)), " ", aSQL(3, 0)) & "', " _
                    & " '" & cPhoneNumber & "', " _
                    & " '" & aArray(4) & "') " _
                    & " Select @@Identity x IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
            Else
                cSQL = "Begin Transaction Insert into Response_ANIALI " _
                    & " (TimeCallReceived, PhoneNumber, Address, City, State, PilotPhone, AptNumber)" _
                    & " Values ('" & Format(Now, "MMM DD YYYY HH:NN:SS") & "', " _
                    & " '" & IIf(IsNull(aSQL(3, 0)), " ", aSQL(3, 0)) & "', " _
                    & " '" & IIf(IsNull(aSQL(0, 0)), " ", Replace(aSQL(0, 0), "'", "''")) & "', " _
                    & " '" & IIf(IsNull(aSQL(1, 0)), " ", aSQL(1, 0)) & "', " _
                    & " '" & IIf(IsNull(aSQL(2, 0)), " ", aSQL(2, 0)) & "', " _
                    & " '" & IIf(IsNull(aSQL(3, 0)), " ", aSQL(3, 0)) & "', " _
                    & " '" & IIf(IsNull(aSQL(5, 0)), " ", aSQL(5, 0)) & "') " _
                    & " Select @@Identity x IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
            End If
                
            nRows = FFADOPut(cSQL, aSQL, nIncID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
        
            If nRows > 0 Then
                AniAli = aSQL(0, 0) & "," & aArray(1) & "," & aArray(2)
                If gUseXMLPort Then
                    Call goCADServer.SendMessageXML(NP_READ_ANIALI, AniAli, False, False)
                Else
                    Call goCADServer.SendMessage(NP_READ_ANIALI, AniAli, False, False)
                End If
            End If
           
        End If
        
    End If
    
    Call AVLDebug("Function bsiMDTProcessANIALI completed for RMI ID = " & aArray(0), gDetailedDebugging)
    
    Exit Function

ErrHandler:
    Call AVLDebug("ERROR - Function bsiMDTProcessANIALI: cSQL = " & cSQL, True)
    Call AVLDebug("ERROR - Function bsiMDTProcessANIALI (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
End Function

Public Function bsiSetUnitsOffDuty(nAgencyID As Integer) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim aUnits As Variant
    Dim nUnits As Integer
    Dim nRows As Long
    Dim nID As Long
    Dim n As Integer
    
    On Error GoTo ERH

    '   Book off all units in the 'nAgencyID' agency

    bsiSetUnitsOffDuty = False
    
    cSQL = "Select v.ID, u.Code, s.description, v.master_incident_id " _
        & " from Vehicle v join Unit_Names u on v.UnitName_ID = u.ID " _
        & " join status s on v.Status_ID = s.ID " _
        & " where Master_Incident_ID is NULL " _
        & " order by u.code"
    
    nUnits = FFADOGet(cSQL, aUnits, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)

    If nUnits > 0 Then
        cSQL = "Update Vehicle " _
                & " Set Status_ID = " & Str(K_CAD_OffDuty) _
                & " Where Master_Incident_ID is NULL "
    
        nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
        
        bsiSetUnitsOffDuty = True
        For n = 0 To nUnits - 1
            If gUseXMLPort Then
                Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(Str(aUnits(0, n))), False, False)    '   Got to make sure folks know
            Else
                Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(Str(aUnits(0, n))), False, False)    '   Got to make sure folks know
            End If
            Call AVLDebug("[DEBUG] bsiSetUnitsOffDuty: Unit " & aUnits(1, n) & " (" & aUnits(2, n) & ") NP_READ_VEHICLE sent", gDetailedDebugging)
        Next n
    End If
    
    Exit Function
    
ERH:

    Call AVLDebug("ERROR: SetUnitsOffDuty - AgencyID = " & Str(nAgencyID), True)
    Call AVLDebug("  ERR: " & Str(Err.Number) & " Desc: " & Err.Description & " Source: " & Err.Source, True)

End Function

Public Function bsiSetUnitsOnDuty(aMessage As Variant) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim nID As Long
    Dim n As Integer
    Dim OnTime As Variant
    Dim cLoc As String
    Dim nLat As Long, nLon As Long
    Dim cRadioName As String
    Dim nVehID As Long
    Dim nUnitNameID As Long
    Dim cMessage As String
    Dim cXMLMsg As String
        
    On Error GoTo ERH

    bsiSetUnitsOnDuty = False
    
    '   Book all units OnDuty in the 'nAgencyID' agency based on the Activity Log BookON records
    
    OnTime = CDate(aMessage(1))
    cLoc = aMessage(2)
    nLat = Val(aMessage(3))
    nLon = Val(aMessage(4))
    cRadioName = aMessage(5)
    nVehID = Val(aMessage(6))

    cMessage = "["                                      '   reconstruct the debugging message
    For n = 0 To UBound(aMessage)
        If n > 0 Then cMessage = cMessage & "|"
        cMessage = cMessage & aMessage(n)
    Next n
    cMessage = cMessage & "]"

    cSQL = "Select ID from Unit_Names Where Code = '" & cRadioName & "' "
    
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    If nRows > 0 Then
        nUnitNameID = Val(aSQL(0, 0))
    
        cSQL = "BEGIN TRANSACTION Update Vehicle " _
                & " Set Current_Location = '" & cLoc & "', " _
                & "     Current_Lat = " & nLat & ", " _
                & "     Current_Lon = " & nLon & ", " _
                & "     Destination_Location = NULL, " _
                & "     Destination_City = NULL, " _
                & "     Destination_Lat = NULL, " _
                & "     Destination_Lon = NULL, " _
                & "     StartShiftDate = '" & Format(OnTime, "MMM DD YYYY HH:NN:SS") & "', " _
                & "     EndShiftDate = '" & Format(DateAdd("H", 12, OnTime), "MMM DD YYYY HH:NN:SS") & "', " _
                & "     UnitName_ID = " & Str(nUnitNameID) & ", " _
                & "     Status_ID = " & Str(K_CAD_Avail) _
                & " Where ID = " & Str(nVehID) _
                & "   and Master_Incident_ID is NULL " _
                & " IF @@ERROR = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
                
        nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
        
        Call MDTUpdateActivityLog(Format(OnTime, "MMM DD YYYY HH:NN:SS"), nVehID, cRadioName, "SIM_LOGON", cLoc, cMessage, "NULL", nLat, nLon, 0, "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL")
        If gUseXMLPort Then
            Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(Str(nVehID)), False, False)    '   Got to make sure folks know
        Else
            Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(Str(nVehID)), False, False)    '   Got to make sure folks know
        End If
        
        cXMLMsg = MDTBuildXML244(nVehID, K_CAD_OffDuty, K_CAD_Avail, 0, Format(OnTime, "MMM DD YYYY HH:NN:SS"), "0")    '   send it as a status change
        
        If gUseXMLPort Then
            Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)                                     '   so that the route to station
        Else                                                                                                            '   will be calculated by the Sim Mgr
            Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)                                        '   so that the route to station
        End If
        bsiSetUnitsOnDuty = True
        Call AVLDebug("bsiSetUnitsOnDuty - Unit Logged On: " & cRadioName, gDetailedDebugging)
    
    Else
        Call AVLDebug("ERROR: bsiSetUnitsOnDuty - Radio Name not found:" & cRadioName & " " & cMessage, True)
    End If
    
    Exit Function
    
ERH:
    
    Call AVLDebug("ERROR: bsiSetUnitsOnDuty - " & cMessage, True)
    Call AVLDebug("  ERR: " & Str(Err.Number) & " Desc: " & Err.Description & " Source: " & Err.Source, True)
End Function

Public Function bsiCreateActivityContext(StartTime As Variant) As Boolean
    Dim nRows As Long
    Dim cSQL As String
    Dim aSQL As Variant
    Dim aACT As Variant
    Dim cStart As String
    Dim cStop As String
    Dim n As Integer
    Dim nID As Long
    
    On Error GoTo ErrHandler
    
    '   Purge Activity Log for today (if anything exists)
    
    cStart = Format(Now, "MMM DD, YYYY") & " 00:00:00"
    cStop = Format(Now, "MMM DD, YYYY HH:NN:SS")
    
    cSQL = "BEGIN TRANSACTION " _
        & " DELETE from Activity_Log " _
        & " Where Date_Time Between '" & cStart & "' and '" & cStop & "' " _
        & " If @@ERROR = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION "
        
    nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    Call AVLDebug("bsiCreateActivityContext - " & Str(nRows) & " purged from Activity Log", True)
    
    '   Now insert all the non-AVL rows from the script date into "today"
    
    cStart = Format(StartTime, "MMM DD, YYYY") & " 00:00:00"
    cStop = Format(StartTime, "MMM DD, YYYY") & " " & Format(Now, "HH:NN:SS")
    
    cSQL = "Select Date_Time " _
        & " From Activity_Log " _
        & " Where date_time between '" & cStart & "' and '" & cStop & "' " _
        & " and Activity <> 'AVL' "

    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog, 90)

    Call AVLDebug("bsiCreateActivityContext - Inserting " & Str(nRows) & " into Activity Log", True)
    
    cSQL = "BEGIN TRANSACTION " _
        & " INSERT into Activity_Log " _
        & " Select DATEADD(day, DATEDIFF(day, Date_Time, GETDATE()), Date_Time), " _
        & " Activity, Location, Comment, Latitude, Longitude, Radio_Name, Dispatcher_Init, Agency_ID, Jurisdiction_ID, " _
        & " Division_ID, Battalion_ID, Station_ID, Terminal, Radio_Code, Master_Incident_ID, Vehicle_ID, Personnel_ID " _
        & " From Activity_Log " _
        & " Where date_time between '" & cStart & "' and '" & cStop & "' " _
        & " and Activity <> 'AVL' " _
        & " IF @@ERROR = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION "

    nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog, 90)

    Exit Function

ErrHandler:
    Call AVLDebug("ERROR: bsiCreateActivityContext - cSQL = " & cSQL, True)
    Call AVLDebug("ERROR: bsiCreateActivityContext - nRows = " & Str(nRows), True)
    Call AVLDebug("  ERR: " & Str(Err.Number) & " Desc: " & Err.Description & " Source: " & Err.Source, True)
End Function

Public Function bsiCreateStartUpContext(StartTime As Variant, ScriptTime As Variant) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long, o As Long
    Dim OldID As String
    Dim TempTime As Long
    Dim cMessage As String
    Dim nIncID As Long
    Dim nOldIncID As Long
    Dim nNewIncID As Long
    Dim lPreSched As Boolean
    Dim lExists As Boolean
    Dim tCADInc As tCADIncident
    Dim nStatusID As Long
    Dim nVehID As Long
    Dim StatusTime As Variant
    Dim IncStartTime As Variant
    Dim IncAssignedTime As Variant
    Dim nPreviousVehID As Long

    On Error GoTo ERRHNDLR
    
    cSQL = "Select  rm.ID, rv.ID, rt.ID, " _
                & " rm.priority_description, " _
                & " rv.radio_name, " _
                & " re.description, " _
                & " rm.address, " _
                & " rt.Location_name, " _
                & " rv.time_assigned, " _
                & " rv.time_enroute, " _
                & " rv.time_ArrivedAtScene, " _
                & " rt.time_Depart_Scene, " _
                & " rt.time_arrive_destination, " _
                & " rv.time_delayed_availability, " _
                & " rv.time_call_cleared, " _
                & " rv.Vehicle_ID " _
        & " from    Response_master_incident rm left outer join " _
                & " Response_Vehicles_assigned rv on rv.master_incident_id = rm.id left outer join " _
                & " Response_Transports rt on rt.vehicle_assigned_id = rv.id left outer join " _
                & " Vehicle ve on ve.ID = rv.Vehicle_ID left outer join " _
                & " Resource re on ve.PrimaryResource_ID = re.ID " _
        & " Where   rm.Time_CallEnteredQueue <= '" & Format(StartTime, "MMM DD YYYY HH:NN:SS") & "' " _
                & " and rm.Time_CallClosed > '" & Format(StartTime, "MMM DD YYYY HH:NN:SS") & "' " _
                & " order by rm.id, rv.time_assigned "
                
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    If nRows > 0 Then
        n = 0
        Do While n < nRows
            nOldIncID = Val(aSQL(0, n))
            tCADInc = GetCADIncident(nOldIncID, lExists, lPreSched)
            IncStartTime = DateAdd("S", DateDiff("S", StartTime, tCADInc.Response_Date), ScriptTime)
            If lExists Then
                nNewIncID = InsertIncident(tCADInc, nOldIncID, IncStartTime, lPreSched)
                If glIs110 Then
                    Call UpdatePremiseCaution(nNewIncID)
                End If
                If Not lPreSched Then
                    Call CreateIncNumber(nNewIncID, tCADInc)
                Else
                    Call CreateConfirmationNumber(nNewIncID, tCADInc)
                    Call CopyPrescheduledInformation(nNewIncID, nOldIncID)
                    Call CopyResponseTransports(nNewIncID, nOldIncID)
                    Call ActivateCADMon(nNewIncID)
                End If
                Call CopyIncidentComments(nNewIncID, nOldIncID)
                
                If gUseXMLPort Then
                    Call goCADServer.SendMessageXML(NP_READ_RESPONSE_COMMENTS, Trim(Str(nNewIncID)), False, False)
                    Call goCADServer.SendMessageXML(NP_ADD_DISPLAY_COMMENT_NOTE, Trim(Str(nNewIncID)), False, False)
                    Call goCADServer.SendMessageXML(NP_NEW_RESPONSE, Trim(Str(nNewIncID)), False, False)
                
                    '   Now tell Simulator the old vs. new incident numbers
                    
                    Call goCADServer.SendMessageXML(NP_BRIMAC_MESSAGE_CLASS, Format(NP_BSI_OLDINC_NEWINC_XREF, "0000") & "|" _
                                        & Trim(Str(nOldIncID)) & "|" & Trim(Str(nNewIncID)), False, False)
                Else
                    Call goCADServer.SendMessage(NP_READ_RESPONSE_COMMENTS, Trim(Str(nNewIncID)), False, False)
                    Call goCADServer.SendMessage(NP_ADD_DISPLAY_COMMENT_NOTE, Trim(Str(nNewIncID)), False, False)
                    Call goCADServer.SendMessage(NP_NEW_RESPONSE, Trim(Str(nNewIncID)), False, False)
                
                    '   Now tell Simulator the old vs. new incident numbers
                
                    Call goCADServer.SendMessage(NP_BRIMAC_MESSAGE_CLASS, Format(NP_BSI_OLDINC_NEWINC_XREF, "0000") & "|" _
                                                        & Trim(Str(nOldIncID)) & "|" & Trim(Str(nNewIncID)), False, False)
                End If
                
                Do While aSQL(0, n) = nOldIncID
                    If IsNull(aSQL(15, n)) Then
                        nVehID = 0
                    Else
                        nVehID = Val(aSQL(15, n))
                    End If
                    If nVehID <> nPreviousVehID Then        '   we need this test to eliminate duplicate assignments
                        nPreviousVehID = nVehID             '   caused by two transport records for the same unit on the same call
                        If nVehID > 0 Then
                            nStatusID = 0
                            For o = 14 To 8 Step -1
                                If Not IsNull(aSQL(o, n)) Then
                                    StatusTime = CDate(aSQL(o, n))
                                    If StatusTime <= StartTime Then
                                        nStatusID = GetStatusID(o)
                                        Exit For
                                    End If
                                End If
                            Next o
                            If nStatusID >= K_CAD_Disp And nStatusID <= K_CAD_DELAYEDAVAILABLE Then
                                Call AssignUnits(nNewIncID, nVehID, IncStartTime)
                                '   Call StatusUnit(nVehID, nStatusID)
                            End If
                        End If
                    End If
                    n = n + 1
                Loop
            Else
                Call AVLDebug("bsiCreateStartUpContext: Error Retrieving ID " & Str(nOldIncID) & ". Requested Incident NOT created.", True)
                n = n + 1
            End If
        Loop
    
    End If
    
    Exit Function
    
ERRHNDLR:
    Call AVLDebug("ERROR: bsiCreateStartUpContext - " & Str(Err.Number) & "   ERR: " & Err.Description, True)
    Call AVLDebug("ERROR: bsiCreateStartUpContext - cSQL = " & cSQL, True)
    Call AVLDebug("ERROR: bsiCreateStartUpContext - nRows = " & Str(nRows), True)
End Function

Public Function AssignUnits(nIncID As Long, nVehID As Long, StartTime As Variant)
    Dim cSQL As String
    Dim rSQL As Variant
    Dim vSQL As Variant
    Dim aSQL As Variant
    Dim nRows As Long
    Dim nID As Long
    Dim nPrimaryFlag As Integer
    Dim n As Long
    Dim nLat As Long, nLon As Long
    Dim cLoc As String
    Dim nOldStatusID As Long
    Dim cXMLMsg As String
    Dim cRadioName As String
    Dim cActivity As String
    Dim nPostID As Integer
    
    On Error GoTo AssignUnits_ERH
        
    AssignUnits = False
    
    cSQL = "Select  r.Address, r.City, r.Agency_Type, r.Jurisdiction, r.Division, r.Battalion, " _
            & "     r.Station, r.Latitude, r.Longitude, d.ID " _
            & " From Response_Master_Incident r join Division d on r.Division = d.DivName " _
            & " Where r.ID = " & Str(nIncID)
            
    nRows = FFADOGet(cSQL, rSQL, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    If nRows > 0 Then
        cSQL = "Select v.Name, u.description, v.Current_Location, v.Current_Lat, v.Current_Lon, v.Status_ID, v.Post_Station_ID " _
                & " from vehicle v join unit_names u on v.UnitName_ID = u.id " _
                & " where v.id = " & Str(nVehID)
        
        nRows = FFADOGet(cSQL, vSQL, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
        
        cRadioName = vSQL(1, 0)
        nOldStatusID = vSQL(5, 0)
        nPostID = vSQL(6, 0)
        
        '   Set the unit assignment
        
        cSQL = "BEGIN TRANSACTION UPDATE VEHICLE " _
                & " SET STATUS_ID = " & Str(K_CAD_Disp) & ", " _
                & "     Destination_Location = '" & rSQL(0, 0) & "', " _
                & "     Destination_City = '" & rSQL(1, 0) & "', " _
                & "     Destination_Lat = " & rSQL(7, 0) & ", " _
                & "     Destination_Lon = " & rSQL(8, 0) & ", " _
                & "     Elapsed_Time = '" & Format(Now, "MMM DD YYYY HH:NN:SS") & "', " _
                & "     Master_Incident_ID = " & Str(nIncID) & ", " _
                & "     CurrentDivision_ID = " & rSQL(9, 0) & ", " _
                & "     Saved_Status_ID = Status_ID " _
                & " Where ID = " & Str(nVehID) _
                & " And Status_ID = " & Str(nOldStatusID) _
                & " IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION "

        nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
        '   Insert the RVA record
        
        cSQL = " Select PrimaryVehicleFlag From Response_Vehicles_Assigned Where Master_Incident_ID = " & Str(nIncID)

        nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
        
        If nRows > 0 Then
            nPrimaryFlag = 0
        Else
            nPrimaryFlag = 1
        End If
        
        cSQL = "BEGIN TRANSACTION " _
                & " Insert Into Response_Vehicles_Assigned " _
                & " (Master_Incident_ID, Agency_Type, Jurisdiction, Division, Battalion, Station," _
                & " Radio_Name, Vehicle_ID, Status_ID, TimeStatusChanged, Time_assigned, Fixed_Time_assigned, " _
                & " Location_At_Assign_Time, Longitude_At_Assign_Time, Latitude_At_Assign_Time, Vehicle_Info_ID, " _
                & " Assign_Performed_By, PrimaryVehicleFlag) " _
                & " Values (" & Str(nIncID) & ", " _
                & " '" & rSQL(2, 0) & "', " _
                & " '" & rSQL(3, 0) & "', " _
                & " '" & rSQL(4, 0) & "', " _
                & " '" & rSQL(5, 0) & "', " _
                & " '" & rSQL(6, 0) & "', " _
                & " '" & vSQL(1, 0) & "', " _
                & " " & Str(nVehID) & ", " _
                & " " & Str(K_CAD_Disp) & ", " _
                & " '" & Format(StartTime, "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & Format(StartTime, "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & Format(StartTime, "MMM DD YYYY HH:NN:SS") & "', " _
                & " '" & vSQL(2, 0) & "', " _
                & " '" & vSQL(3, 0) & "', " _
                & " '" & vSQL(4, 0) & "', " _
                & " 0, 'Simulator', " & Str(nPrimaryFlag) & " ) " _
                & " Select @@IDENTITY x IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION "
    
        nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
        nID = aSQL(0, 0)
        
        Call NewCreateFormattedNumberBySP("R", nID, CStr(rSQL(4, 0)))
        
        '   Complete Assignment by updating RMI record
        
        If nPrimaryFlag = 1 Then
        
            cSQL = "BEGIN TRANSACTION " _
                    & " UPDATE Response_Master_Incident " _
                    & " SET WhichQueue = 'A', " _
                    & "     Time_First_Unit_Assigned = '" & Format(Now, "MMM DD YYYY HH:NN:SS") & "' " _
                    & " WHERE ID = " & Str(nIncID) _
                    & " IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
                    
            nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
        
        End If
        
        '   Update the activity Log
        
        cLoc = rSQL(0, 0)
        nLat = rSQL(7, 0)
        nLon = rSQL(8, 0)
        
        Call MDTUpdateActivityLog(Format(StartTime, "MMM DD YYYY HH:NN:SS"), nVehID, cRadioName, cActivity, cLoc, "SIM LOADTEST", "NULL", nLat, nLon, 0, "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL")
        
        cXMLMsg = MDTBuildXML244(nVehID, nOldStatusID, K_CAD_Disp, nIncID, Format(StartTime, "MMM DD YYYY HH:NN:SS"), Str(nPostID))
        
        If gUseXMLPort Then
            Call goCADServer.SendMessageXML(NP_READ_RESPONSE_COMMENTS, Trim(Str(nIncID)), False, False)
            Call goCADServer.SendMessageXML(NP_ADD_DISPLAY_COMMENT_NOTE, Trim(Str(nIncID)), False, False)
            Call goCADServer.SendMessageXML(NP_READ_RESPONSE_MASTER_INCIDENT, Trim(Str(nIncID)), False, False)
            Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(Str(nVehID)), False, False)
            Call goCADServer.SendMessageXML(NP_READ_RESPONSE_VEHICLESASSIGN, Trim(Str(nIncID)), False, False)
            Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)
        Else
            Call goCADServer.SendMessage(NP_READ_RESPONSE_COMMENTS, Trim(Str(nIncID)), False, False)
            Call goCADServer.SendMessage(NP_ADD_DISPLAY_COMMENT_NOTE, Trim(Str(nIncID)), False, False)
            Call goCADServer.SendMessage(NP_READ_RESPONSE_MASTER_INCIDENT, Trim(Str(nIncID)), False, False)
            Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(Str(nVehID)), False, False)
            Call goCADServer.SendMessage(NP_READ_RESPONSE_VEHICLESASSIGN, Trim(Str(nIncID)), False, False)
            Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)
        End If
        
        Call AVLDebug("AssignUnits: " & Str(nIncID) & " assigned to " & vSQL(1, 0), gDetailedDebugging)
        
    Else
            
        Call AVLDebug("ERROR: AssignUnits: " & Str(nIncID) & " Not Found", True)
        
    End If
    
    Exit Function
    
AssignUnits_ERH:
    Call AVLDebug("ERROR: AssignUnits " & Str(nIncID), True)
    Call AVLDebug("  ERR: " & Str(Err.Number) & " Desc: " & Err.Description & " Source: " & Err.Source, True)
End Function

Public Function SetUnitAssignedToPost(nVehID As Long)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim cRadioName As String
    Dim cLoc As String
    Dim nLat As Long, nLon As Long
    Dim nID As Long
    Dim nOldStatusID As Long
    Dim nPostID As Long
    Dim cXMLMsg As String
    
    On Error GoTo SetUnitAssignedToPost_ERH

    cSQL = "SELECT s.ID, s.Name, s.City, s.lat, s.lon, v.Name, v.Status_ID FROM Stations s JOIN Vehicle v on s.ID = v.Post_Station_ID where v.ID = " & Str(nVehID)
    
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    If nRows > 0 Then
    
        nPostID = aSQL(0, 0)
        cLoc = aSQL(1, 0)
        nLat = aSQL(3, 0)
        nLon = aSQL(4, 0)
        cRadioName = aSQL(5, 0)
        nOldStatusID = aSQL(6, 0)
        
        cSQL = "UPDATE Vehicle " _
                & "SET Destination_Location = '" & aSQL(1, 0) & "', " _
                & "    Destination_City = '" & aSQL(2, 0) & "', " _
                & "    Destination_Lat = " & aSQL(3, 0) & ", " _
                & "    Destination_Lon = " & aSQL(4, 0) & ", " _
                & "    Status_ID = " & Str(K_CAD_ATP) _
                & " WHERE ID = " & Str(nVehID)

        nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
        Call MDTUpdateActivityLog(Format(Now, "MMM DD YYYY HH:NN:SS"), nVehID, cRadioName, "POST", cLoc, "SIM LOADTEST", "NULL", nLat, nLon, 0, "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL")
        
        cXMLMsg = MDTBuildXML244(nVehID, nOldStatusID, K_CAD_ATP, 0, Format(Now, "MMM DD YYYY HH:NN:SS"), Str(nPostID))
        
        If gUseXMLPort Then
            Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(Str(nVehID)), False, False)
            Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)
        Else
            Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(Str(nVehID)), False, False)
            Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)
        End If
        
        Call AVLDebug("SetUnitAssignedToPost: " & Str(nVehID) & " assigned to " & cLoc, gDetailedDebugging)
        
    End If
    
    Exit Function
    
SetUnitAssignedToPost_ERH:
    Call AVLDebug("ERROR in SetUnitAssignedToPost: " & Str(nVehID) & " assigned to " & cLoc & " > " & Err.Description & " (" & Err.Number & ")", gDetailedDebugging)
End Function

Public Function SetUnitAvailableStatus(nVehID As Long)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim rSQL As Variant
    Dim nRows As Long
    Dim n As Integer
    Dim cRadioName As String
    Dim cLoc As String
    Dim nLat As Long, nLon As Long
    Dim nID As Long
    Dim nOldStatusID As Long
    Dim nRMIID As Long
    Dim nUnitID As Long
    Dim cPostStationID As String
    Dim cXMLMsg As String
    Dim nRVAUnitStatus As Integer
    Dim nVehiclesAssigned As Integer
    
    On Error GoTo SetUnitAvailableStatus_ERH

    '   Get ALL units assigned to the incident (so we know whether to cancel the incident or not (after the last unit)

    cSQL = "SELECT rm.ID, rv.Radio_Name, rv.Vehicle_ID, rv.ID, v.Status_ID, v.Current_Lat, v.Current_Lon, v.Post_Station_ID, rv.Status_ID " _
            & " FROM Response_Master_Incident rm " _
            & "     JOIN Response_Vehicles_Assigned rv ON rm.ID = rv.Master_Incident_ID " _
            & "     JOIN Vehicle v on v.Master_Incident_ID = rm.ID " _
            & " WHERE v.ID = " & Str(nVehID)
    
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    Call AVLDebug("SetUnitAvailableStatus: Getting record - " & Str(nVehID) & " assigned to " & Trim(Str(aSQL(0, 0))) & vbCrLf & " >>> " & cSQL, gDetailedDebugging)
    
    If nRows > 0 Then
    
        nVehiclesAssigned = 0
    
        For n = 0 To nRows - 1

            nRMIID = aSQL(0, n)
            cRadioName = aSQL(1, n)
            nUnitID = aSQL(2, n)
            nOldStatusID = aSQL(4, n)
            nLat = aSQL(5, n)
            nLon = aSQL(6, n)
            cPostStationID = aSQL(7, n)
            nRVAUnitStatus = aSQL(8, 0)
            
            If nRVAUnitStatus <> K_CAD_Avail Then                   '   count the units that are still assigned to the incident
                nVehiclesAssigned = nVehiclesAssigned + 1
            End If
            
            If nUnitID = nVehID Then            '   set the current unit to AVAILABLE
            
                cSQL = "UPDATE Vehicle " _
                        & "SET Master_Incident_ID = NULL, " _
                        & "    Status_ID = " & Str(K_CAD_Avail) _
                        & " WHERE ID = " & aSQL(2, n)
        
                nRows = FFADOPut(cSQL, rSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
            
                Call AVLDebug("SetUnitAvailableStatus: Vehicle record updated - " & aSQL(2, n), gDetailedDebugging)
        
                cSQL = "UPDATE Response_Vehicles_Assigned " _
                        & "SET Fixed_Time_Call_Cleared = '" & Format(Now, "MMM DD YYYY HH:NN:SS") & "', " _
                        & "    Time_Call_Cleared = '" & Format(Now, "MMM DD YYYY HH:NN:SS") & "', " _
                        & "    Status_ID = " & Str(K_CAD_Avail) _
                        & " WHERE ID = " & aSQL(3, n)
        
                nRows = FFADOPut(cSQL, rSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
                
                Call AVLDebug("SetUnitAvailableStatus: RVA record updated - " & aSQL(3, n), gDetailedDebugging)
        
                Call MDTUpdateActivityLog(Format(Now, "MMM DD YYYY HH:NN:SS"), nUnitID, cRadioName, "CALL CLOSED", "", "SIM LOADTEST", "NULL", nLat, nLon, nRMIID, "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL")
                
                Call AVLDebug("SetUnitAvailableStatus: Activity Log updated", gDetailedDebugging)
                
                cXMLMsg = MDTBuildXML244(nUnitID, nOldStatusID, K_CAD_Avail, nRMIID, Format(Now, "MMM DD YYYY HH:NN:SS"), cPostStationID)
                
                Call AVLDebug("SetUnitAvailableStatus: XML244=" & cXMLMsg, gDetailedDebugging)
                
                If gUseXMLPort Then
                    Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(Str(nUnitID)), False, False)
                    Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)
                    Call goCADServer.SendMessageXML(NP_READ_RESPONSE_VEHICLESASSIGN, Trim(Str(aSQL(0, n))), False, False)
                    Call goCADServer.SendMessageXML(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(Str(nUnitID)) & ";" & aSQL(0, n), False, False)
                Else
                    Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(Str(nUnitID)), False, False)
                    Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)
                    Call goCADServer.SendMessage(NP_READ_RESPONSE_VEHICLESASSIGN, Trim(Str(aSQL(0, n))), False, False)
                    Call goCADServer.SendMessage(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(Str(nUnitID)) & ";" & aSQL(0, n), False, False)
                End If
            
                Call AVLDebug("SetUnitAvailableStatus: " & Str(nUnitID) & " assigned to " & Trim(Str(aSQL(0, 0))), gDetailedDebugging)
            End If
        Next n
        
        Call AVLDebug("SetUnitAvailableStatus: " & Str(nVehiclesAssigned) & " units assigned to " & Trim(Str(aSQL(0, 0))), gDetailedDebugging)
        
        If nVehiclesAssigned <= 1 Then       '   if this is the LAST unit on the incident, then we need to close the incident, as well
        
            cSQL = "UPDATE Response_Master_Incident " _
                    & "SET Fixed_Time_CallClosed = '" & Format(Now, "MMM DD YYYY HH:NN:SS") & "', " _
                    & "    Time_CallClosed = '" & Format(Now, "MMM DD YYYY HH:NN:SS") & "', " _
                    & "    TimeFirstCallCleared = '" & Format(Now, "MMM DD YYYY HH:NN:SS") & "', " _
                    & "    CallClearedPerfBy = 'LoadTest Simulator', " _
                    & "    WhichQueue = ' ', " _
                    & "    Call_Is_Active = 0 " _
                    & " WHERE ID = " & aSQL(0, 0)
    
            nRows = FFADOPut(cSQL, rSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
            
            Call AVLDebug("SetUnitAvailableStatus: LAST UNIT - RMI record closed - " & aSQL(0, 0), gDetailedDebugging)
            
            Call MDTUpdateActivityLog(Format(Now, "MMM DD YYYY HH:NN:SS"), nVehID, cRadioName, "CALL CLOSED", cLoc, "SIM LOADTEST", "NULL", nLat, nLon, 0, "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL")
            
            If gUseXMLPort Then
                Call goCADServer.SendMessageXML(NP_REMOVE_RESPONSE, Trim(Str(aSQL(0, 0))), False, False)
                Call goCADServer.SendMessageXML(NP_READ_RESPONSE_MASTER_INCIDENT, Trim(Str(aSQL(0, 0))), False, False)
            Else
                Call goCADServer.SendMessage(NP_REMOVE_RESPONSE, Trim(Str(aSQL(0, 0))), False, False)
                Call goCADServer.SendMessage(NP_READ_RESPONSE_MASTER_INCIDENT, Trim(Str(aSQL(0, 0))), False, False)
            End If
        End If
    End If
    
    Exit Function
    
SetUnitAvailableStatus_ERH:
    Call AVLDebug("ERROR in SetUnitAvailableStatus: " & Str(nVehID) & " assigned to " & aSQL(0, 0) & " > " & Err.Description & " (" & Err.Number & ")", gDetailedDebugging)
End Function

Public Function GetStatusID(ByVal nField As Integer) As Integer
'    & " rv.time_assigned, " _                  8
'    & " rv.time_enroute, " _                   9
'    & " rv.time_ArrivedAtScene, " _            10
'    & " rt.time_Depart_Scene, " _              11
'    & " rt.time_arrive_destination, " _        12
'    & " rv.time_delayed_availability, " _      13
'    & " rv.time_call_cleared, "                14

'    Public Const K_CAD_OffDuty = 0
'    Public Const K_CAD_Avail = 1
'    Public Const K_CAD_ATPOST = 2
'    Public Const K_CAD_LocalArea = 3
'    Public Const K_CAD_AvailOS = 4
'    Public Const K_CAD_OOS = 5
'    Public Const K_CAD_Disp = 6
'    Public Const K_CAD_RESPONDING = 7
'    Public Const K_CAD_Enr2Post = 8
'    Public Const K_CAD_Staged = 9
'    Public Const K_CAD_ATSCENE = 10
'    Public Const K_CAD_PtContact = 11
'    Public Const K_CAD_DEPARTSCENE = 12
'    Public Const K_CAD_ATDESTINATION = 13
'    Public Const K_CAD_DELAYEDAVAILABLE = 14
'    Public Const K_CAD_AssignEval = 15
'    Public Const K_CAD_ShiftPend = 16
'    Public Const K_CAD_ATP = 17
'    Public Const K_CAD_MultiAssign = 18
'    Public Const K_CAD_D2L = 19
'    Public Const K_CAD_R2L = 20
'    Public Const K_CAD_A2L = 21

    Select Case nField
        Case 8
            GetStatusID = K_CAD_Disp
        Case 9
            GetStatusID = K_CAD_RESPONDING
        Case 10
            GetStatusID = K_CAD_ATSCENE
        Case 11
            GetStatusID = K_CAD_DEPARTSCENE
        Case 12
            GetStatusID = K_CAD_ATDESTINATION
        Case 13
            GetStatusID = K_CAD_DELAYEDAVAILABLE
        Case 14
            GetStatusID = K_CAD_Avail
        Case Else
    End Select
End Function

Private Sub CADMonSchedWaitingCallLate(nNewIncID As Long)
    Dim cSQL As String
    Dim aParams As Variant
    Dim aParamTypes As Variant
    Dim nRows As Long
    Dim aSQL As Variant
    Dim nID As Long
    
    '   We need to add USP_CADMonSchedWaitingCallLate    @MasterIncidentID
    
    cSQL = "USP_CADMonSchedWaitingCallLate"
    aParams = Array(nNewIncID)
    aParamTypes = Array(1)
    
    nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog, , True, , aParams, aParamTypes)

    Call AVLDebug("CADMonSchedWaitingCallLate: Setting timer for " & Str(nNewIncID) & ".", True)

End Sub

Private Sub CADMonMarkWaitingCallInQueue(nNewIncID As Long)
    Dim cSQL As String
    Dim aParams As Variant
    Dim aParamType As Variant
    Dim nRows As Long
    Dim aSQL As Variant
    Dim nID As Long
    
    '   We need to add USP_CADMonMarkWaitingCallInQueue    @MasterIncidentID, '1'
    
    cSQL = "USP_CADMonMarkWaitingCallInQueue"
    aParams = Array(nNewIncID, 1)
    aParamType = Array(1, 1)
    
    nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog, , True, , aParams, aParamType)

    Call AVLDebug("CADMonMarkWaitingCallInQueue: Setting timer for " & Str(nNewIncID) & ".", True)

End Sub

Private Sub CADMonSchedWaitingCallInQueueWarning(nNewIncID As Long)
    Dim cSQL As String
    Dim aParams As Variant
    Dim aParamType As Variant
    Dim nRows As Long
    Dim aSQL As Variant
    Dim nID As Long
    
    '   We need to add USP_CADMonSchedWaitingCallInQueueWarning    @MasterIncidentID
    
    cSQL = "USP_CADMonSchedWaitingCallInQueueWarning"
    aParams = Array(nNewIncID)
    aParamType = Array(1)
    
    nRows = FFADOPut(cSQL, aSQL, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog, , True, , aParams, aParamType)

    Call AVLDebug("CADMonSchedWaitingCallInQueueWarning: Setting timer for " & Str(nNewIncID) & ".", True)

End Sub

Private Function FormatXML_NP_READ_RESPONSE_FIELDS(nIncNum As Long, Optional bUseXML As Boolean = False) As String
    Dim cXMLMsg As String
    
    cXMLMsg = Trim(Str(nIncNum)) & ",Problem;" & Trim(Str(nIncNum)) _
                        & ",Response_Master_Incident,Response_Plan,ResponsePlanType,Priority_Description,Priority_Number"
    If bUseXML Then
        cXMLMsg = cXMLMsg & Chr(K_CHR_EOT) & "<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'" & vbCrLf _
                        & "xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'" & vbCrLf _
                        & vbTab & "xmlns:rs='urn:schemas-microsoft-com:rowset'" & vbCrLf _
                        & vbTab & "xmlns:z='#RowsetSchema'>" & vbCrLf _
                        & "<s:Schema id='RowsetSchema'>" & vbCrLf _
                        & vbTab & "<s:ElementType name='row' content='eltOnly'>" & vbCrLf _
                        & vbTab & vbTab & "<s:AttributeType name='Response_Plan' rs:number='1'" & vbCrLf _
                        & vbTab & vbTab & vbTab & "rs:nullable='true' rs:writeunknown='true'>" & vbCrLf _
                        & vbTab & vbTab & vbTab & "<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='30'/>" & vbCrLf _
                        & vbTab & vbTab & "</s:AttributeType>" & vbCrLf _
                        & vbTab & vbTab & "<s:AttributeType name='ResponsePlanType' rs:number='2'" & vbCrLf _
                        & vbTab & vbTab & vbTab & "rs:nullable='true' rs:writeunknown='true'>" & vbCrLf _
                        & vbTab & vbTab & vbTab & "<s:datatype dt:type='ui1' dt:maxLength='1' rs:precision='3'" & vbCrLf _
                        & vbTab & vbTab & vbTab & "rs:fixedlength='true'/>" & vbCrLf _
                        & vbTab & vbTab & "</s:AttributeType>" & vbCrLf
        cXMLMsg = cXMLMsg & vbTab & vbTab & "<s:AttributeType name='Priority_Description' rs:number='3'" & vbCrLf _
                        & vbTab & vbTab & vbTab & "rs:nullable='true' rs:writeunknown='true'>" & vbCrLf _
                        & vbTab & vbTab & vbTab & "<s:datatype dt:type='string' rs:dbtype='str' dt:maxLength='30'/>" & vbCrLf _
                        & vbTab & vbTab & "</s:AttributeType>" & vbCrLf _
                        & vbTab & vbTab & "<s:AttributeType name='Priority_Number' rs:number='4'" & vbCrLf _
                        & vbTab & vbTab & vbTab & "rs:nullable='true' rs:writeunknown='true'>" & vbCrLf _
                        & vbTab & vbTab & vbTab & "<s:datatype dt:type='int' dt:maxLength='4' rs:precision='10'" & vbCrLf _
                        & vbTab & vbTab & vbTab & "rs:fixedlength='true'/>" & vbCrLf _
                        & vbTab & vbTab & "</s:AttributeType>" & vbCrLf _
                        & vbTab & vbTab & "<s:extends type='rs:rowbase'/>" & vbCrLf _
                        & vbTab & "</s:ElementType>" & vbCrLf _
                        & "</s:Schema>" & vbCrLf _
                        & "<rs:data>" & vbCrLf _
                        & "</rs:data>" & vbCrLf _
                        & "</xml>"
    End If
            
    FormatXML_NP_READ_RESPONSE_FIELDS = cXMLMsg
    
End Function
