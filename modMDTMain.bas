Attribute VB_Name = "modMDTMain"
Option Explicit

Public Type typStreetInfo
    Name As String
    Speed As Integer
    OneWay As Integer
End Type

Public Type typVEHInfo
    ID As Long
    Name As String
    Current_Location As String
    Current_City As String
    Destination_Location As String
    Destination_City As String
    Status_ID As String
    Status_Code As String
    Current_Lat As Long
    Current_Lon As Long
    Destination_Lat As Long
    Destination_Lon As Long
    IsActive As Boolean
    MDTEnabled As Boolean
    AVL_ID As Long
    UnitName_ID As String
    CurrentDivision_ID As String
    Master_Incident_ID As Long
    MST_MDT_ID As Long
    Last_Station_ID As String
    UnitName As String
    Post_Station_ID As String
    Battalion_ID As String
    Agency_ID As String
    Jurisdiction_ID As String
End Type

Public Type typRMIInfo
    ID As Long
    Master_Incident_Number As String
    Response_Date As String
    Agency_Type As String
    Jurisdiction As String
    Division As String
    Battalion As String
    Station As String
    Response_Area As String
    Response_Time_Criteria As String
    Response_Plan As String
    Incident_Type As String
    Problem As String
    Priority_Number As String
    Priority_Description As String
    Location_Name As String
    Address As String
    Apartment As String
    City As String
    State As String
    Postal_Code As String
    County As String
    Location_Type As String
    Longitude As String
    Latitude As String
    Map_Info As String
    Cross_Street As String
    Time_First_Unit_Enroute As String
    Time_First_Unit_Arrived As String
    Elapsed_Assigned2FirstEnroute As String
    Elapsed_Enroute2FirstAtScene As String
    Determinant As String
    ProQa_CaseNumber As String
    ANI_Number As String
    Confirmation_Number As String
    Incident_Name As String
    Building As String
    Base_Response_Number As String
End Type

Public Type typRVAInfo
    ID As Long
    Master_Incident_ID As Long
    Agency_Type As String
    Jurisdiction As String
    Division As String
    Battalion As String
    Station As String
    Radio_Name As String
    Vehicle_ID As Long
    Time_Assigned As String
    Time_Enroute As String
    Time_ArrivedAtScene As String
    Time_Staged As String
    Time_Delayed_Availability As String
    Elapsed_Assigned_2_Enroute As String
    Elapsed_Enroute_2_Arrival As String
    Fixed_Time_Enroute As String
    Fixed_Time_ArrivedAtScene As String
    Fixed_Time_Staged As String
    Fixed_Time_DelayedAvailable As String
    EnrouteToScene_Performed_By As String
    ArrivedAtScene_Performed_By As String
    Staging_Performed_By As String
    Odometer_Enroute As String
    Odometer_AtScene As String
    Location_At_Assign_Time As String
    Longitude_At_Assign_Time As String
    Latitude_At_Assign_Time As String
    Vehicle_Info_ID As String
    Number_Of_Victims_Seen As String
    Refusal_Number As String
    Time_Avail_AtScene As String
    Fixed_Time_Avail_AtScene As String
    Avail_AtScene_Performed_By As String
    Response_Number As String
    Status_ID As String
    TimeStatusChanged As String
End Type

Public Type typRTRInfo
    ID As Long
    Master_Incident_ID As Long
    Vehicle_Assigned_ID As Long
    Location_Name As String
    Address As String
    Apartment As String
    City As String
    State As String
    Postal_Code As String
    County As String
    Longitude As Long
    Latitude As Long
    Time_Depart_Scene As String
    Time_Arrive_Destination As String
    Fixed_Time_Depart_Scene As String
    Fixed_Time_Arrive_Destination As String
    Fixed_Time_DelayedAvailability As String
    Departure_Performed_By As String
    ArrivedAtDest_Performed_By As String
    Odometer_At_Scene As String
    Odometer_At_Destination As String
    Transport_Mileage As String
    Transport_Mode As String
    Transport_Protocol As String
    Assisted_By As String
    Phone As String
    Map_Info As String
    Building As String
    Time_PickupPromised As String
End Type

Public Type typRPRInfo
    ID As Long
    Master_Incident_ID As Long
    PreScheduled_ID As Long
    Time_CallTaken As String
    Time_PickupPromised As String
    Time_PickupRequested As String
    Name_First As String
    Name_Last As String
    Name_MI As String
    Authorization_Number As String
    Patient_Info_ID As Long
    Return_Master_Incident_ID As Long
    Date_Of_Birth As String
    Social_Security As String
End Type

Public Type typTPCState
    stateID As Long
    StateTime As String
    userID As Integer
    JobID As Long
    State As Integer
    stateType As Integer
    stateName As String
    Latitude As Long ' Lat = Int(Abs(nLat) * 1000000)
    Longitude As Long ' Long = Int(Abs(nLon) * 1000000)
    bHasForm As Boolean
    formID As Long
    odometer As Single
    RunNumber As String
    VehID As Long
    FormName As String
    DestinationCode As String       '   Added BM 071403  -  Destination Code from the Simulator Side
End Type

Public Type typCallForm
    RunNumber As Long
    ResponseGrid As String
    Priority As String
    Address As String
    City As String
    LocationName As String
    ZipCode As String
    Apartment As String
    ProblemType As String
    PatientSSN As String
    PatientName As String
    PatientDOB As String
    TranDestName As String
    TranDestAddress As String
    Hospital As String
    NumOfPatients As Integer
    TransportMode As String
    TransportPriority As String
    DestLocationCAD As String
    DestAddress As String
    DestCity As String
    DestApartment As String
    DestBuilding As String
    DestPhone As String
    DestZipCode As String
    DestState As String
    DestCounty As String
    DestCrossStreet As String
    DestLatitude As Long
    DestLongitude As Long
End Type

Public Type typLogLine
    MSTID As String
    UnitName As String
    Activity As String
    MsgData As String
End Type

Public gtVEHInfo As typVEHInfo
Public gtRMIInfo As typRMIInfo
Public gtRVAInfo As typRVAInfo
Public gtRTRInfo As typRTRInfo
Public gtRPRInfo As typRPRInfo
Public gtTPCState As typTPCState
Public gtCallForm As typCallForm
Public gtLogLine As typLogLine

Public gtRMISendInfo As typRMIInfo
Public gtRTRSendInfo As typRTRInfo

Public gcMyInits As String
Public gcMyName As String

Public gcFormNumber As String
Public gcFormRevision As String

Dim saStatusMap(19, 1) As Integer
Dim saRMIWatch(0 To 49) As Long

Public glCADBusy As Boolean
Public glTPCBusy As Boolean
Public gcDemoTrucks As String
Public glUseInterfaceTime As Boolean
Public gnSlowClockThreshold As Integer

Public gnTotMsgs As Long
Public gnSaved As Long

Public glIsCommand As Boolean

Public Const EV_AVL = 1                '   Event AVL Update
Public Const EV_STAT = 2               '   Event STATUS Update
Public Const EV_INC = 3                '   Event INCIDENT Creation
Public Const EV_TRANS = 4              '   Event Transporting (Destination) report
Public Const EV_RADIO = 5              '   Event Radio Message from Unit
Public Const EV_E911 = 6               '   Send ANIALI Message using the RMI.ID
Public Const EV_INIT_OFF = 7           '   Begin Intitialization - Units Off Duty
Public Const EV_INIT_ON = 8            '   Step 2 Intitialization - Units ON Duty
Public Const EV_INIT_INC = 9           '   Step 3 Intitialization - Link Incidents and Units
Public Const EV_INIT_DONE = 10         '   Step 4 Intitialization Complete
Public Const EV_ANIALI = 11            '   Event Send ANIALI message using the ANIALI table ID
Public Const EV_LOADTEST_ASSIGN = 12   '   Load Test Assign Unit to Incident
Public Const EV_LOADTEST_AVAIL = 13    '   Load Test Set Unit to Available
Public Const EV_LOADTEST_POST = 14     '   Load Test Assign Unit to Home Station Post
Public Const EV_KILL = 9999            '   Shut down

Public Const NP_NEW_RESPONSE = 35
Public Const NP_READ_VEHICLE = 20
Public Const NP_READ_RESPONSE_MASTER_INCIDENT = 37
Public Const NP_READ_RESPONSE_VEHICLEASSIGN = 45
Public Const NP_READ_RESPONSE_TRANSPORT = 46
Public Const NP_READ_RESPONSE_TRANSPORTLEGS = 47
Public Const NP_READ_MULTI_ASSIGN_VEHICLE = 234
Public Const NP_READ_RESPONSE_LATE = 122
Public Const NP_XML244 = 244
Public Const NP_XML245 = 245

Public Const NP_READ_RESPONSE_FIELDS = 123                          '   new with ~v4.5
Public Const NP_READ_RESPONSE_EDIT = 125                            '   new with ~v4.5
Public Const NP_CHANGED_RESPONSE_PROBLEMNATURECHANGE = 236          '   new with ~v4.5
Public Const NP_READ_RESPONSE_CHANGED_RESPONSEPLAN = 330            '   new with ~v4.5

Public Const K_CAD_OffDuty = 0
Public Const K_CAD_Avail = 1
Public Const K_CAD_ATPOST = 2
Public Const K_CAD_LocalArea = 3
Public Const K_CAD_AvailOS = 4
Public Const K_CAD_OOS = 5
Public Const K_CAD_Disp = 6
Public Const K_CAD_RESPONDING = 7
Public Const K_CAD_Enr2Post = 8
Public Const K_CAD_Staged = 9
Public Const K_CAD_ATSCENE = 10
Public Const K_CAD_PtContact = 11
Public Const K_CAD_DEPARTSCENE = 12
Public Const K_CAD_ATDESTINATION = 13
Public Const K_CAD_DELAYEDAVAILABLE = 14
Public Const K_CAD_AssignEval = 15
Public Const K_CAD_ShiftPend = 16
Public Const K_CAD_ATP = 17
Public Const K_CAD_MultiAssign = 18
Public Const K_CAD_D2L = 19
Public Const K_CAD_R2L = 20
Public Const K_CAD_A2L = 21

Public Const K_CHR_EOT = 4          '   marker for start of XML portion of message

Function MDTReadSettings() As Boolean
Dim clsINIFile As clsIniTFiles
Dim cValue As String
Dim nMap As Integer
Dim cFilename As String
Dim cName As String
Dim nPos As Integer

    Set clsINIFile = New clsIniTFiles

    clsINIFile.InitFileName = App.Path & "\" & App.EXEName & ".ini"

    clsINIFile.SectionName = "Settings"
    gcSQLServerDT = clsINIFile.ReadKey("SQLServer_DT", "")
    gcDTSQLLogin = clsINIFile.ReadKey("DTSQLLogin", "sa")
    gcDTSQLPassword = clsINIFile.ReadKey("DTSQLPassword", "")
    gcDemoTrucks = Trim(clsINIFile.ReadKey("DemoTrucks", ""))
    
    clsINIFile.SectionName = "UDP_Switch"
    gcUDPSwitchAddress = clsINIFile.ReadKey("Address", "127.0.0.1")
    gnUDPSwitchPort = Val(clsINIFile.ReadKey("Port", "6789"))
    
    clsINIFile.SectionName = "Interface"
    gcMyName = clsINIFile.ReadKey("Name", "MDT Interface")
    gcMyInits = clsINIFile.ReadKey("Initials", "mdt")
    glUseInterfaceTime = IIf(clsINIFile.ReadKey("UseInterfaceTime", "1") = 1, True, False)
    gnSlowClockThreshold = Val(clsINIFile.ReadKey("SlowClockThreshold", "15"))
        
    clsINIFile.SectionName = "Form"
    gcFormNumber = clsINIFile.ReadKey("Number", "1")
    gcFormRevision = clsINIFile.ReadKey("Revision", "1")
        
    clsINIFile.SectionName = "Status_Mapping"
    
    nMap = 1
    cName = "Map_" & Trim(nMap)
    cValue = clsINIFile.ReadKey(cName, "*END*")
    
    Do While cValue <> "*END*"
    
        nPos = InStr(cValue, ",")
        If nPos <> 0 Then
            saStatusMap(nMap - 1, 0) = Val(Left(cValue, nPos - 1))
            saStatusMap(nMap - 1, 1) = Val(Right(cValue, Len(cValue) - nPos))
        End If
        
        nMap = nMap + 1
    
        cName = "Map_" & Trim(nMap)
        cValue = clsINIFile.ReadKey(cName, "*END*")
        
    Loop
      
    ' We check to see if the TriTech VisiCAD INI file exists, and if
    ' so, we grab the SQL server from there
    cFilename = Left(gSystemDirectory, 2) & "\tritech\visicad\data\system\system.ini"
    If Len(Dir(cFilename)) <> 0 Then
        clsINIFile.InitFileName = cFilename
        clsINIFile.SectionName = "SystemPreferences"
        gcSQLServerCAD = clsINIFile.ReadKey("SQLServerName", "")
        MDTReadSettings = True
    Else
        Call FFLogWrite(ghErrorLog, "ERROR - Cannot find (" & cFilename & ")", True)
        Call MsgBox("ERROR - Cannot find needed file" & vbCrLf & cFilename, vbCritical + vbOKOnly, "ERROR")
        MDTReadSettings = False
    End If
    
End Function

Public Function MDTAddDispatchWatch(nRVAID As Long)
Static nNextFree As Integer

    saRMIWatch(nNextFree) = nRVAID
    
    nNextFree = nNextFree + 1
    
    If nNextFree > 49 Then
        nNextFree = 0
    End If

End Function
Public Function MDTCheckDispatchWatch(nRVAID As Long) As Boolean
Dim x As Integer

    MDTCheckDispatchWatch = False
    x = 0
    
    Do While x <= 49
    
        If saRMIWatch(x) = nRVAID Then
            saRMIWatch(x) = 0
            MDTCheckDispatchWatch = True
            Exit Do
        End If
        
        x = x + 1
    
    Loop

End Function

Public Function MDTGetVEHInfo(nMSTID As Long, tVEHInfo As typVEHInfo) As Boolean
Dim cSQL As String
Dim aArray As Variant
Dim nRows As Long

    On Error GoTo ErrorHandler

    cSQL = "SELECT DISTINCT VEH.ID, VEH.Name, VEH.Current_Location, VEH.Current_City, " & _
                "VEH.Destination_Location, VEH.Destination_City, VEH.Status_ID, " & _
                "VEH.Current_Lat, VEH.Current_Lon, VEH.Destination_Lat, VEH.Destination_Lon, " & _
                "VEH.IsActive, VEH.MDTEnabled, VEH.AVL_ID, " & _
                "VEH.UnitName_ID, VEH.CurrentDivision_ID, VEH.Master_Incident_ID, " & _
                "VEH.Last_Station_ID, " & _
                "VEH.Post_Station_ID, UNN.Code, STA.Code, SBL.BattalionID " & _
                "FROM Vehicle VEH " & _
                "LEFT JOIN Unit_Names UNN ON VEH.UnitName_ID = UNN.ID " & _
                "LEFT JOIN Status STA ON STA.ID = VEH.Status_ID " & _
                "LEFT JOIN StationBattalionList SBL ON SBL.StationID = VEH.Post_Station_ID " & _
                "WHERE " & IIf(gForceMST, "VEH.ID", "VEH.MST_MDT_ID") & " = " & Trim(nMSTID)
        
    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
    
        tVEHInfo.ID = Val(aArray(0, 0))
        tVEHInfo.Name = aArray(1, 0)
        tVEHInfo.Current_Location = FFADOFixString((aArray(2, 0)), True)
        tVEHInfo.Current_City = aArray(3, 0)
        tVEHInfo.Destination_Location = FFADOFixString((aArray(4, 0)), True)
        tVEHInfo.Destination_City = aArray(5, 0)
        tVEHInfo.Status_ID = aArray(6, 0)
        tVEHInfo.Current_Lat = Val(aArray(7, 0))
        tVEHInfo.Current_Lon = Val(aArray(8, 0))
        tVEHInfo.Destination_Lat = Val(aArray(9, 0))
        tVEHInfo.Destination_Lon = Val(aArray(10, 0))
        tVEHInfo.IsActive = IIf(aArray(11, 0) = "1", True, False)
        tVEHInfo.MDTEnabled = IIf(aArray(12, 0) = "1", True, False)
        tVEHInfo.AVL_ID = Val(aArray(13, 0))
        tVEHInfo.UnitName_ID = aArray(14, 0)
        tVEHInfo.CurrentDivision_ID = aArray(15, 0)
        tVEHInfo.Master_Incident_ID = Val(aArray(16, 0))
        tVEHInfo.MST_MDT_ID = nMSTID
        tVEHInfo.Last_Station_ID = aArray(17, 0)
        tVEHInfo.Post_Station_ID = aArray(18, 0)
        tVEHInfo.UnitName = aArray(19, 0)
        tVEHInfo.Status_Code = aArray(20, 0)
        tVEHInfo.Battalion_ID = aArray(21, 0)
        
        ' Possibly read the Agency, Jurisdiction and Division tables at
        ' start of program, eliminating the need for this query on what
        ' is almost certainly static data
        cSQL = "SELECT JUR.ID, AGN.ID FROM Division DIV " & _
                "LEFT JOIN Jurisdiction JUR ON JUR.ID = DIV.JurisdictionID " & _
                "LEFT JOIN AgencyTypes AGN ON AGN.ID = JUR.AgencyID " & _
                "WHERE DIV.ID = " & Trim(tVEHInfo.CurrentDivision_ID)
        
        nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
        
        tVEHInfo.Jurisdiction_ID = aArray(0, 0)
        tVEHInfo.Agency_ID = aArray(1, 0)
        
        MDTGetVEHInfo = True
    
    Else
    
        MDTGetVEHInfo = False
        
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTGetVEHInfo (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
    
End Function

Public Function MDTGetRMIInfo(nRMIID As Long, tRMIInfo As typRMIInfo) As Boolean
Dim cSQL As String
Dim aArray As Variant
Dim nRows As Long

    On Error GoTo ErrorHandler
        
    cSQL = "SELECT RMI.ID,RMI.Master_Incident_Number,RMI.Response_Date,RMI.Agency_Type,RMI.Jurisdiction," & _
                "RMI.Division,RMI.Battalion,RMI.Station,RMI.Response_Area,RMI.Response_Time_Criteria," & _
                "RMI.Response_Plan,RMI.Incident_Type,RMI.Problem,RMI.Priority_Number," & _
                "RMI.Priority_Description,RMI.Location_Name,RMI.Address,RMI.Apartment,RMI.City," & _
                "RMI.State,RMI.Postal_Code,RMI.County,RMI.Location_Type,RMI.Longitude,RMI.Latitude," & _
                "RMI.Map_Info,RMI.Cross_Street,RMI.Time_First_Unit_Enroute,RMI.Time_First_Unit_Arrived," & _
                "RMI.Elapsed_Assigned2FirstEnroute,RMI.Elapsed_Enroute2FirstAtScene,RMI.Determinant," & _
                "RMI.ProQa_CaseNumber,RMI.ANI_Number,RMI.Confirmation_Number,RMI.Incident_Name," & _
                "RMI.Building,RMI.Base_Response_Number " & _
            "FROM Response_Master_Incident RMI WHERE RMI.ID = " & Trim(nRMIID)

    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
    
        tRMIInfo.ID = aArray(0, 0)
        tRMIInfo.Master_Incident_Number = aArray(1, 0)
        tRMIInfo.Response_Date = aArray(2, 0)
        tRMIInfo.Agency_Type = aArray(3, 0)
        tRMIInfo.Jurisdiction = aArray(4, 0)
        tRMIInfo.Division = aArray(5, 0)
        tRMIInfo.Battalion = aArray(6, 0)
        tRMIInfo.Station = aArray(7, 0)
        tRMIInfo.Response_Area = aArray(8, 0)
        tRMIInfo.Response_Time_Criteria = aArray(9, 0)
        tRMIInfo.Response_Plan = aArray(10, 0)
        tRMIInfo.Incident_Type = aArray(11, 0)
        tRMIInfo.Problem = aArray(12, 0)
        tRMIInfo.Priority_Number = aArray(13, 0)
        tRMIInfo.Priority_Description = aArray(14, 0)
        tRMIInfo.Location_Name = FFADOFixString((aArray(15, 0)), True)
        tRMIInfo.Address = FFADOFixString((aArray(16, 0)), True)
        tRMIInfo.Apartment = aArray(17, 0)
        tRMIInfo.City = aArray(18, 0)
        tRMIInfo.State = aArray(19, 0)
        tRMIInfo.Postal_Code = aArray(20, 0)
        tRMIInfo.County = aArray(21, 0)
        tRMIInfo.Location_Type = aArray(22, 0)
        tRMIInfo.Longitude = aArray(23, 0)
        tRMIInfo.Latitude = aArray(24, 0)
        tRMIInfo.Map_Info = aArray(25, 0)
        tRMIInfo.Cross_Street = FFADOFixString((aArray(26, 0)), True)
        tRMIInfo.Time_First_Unit_Enroute = aArray(27, 0)
        tRMIInfo.Time_First_Unit_Arrived = aArray(28, 0)
        tRMIInfo.Elapsed_Assigned2FirstEnroute = aArray(29, 0)
        tRMIInfo.Elapsed_Enroute2FirstAtScene = aArray(30, 0)
        tRMIInfo.Determinant = aArray(31, 0)
        tRMIInfo.ProQa_CaseNumber = aArray(32, 0)
        tRMIInfo.ANI_Number = aArray(33, 0)
        tRMIInfo.Confirmation_Number = aArray(34, 0)
        tRMIInfo.Incident_Name = aArray(35, 0)
        tRMIInfo.Building = aArray(36, 0)
        tRMIInfo.Base_Response_Number = aArray(37, 0)
                
        MDTGetRMIInfo = True
    
    Else
    
        MDTGetRMIInfo = False
        
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTGetRMIInfo (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
    
End Function

Public Function MDTGetRVAInfo(nRMIID As Long, nVehID As Long, tRVAInfo As typRVAInfo) As Boolean
Dim cSQL As String
Dim aArray As Variant
Dim nRows As Long
Dim nUse As Long

    On Error GoTo ErrorHandler
        
    cSQL = "SELECT RVA.ID,RVA.Master_Incident_ID,RVA.Agency_Type,RVA.Jurisdiction,RVA.Division,RVA.Battalion," & _
                "RVA.Station,RVA.Radio_Name,RVA.Vehicle_ID,RVA.Time_Assigned,RVA.Time_Enroute,RVA.Time_ArrivedAtScene," & _
                "RVA.Time_Staged,RVA.Time_Delayed_Availability,RVA.Elapsed_Assigned_2_Enroute," & _
                "RVA.Elapsed_Enroute_2_Arrival,RVA.Fixed_Time_Enroute,RVA.Fixed_Time_ArrivedAtScene," & _
                "RVA.Fixed_Time_Staged,RVA.Fixed_Time_DelayedAvailable,RVA.EnrouteToScene_Performed_By," & _
                "RVA.ArrivedAtScene_Performed_By,RVA.Staging_Performed_By,RVA.Odometer_Enroute," & _
                "RVA.Odometer_AtScene,RVA.Location_At_Assign_Time,RVA.Longitude_At_Assign_Time," & _
                "RVA.Latitude_At_Assign_Time,RVA.Vehicle_Info_ID,RVA.Number_Of_Victims_Seen," & _
                "RVA.Refusal_Number,RVA.Time_Avail_AtScene,RVA.Fixed_Time_Avail_AtScene," & _
                "RVA.Avail_AtScene_Performed_By,RVA.Response_Number"
    
    If glIsCommand Then
        cSQL = cSQL & ",RVA.Status_ID,RVA.TimeStatusChanged"
    End If
    
    cSQL = cSQL & " FROM Response_Vehicles_Assigned RVA " & _
            "WHERE RVA.Master_Incident_ID = " & Trim(nRMIID) & _
                " AND RVA.Vehicle_ID = " & Trim(nVehID) & _
                " AND RVA.Time_Call_Cleared IS NULL"

    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
    
        ' We want to use the last row returned.
        ' We should only ever return 1 row, but in case we return more,
        ' this will set nUse to the last row.
        nUse = nRows - 1
    
        ' If more than 1 row is returned, we have a problem!
        If nRows > 1 Then
            Call AVLDebug("ERROR - Expected 1 row from RVA query, got " & Trim(nRows), True)
        End If
        
        tRVAInfo.ID = aArray(0, nUse)
        tRVAInfo.Master_Incident_ID = aArray(1, nUse)
        tRVAInfo.Agency_Type = aArray(2, nUse)
        tRVAInfo.Jurisdiction = aArray(3, nUse)
        tRVAInfo.Division = aArray(4, nUse)
        tRVAInfo.Battalion = aArray(5, nUse)
        tRVAInfo.Station = aArray(6, nUse)
        tRVAInfo.Radio_Name = aArray(7, nUse)
        tRVAInfo.Vehicle_ID = aArray(8, nUse)
        tRVAInfo.Time_Assigned = aArray(9, nUse)
        tRVAInfo.Time_Enroute = aArray(10, nUse)
        tRVAInfo.Time_ArrivedAtScene = aArray(11, nUse)
        tRVAInfo.Time_Staged = aArray(12, nUse)
        tRVAInfo.Time_Delayed_Availability = aArray(13, nUse)
        tRVAInfo.Elapsed_Assigned_2_Enroute = aArray(14, nUse)
        tRVAInfo.Elapsed_Enroute_2_Arrival = aArray(15, nUse)
        tRVAInfo.Fixed_Time_Enroute = aArray(16, nUse)
        tRVAInfo.Fixed_Time_ArrivedAtScene = aArray(17, nUse)
        tRVAInfo.Fixed_Time_Staged = aArray(18, nUse)
        tRVAInfo.Fixed_Time_DelayedAvailable = aArray(19, nUse)
        tRVAInfo.EnrouteToScene_Performed_By = aArray(20, nUse)
        tRVAInfo.ArrivedAtScene_Performed_By = aArray(21, nUse)
        tRVAInfo.Staging_Performed_By = aArray(22, nUse)
        tRVAInfo.Odometer_Enroute = aArray(23, nUse)
        tRVAInfo.Odometer_AtScene = aArray(24, nUse)
        tRVAInfo.Location_At_Assign_Time = FFADOFixString((aArray(25, nUse)), True)
        tRVAInfo.Longitude_At_Assign_Time = aArray(26, nUse)
        tRVAInfo.Latitude_At_Assign_Time = aArray(27, nUse)
        tRVAInfo.Vehicle_Info_ID = aArray(28, nUse)
        tRVAInfo.Number_Of_Victims_Seen = aArray(29, nUse)
        tRVAInfo.Refusal_Number = aArray(30, nUse)
        tRVAInfo.Time_Avail_AtScene = aArray(31, nUse)
        tRVAInfo.Fixed_Time_Avail_AtScene = aArray(32, nUse)
        tRVAInfo.Avail_AtScene_Performed_By = aArray(33, nUse)
        tRVAInfo.Response_Number = aArray(34, nUse)
        
        If Trim(tRVAInfo.Response_Number) = "" Then
            tRVAInfo.Response_Number = "NA"
            Call AVLDebug("WARNING - Empty RESPONSE_NUMBER for RVAID = " & Trim(tRVAInfo.ID), True)
        End If
        
        If glIsCommand Then
            tRVAInfo.Status_ID = aArray(35, nUse)
            tRVAInfo.TimeStatusChanged = aArray(36, nUse)
        Else
            tRVAInfo.Status_ID = ""
            tRVAInfo.TimeStatusChanged = ""
        End If

        MDTGetRVAInfo = True
    
    Else
    
        MDTGetRVAInfo = False
        
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTGetRVAInfo (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
    
End Function

Public Function MDTGetRTRInfo(nRMIID As Long, nRVAID As Long, nPINID As Long, tRTRInfo As typRTRInfo) As Boolean
Dim cSQL As String
Dim aArray As Variant
Dim nRows As Long
Dim nUse As Long
Dim cDebugMsg As String

    On Error GoTo ErrorHandler
        
    cDebugMsg = "Assign SQL"
    cSQL = "SELECT RTR.ID,RTR.Master_Incident_ID,RTR.Vehicle_Assigned_ID,RTR.Location_Name,RTR.Address," & _
                "RTR.Apartment,RTR.City,RTR.State,RTR.Postal_Code,RTR.County,RTR.Longitude,RTR.Latitude," & _
                "RTR.Time_Depart_Scene,RTR.Time_Arrive_Destination,RTR.Fixed_Time_Depart_Scene," & _
                "RTR.Fixed_Time_Arrive_Destination,Fixed_Time_DelayedAvailability,RTR.Departure_Performed_By,RTR.ArrivedAtDest_Performed_By," & _
                "RTR.Odometer_At_Scene,RTR.Odometer_At_Destination,RTR.Transport_Mileage,RTR.Transport_Mode," & _
                "RTR.Transport_Protocol,RTR.Assisted_By,RTR.Phone,RTR.Map_Info,RTR.Building,RTR.Time_PickupPromised " & _
            "FROM Response_Transports RTR WHERE RTR.Master_Incident_ID = " & Trim(nRMIID)
            
    cDebugMsg = "IF nRVA ID"
    If nRVAID <> 0 Then
        cSQL = cSQL & " AND RTR.Vehicle_Assigned_ID = " & Trim(nRVAID)
    End If
    
    cDebugMsg = "IF nPIN ID"
    If nPINID <> 0 Then
        cSQL = cSQL & " AND RTR.Patient_Info_ID = " & Trim(nPINID)
    End If
    
    ' *CHANGE*
    ' We need to add a WHERE clause to the above because we COULD get more than 1 record for multiple patients
    ' Figure out another condition to make sure we only get one.

    cDebugMsg = "FFADOGet"
    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
        
        ' We want to use the last row returned.
        ' We should only ever return 1 row, but in case we return more,
        ' this will set nUse to the last row.
        nUse = nRows - 1
    
        ' If more than 1 row is returned, we have a problem!
        If nRows > 1 Then
            Call AVLDebug("WARNING - Expected 1 row from RTR query, got " & Trim(nRows) & "   RTR.Master_Incident_ID = " & Trim(nRMIID), True)
        End If
            
        cDebugMsg = "Assign Values"
        
        tRTRInfo.ID = Val(aArray(0, nUse))
        tRTRInfo.Master_Incident_ID = Val(aArray(1, nUse))
        tRTRInfo.Vehicle_Assigned_ID = Val(aArray(2, nUse))
        tRTRInfo.Location_Name = FFADOFixString((aArray(3, nUse)), True)
        tRTRInfo.Address = FFADOFixString((aArray(4, nUse)), True)
        tRTRInfo.Apartment = aArray(5, nUse)
        tRTRInfo.City = aArray(6, nUse)
        tRTRInfo.State = aArray(7, nUse)
        tRTRInfo.Postal_Code = aArray(8, nUse)
        tRTRInfo.County = aArray(9, nUse)
        tRTRInfo.Longitude = Val(aArray(10, nUse))
        tRTRInfo.Latitude = Val(aArray(11, nUse))
        tRTRInfo.Time_Depart_Scene = aArray(12, nUse)
        tRTRInfo.Time_Arrive_Destination = aArray(13, nUse)
        tRTRInfo.Fixed_Time_Depart_Scene = aArray(14, nUse)
        tRTRInfo.Fixed_Time_Arrive_Destination = aArray(15, nUse)
        tRTRInfo.Fixed_Time_DelayedAvailability = aArray(16, nUse)
        tRTRInfo.Departure_Performed_By = aArray(17, nUse)
        tRTRInfo.ArrivedAtDest_Performed_By = aArray(18, nUse)
        tRTRInfo.Odometer_At_Scene = aArray(19, nUse)
        tRTRInfo.Odometer_At_Destination = aArray(20, nUse)
        tRTRInfo.Transport_Mileage = aArray(21, nUse)
        tRTRInfo.Transport_Mode = aArray(22, nUse)
        tRTRInfo.Transport_Protocol = aArray(23, nUse)
        tRTRInfo.Assisted_By = aArray(24, nUse)
        tRTRInfo.Phone = aArray(25, nUse)
        tRTRInfo.Map_Info = aArray(26, nUse)
        tRTRInfo.Building = aArray(27, nUse)
        tRTRInfo.Time_PickupPromised = aArray(28, nUse)

        MDTGetRTRInfo = True
    
    Else
    
        MDTGetRTRInfo = False
        
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTGetRTRInfo (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description & " (" & cDebugMsg & ")", True)
    MDTGetRTRInfo = False
    
End Function

Public Function MDTGetRPRInfo(nRMIID As Long, tRPRInfo As typRPRInfo) As Boolean
Dim cSQL As String
Dim aArray As Variant
Dim nRows As Long

    On Error GoTo ErrorHandler
        
    cSQL = "SELECT RPR.ID, RPR.PreScheduled_ID, RPR.Time_CallTaken, RPR.Time_PickupPromised, " & _
                    "RPR.Time_PickupRequested, RPR.Name_First, RPR.Name_Last, RPR.Name_MI, " & _
                    "RPR.Authorization_Number, RPR.Patient_Info_ID, RPR.Return_Master_Incident_ID, " & _
                    "PIN.Date_Of_Birth, PIN.Social_Security " & _
            "FROM Response_PreScheduled_Info RPR " & _
            "LEFT JOIN Patient_Information PIN ON PIN.ID = RPR.Patient_Info_ID " & _
            "WHERE RPR.Master_Incident_ID = " & Trim(nRMIID)

    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
    
        tRPRInfo.Master_Incident_ID = nRMIID
        tRPRInfo.ID = Val(aArray(0, 0))
        tRPRInfo.PreScheduled_ID = Val(aArray(1, 0))
        tRPRInfo.Time_CallTaken = aArray(2, 0)
        tRPRInfo.Time_PickupPromised = aArray(3, 0)
        tRPRInfo.Time_PickupRequested = aArray(4, 0)
        tRPRInfo.Name_First = aArray(5, 0)
        tRPRInfo.Name_Last = aArray(6, 0)
        tRPRInfo.Name_MI = aArray(7, 0)
        tRPRInfo.Authorization_Number = aArray(8, 0)
        tRPRInfo.Patient_Info_ID = Val(aArray(9, 0))
        tRPRInfo.Return_Master_Incident_ID = Val(aArray(10, 0))
        tRPRInfo.Date_Of_Birth = aArray(11, 0)
        tRPRInfo.Social_Security = aArray(12, 0)

        MDTGetRPRInfo = True
    
    Else
    
        MDTGetRPRInfo = False
        
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTGetRPRInfo (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
    
End Function

Function bsiMDTProcessMDTButtonPush(aAVLMDTMessage) As Boolean
Dim nToStatusID As Long
Dim nFromStatusID As Long
Dim cActivity As String
Dim cMsg As String
Dim tRPRInfo As typRPRInfo
Dim nMSTID As Long
Dim nNumPats As Long
Dim cProtocol As String
Dim cMode As String
Dim nNewStatus As Integer
Dim cDestinationCode As String

    On Error GoTo ErrorHandler
    
    Call MDTClearGlobalTypes                                                '   reset all structures to empty
    
    If UBound(aAVLMDTMessage) > 7 Then
        nMSTID = aAVLMDTMessage(1)                                          '   MDT ID
        nFromStatusID = Val(aAVLMDTMessage(2))                              '   From Status
        nToStatusID = Val(aAVLMDTMessage(3))                                '   To Status
        gtTPCState.Latitude = Int(Abs(Val(aAVLMDTMessage(4))) * 1000000)    '   Latitude
        gtTPCState.Longitude = Int(Abs(Val(aAVLMDTMessage(5))) * 1000000)   '   Longitude
        gtTPCState.StateTime = aAVLMDTMessage(6)                            '   Time of Status Change
        cProtocol = aAVLMDTMessage(7)                                       '   Transport Protocol
        cMode = aAVLMDTMessage(8)                                           '   Transport Mode
        nNumPats = Val(aAVLMDTMessage(9))                                   '   Number of Patients on Transport Record
        gtTPCState.DestinationCode = aAVLMDTMessage(10)                     '   Destination Code
    Else
        nMSTID = aAVLMDTMessage(1)                                          '   MDT ID
        nFromStatusID = Val(aAVLMDTMessage(2))                              '   From Status
        nToStatusID = Val(aAVLMDTMessage(3))                                '   To Status
        gtTPCState.Latitude = Int(Abs(Val(aAVLMDTMessage(4))) * 1000000)    '   Latitude
        gtTPCState.Longitude = Int(Abs(Val(aAVLMDTMessage(5))) * 1000000)   '   Longitude
        gtTPCState.StateTime = aAVLMDTMessage(6)                            '   Time of Status Change
        gtTPCState.DestinationCode = aAVLMDTMessage(7)                      '   Destination Code
    End If
    
    gtLogLine.MSTID = Trim(nMSTID)
    gtLogLine.UnitName = "NA"
    gtLogLine.Activity = "ERROR"

    ' We get all the vehicle info VEH.MSTID = MSTID
    ' If no vehicle found, display error
    If Not MDTGetVEHInfo(nMSTID, gtVEHInfo) Then
        gtLogLine.MsgData = "No vehicle matches MSTID=" & Trim(nMSTID) & " in VisiCAD"
        bsiMDTProcessMDTButtonPush = False
        Exit Function
    End If
    
    '   nFromStatusID = Val(gtVEHInfo.Status_ID)
    
    Call AVLDebug("VEH Status From=" & Str(nFromStatusID) & "  To=" & Str(nToStatusID), gDetailedDebugging)
    
    gtLogLine.UnitName = gtVEHInfo.UnitName
    gtLogLine.MsgData = gtVEHInfo.Destination_Location
    
    ' Check to see if this status change is allowed from the current status
    cActivity = MDTCheckStatusAllowed(nFromStatusID, nToStatusID)
    
    ' We also make sure it's not a AT_POST to AT_POST status change, special case
    If (cActivity = "/*EMPTY*/") Or (nFromStatusID = 2 And nToStatusID = 2) Then
        gtLogLine.Activity = "WARNING"
        gtLogLine.MsgData = "Status change not allowed  From=" & Trim(nFromStatusID) & " To=" & Trim(nToStatusID)
        Call AVLDebug("WARNING: VEH Status From=" & Trim(nFromStatusID) & "  To=" & Trim(nToStatusID) & " NOT ALLOWED!", gDetailedDebugging)
        bsiMDTProcessMDTButtonPush = False
        Exit Function
    End If
    
    gtLogLine.Activity = cActivity
    
    ' We want to accept "AT POST" statuses for vehicles without incidents
    If gtVEHInfo.Master_Incident_ID = 0 And nToStatusID <> K_CAD_ATPOST And nToStatusID <> K_CAD_Enr2Post And nToStatusID <> K_CAD_LocalArea Then
        gtLogLine.MsgData = "No incident for Unit=" & Trim(gtVEHInfo.UnitName)
        Call AVLDebug("WARNING: " & gtLogLine.MsgData, gDetailedDebugging)
        bsiMDTProcessMDTButtonPush = False
        Exit Function
    End If
        
    ' If it's ok, we move ahead and grab other info
    ' If status is ATPOST or Enr2Post , then no call info will be available
    If Not (nToStatusID = K_CAD_ATPOST Or nToStatusID = K_CAD_Enr2Post Or nToStatusID = K_CAD_Enr2Post) Then
    
        ' Ger RMI.Info or RMIID = VEH.RMIID
        Call MDTGetRMIInfo(gtVEHInfo.Master_Incident_ID, gtRMIInfo)
        
        ' Get RVA.Info for RMIID = VEH.RMIID and VEHID = VEH.ID
        ' NOTE: there may be more than 1 RVA record, but we don't care, we only want the one
        '       to do with the current vehicle that did the status push
        Call MDTGetRVAInfo(gtRMIInfo.ID, gtVEHInfo.ID, gtRVAInfo)
        
        gtLogLine.MsgData = "(" & Trim(gtRVAInfo.Response_Number) & ") " & gtRMIInfo.Address
        
    End If
    
    ' Here we do stuff according to what status the truck is in
    Select Case nToStatusID
        Case K_CAD_Enr2Post
            Call MDT_Enr2Post(nFromStatusID, cActivity)
            Call AVLDebug("Process Enroute to POST complete", gDetailedDebugging)
        Case K_CAD_LocalArea
            Call MDT_LOCALAREA(nFromStatusID, cActivity)
            Call AVLDebug("Process LOCAL AREA complete", gDetailedDebugging)
        Case K_CAD_ATPOST
            Call MDT_ATPOST(nFromStatusID, cActivity)
            Call AVLDebug("Process AT POST complete", gDetailedDebugging)
        Case K_CAD_RESPONDING
            Call MDT_RESPONDING(nFromStatusID, cActivity)
            Call AVLDebug("Process RESPONDING complete", gDetailedDebugging)
    '    Case K_CAD_STAGED
    '        Call FFAddToListBox(frmMain.lstDebug, "Process STAGED", , True)
    '        Call MDT_STAGED(nFromStatusID, cActivity)
        Case K_CAD_ATSCENE
            Call MDT_ATSCENE(nFromStatusID, cActivity)
            Call AVLDebug("Process AT SCENE complete", gDetailedDebugging)
        Case K_CAD_DEPARTSCENE
            If bsiMDTGetTruckPCCallForm(nNumPats, cProtocol, cMode) Then
                ' We grab the RTR info here, it will only exist for prescheduled calls
                Call MDTGetRPRInfo(gtRMIInfo.ID, tRPRInfo)
                Call MDTGetRTRInfo(gtRMIInfo.ID, gtRVAInfo.ID, tRPRInfo.Patient_Info_ID, gtRTRInfo)
                If gtRTRInfo.ID <> 0 Then
                    Call MDT_DEPARTSCENE(nFromStatusID, cActivity, True)
                    gtLogLine.MsgData = "(" & Trim(gtRVAInfo.Response_Number) & ") " & gtRTRInfo.Location_Name
                Else
                    Call MDTValidateDestination
                    Call MDT_DEPARTSCENE(nFromStatusID, cActivity)
                    gtLogLine.MsgData = "(" & Trim(gtRVAInfo.Response_Number) & ") " & gtCallForm.DestLocationCAD
                End If
                    
    '            ElseIf MDTValidateDestination() Then
    '                Call MDT_DEPARTSCENE(nFromStatusID, cActivity)
    '                gtLogLine.MsgData = "(" & Trim(gtRVAInfo.Response_Number) & ") " & gtCallForm.DestLocationCAD
    '            Else
    '                gtLogLine.MsgData = "ERROR - Could not validate '" & Trim(gtCallForm.Hospital) & "'"
    '            End If
            
            Else
                gtLogLine.MsgData = "ERROR - No form info for Depart Scene!"
            End If
            Call AVLDebug("Process DEPART SCENE complete", gDetailedDebugging)
        Case K_CAD_ATDESTINATION
            ' Grab the existing transport info
            Call MDTGetRTRInfo(gtRMIInfo.ID, gtRVAInfo.ID, 0, gtRTRInfo)
            Call MDT_ATDESTINATION(nFromStatusID, cActivity)
            gtLogLine.MsgData = "(" & Trim(gtRVAInfo.Response_Number) & ") " & gtRTRInfo.Location_Name ' gtCallForm.DestLocationCAD
            Call AVLDebug("Process AT DESTINATION complete", gDetailedDebugging)
        Case K_CAD_DELAYEDAVAILABLE
            Call MDT_DELAYEDAVAILABLE(nFromStatusID, cActivity)
            Call AVLDebug("Process DELAYED AVAILABLE complete", gDetailedDebugging)
        Case Else
            Call AVLDebug("WARNING - Function MDTProcessMDTButtonPush - Unhandled status change = " & Trim(nToStatusID), gDetailedDebugging)
    End Select

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTProcessMDTButtonPush (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function bsiMDTGetTruckPCCallForm(nNumPatients As Long, cProtocol As String, cMode As String) As Boolean
Dim cSQL As String
Dim nRows As Long
Dim aArray As Variant

    On Error GoTo ErrorHandler

    cSQL = "SELECT Code, Name, Address, City, Zip, Latitude, Longitude " _
            & " FROM Locations WHERE Code = '" & Trim(gtTPCState.DestinationCode) & "' "

    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then

        gtCallForm.Hospital = aArray(0, 0)
        gtCallForm.LocationName = aArray(1, 0)
        gtCallForm.Address = aArray(2, 0)
        gtCallForm.City = aArray(3, 0)
        gtCallForm.ZipCode = aArray(4, 0)
        gtCallForm.NumOfPatients = nNumPatients
        gtCallForm.TransportPriority = MDTGetTransProtocol(cProtocol)
        gtCallForm.TransportMode = MDTGetTransPriority(cMode)
        
        '  **CHANGE: We'll need to simulate the DEST NAME and DEST ADDRESS fields somehow.
        
        Call AVLDebug("Depart Scene NUM PATIENTS = " & Trim(gtCallForm.NumOfPatients), gDetailedDebugging)
        Call AVLDebug("Depart Scene TRANS MODE   = " & Trim(gtCallForm.TransportMode), gDetailedDebugging)
        Call AVLDebug("Depart Scene DEST NAME    = " & Trim(gtCallForm.TranDestName), gDetailedDebugging)
        Call AVLDebug("Depart Scene DEST ADDRESS = " & Trim(gtCallForm.TranDestAddress), gDetailedDebugging)
        
        bsiMDTGetTruckPCCallForm = True
        
    Else
        
        bsiMDTGetTruckPCCallForm = False
    
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function bsiMDTGetTruckPCCallForm (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTGetTransPriority(cCode As String) As String
Dim cSQL As String
Dim nRows As Long
Dim aArray As Variant

    On Error GoTo ErrorHandler
    
    cSQL = "SELECT TPR.Description FROM TransportPriority TPR WHERE TPR.Code = '" & cCode & "'"

    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then

        MDTGetTransPriority = aArray(0, 0)
        
    Else
    
        MDTGetTransPriority = cCode
        
    End If

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTGetTransPriority (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTGetTransProtocol(cCode As String) As String
Dim cSQL As String
Dim nRows As Long
Dim aArray As Variant

    On Error GoTo ErrorHandler
    
    cSQL = "SELECT Description FROM Protocol WHERE Code = '" & cCode & "'"

    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then

        MDTGetTransProtocol = aArray(0, 0)
        
    Else
    
        MDTGetTransProtocol = cCode
        
    End If

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTGetTransProtocol (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTValidateDestination() As Boolean
Dim cSQL As String
Dim nRows As Long
Dim aArray As Variant

    On Error GoTo ErrorHandler

    cSQL = "SELECT LOC.Name, LOC.Address, LOC.City, LOC.Apt, LOC.Bldg, LOC.Phone, " & _
                    "LOC.Zip, LOC.State, LOC.County, LOC.Cross_Street, LOC.Latitude, LOC.Longitude " & _
            "FROM Locations LOC WHERE LOC.Code = '" & gtCallForm.Hospital & "'"

    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
        
        gtCallForm.DestLocationCAD = Replace(aArray(0, 0), "'", "''")       '   deal with apostrophies in the Location field
        gtCallForm.DestAddress = aArray(1, 0)
        gtCallForm.DestCity = aArray(2, 0)
        gtCallForm.DestApartment = aArray(3, 0)
        gtCallForm.DestBuilding = aArray(4, 0)
        gtCallForm.DestPhone = aArray(5, 0)
        gtCallForm.DestZipCode = aArray(6, 0)
        gtCallForm.DestState = aArray(7, 0)
        gtCallForm.DestCounty = aArray(8, 0)
        gtCallForm.DestCrossStreet = aArray(9, 0)
        gtCallForm.DestLatitude = Val(aArray(10, 0))
        gtCallForm.DestLongitude = Val(aArray(11, 0))
        
        MDTValidateDestination = True
        
    Else
        
        gtCallForm.DestLocationCAD = "UNKNOWN DESTINATION"
        gtCallForm.DestAddress = ""
        gtCallForm.DestCity = ""
        gtCallForm.DestApartment = ""
        gtCallForm.DestBuilding = ""
        gtCallForm.DestPhone = ""
        gtCallForm.DestZipCode = ""
        gtCallForm.DestState = ""
        gtCallForm.DestCounty = ""
        gtCallForm.DestCrossStreet = ""
        gtCallForm.DestLatitude = 0
        gtCallForm.DestLongitude = 0

        gtCallForm.NumOfPatients = 1

        MDTValidateDestination = False
        
    End If

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTValidateDestination (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTGetRosteredIDs(nVehID As Long) As String
Dim cSQL As String
Dim nRows As Long
Dim aArray As Variant
Dim cIDs As String
Dim x As Integer

    On Error GoTo ErrorHandler

    cSQL = "SELECT RSC.Vehicle_ID, ESC.Emp_Rec_ID, ESC.PageWithVehicle " & _
                "FROM RosterSchedule RSC " & _
                "LEFT JOIN EmployeeSchedule ESC ON ESC.RosterScheduleID = RSC.RosterScheduleID " & _
                "WHERE RSC.ActiveFlag = 1 AND RSC.Vehicle_ID = " & Trim(nVehID)
            
    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)

    If nRows < 1 Then
        cIDs = " "
    Else
        For x = 0 To nRows - 1
            cIDs = cIDs & Trim(aArray(1, x)) & "^"
        Next x
    End If
    
    MDTGetRosteredIDs = Left(cIDs, Len(cIDs) - 1)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTGetRosteredIDs (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTGetCADStatusID(nTruckPCStatus As Integer) As Long
Dim x As Integer
Dim lFound As Boolean

    lFound = False
    x = 0
    
    Do While x <= 19 And Not lFound
    
        If saStatusMap(x, 0) = nTruckPCStatus Then
            MDTGetCADStatusID = saStatusMap(x, 1)
            lFound = True
        End If
        
        x = x + 1
    
    Loop

End Function

Function MDTCheckStatusAllowed(nFromID As Long, nToID As Long) As String
Dim cSQL As String
Dim nRows As Long
Dim aArray As Variant

    On Error GoTo ErrorHandler

    cSQL = "SELECT SAC.To_Status_ID, STA.Description FROM Status_Actions SAC " & _
            "LEFT JOIN Status STA ON STA.ID = SAC.To_Status_ID " & _
            "WHERE SAC.From_Status_ID = " & Trim(nFromID) & " AND SAC.To_Status_ID = " & Trim(nToID)
            
    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)

    If nRows < 1 Then
        MDTCheckStatusAllowed = "/*EMPTY*/"
        Call AVLDebug("WARNING - Function MDTCheckStatusAllowed: Invalid Status Change from (" & Format(nFromID, "00") & ") to (" & Format(nToID, "00") & ")")
    Else
        MDTCheckStatusAllowed = aArray(1, 0)
    End If

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTCheckStatusAllowed (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDT_Enr2Post(nFromStatusID As Long, cActivity As String)
Dim lIsFirstUpdate As Boolean
Dim cCalcElapsedTime As String
Dim cTimeNow As String
Dim cXMLMsg As String

    On Error GoTo ErrorHandler

    ' update VEHICLE
    Call MDTUpdateVEH_Enr2Post(gtVEHInfo.ID, gtTPCState.StateTime, K_CAD_Enr2Post, nFromStatusID)
        
    ' insert ACTIVITY_LOG
    Call MDTUpdateActivityLog(gtTPCState.StateTime, gtVEHInfo.ID, gtVEHInfo.UnitName, cActivity, _
                                gtVEHInfo.Destination_Location, "", "", gtTPCState.Latitude, gtTPCState.Longitude, _
                                0, gcMyInits, gtVEHInfo.Agency_ID, gtVEHInfo.Jurisdiction_ID, _
                                gtVEHInfo.CurrentDivision_ID, gtVEHInfo.Battalion_ID, gtVEHInfo.Post_Station_ID, _
                                gcComputerName, gtVEHInfo.UnitName)
                                        
    cXMLMsg = MDTBuildXML244(gtVEHInfo.ID, nFromStatusID, K_CAD_Enr2Post, 0, gtTPCState.StateTime, gtVEHInfo.Post_Station_ID)
    
    If gUseXMLPort Then
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(0), False, False)
        Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)
    Else
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(0), False, False)
        Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDT_Enr2Post (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDT_LOCALAREA(nFromStatusID As Long, cActivity As String)
Dim lIsFirstUpdate As Boolean
Dim cCalcElapsedTime As String
Dim cXMLMsg As String

    On Error GoTo ErrorHandler

    ' update VEHICLE
    Call MDTUpdateLocalArea(gtVEHInfo.ID, gtTPCState.StateTime, K_CAD_LocalArea, nFromStatusID)
        
    ' insert ACTIVITY_LOG
    Call MDTUpdateActivityLog(gtTPCState.StateTime, gtVEHInfo.ID, gtVEHInfo.UnitName, cActivity, _
                                gtVEHInfo.Destination_Location, "", "", gtTPCState.Latitude, gtTPCState.Longitude, _
                                0, gcMyInits, gtVEHInfo.Agency_ID, gtVEHInfo.Jurisdiction_ID, _
                                gtVEHInfo.CurrentDivision_ID, gtVEHInfo.Battalion_ID, gtVEHInfo.Post_Station_ID, _
                                gcComputerName, gtVEHInfo.UnitName)
                                        
    cXMLMsg = MDTBuildXML244(gtVEHInfo.ID, nFromStatusID, K_CAD_LocalArea, 0, gtTPCState.StateTime, gtVEHInfo.Post_Station_ID)
    
    If gUseXMLPort Then
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(0), False, False)
        Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)
    Else
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(0), False, False)
        Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDT_LOCALAREA (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDT_ATPOST(nFromStatusID As Long, cActivity As String)
Dim lIsFirstUpdate As Boolean
Dim cCalcElapsedTime As String
Dim cXMLMsg As String

    On Error GoTo ErrorHandler

    ' update VEHICLE
    Call MDTUpdateVEH_ATPOST(gtVEHInfo.ID, gtTPCState.StateTime, K_CAD_ATPOST, nFromStatusID)
        
    ' insert ACTIVITY_LOG
    Call MDTUpdateActivityLog(gtTPCState.StateTime, gtVEHInfo.ID, gtVEHInfo.UnitName, cActivity, _
                                gtVEHInfo.Destination_Location, "", "", gtTPCState.Latitude, gtTPCState.Longitude, _
                                0, gcMyInits, gtVEHInfo.Agency_ID, gtVEHInfo.Jurisdiction_ID, _
                                gtVEHInfo.CurrentDivision_ID, gtVEHInfo.Battalion_ID, gtVEHInfo.Post_Station_ID, _
                                gcComputerName, gtVEHInfo.UnitName)
                                        
    cXMLMsg = MDTBuildXML244(gtVEHInfo.ID, nFromStatusID, K_CAD_ATPOST, 0, gtTPCState.StateTime, gtVEHInfo.Post_Station_ID)
    
    If gUseXMLPort Then
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(0), False, False)
        Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)
    Else
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(0), False, False)
        Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDT_ATPOST (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDT_RESPONDING(nFromStatusID As Long, cActivity As String)
Dim lIsFirstUpdate As Boolean
Dim cCalcElapsedTime As String
Dim cXMLMsg As String

    On Error GoTo ErrorHandler

    ' * RESPONDING *
    ' update VEHICLE
    Call MDTUpdateVEH_RESPONDING(gtVEHInfo.ID, gtTPCState.StateTime, K_CAD_RESPONDING, nFromStatusID)
        
    ' insert ACTIVITY_LOG
    Call MDTUpdateActivityLog(gtTPCState.StateTime, gtVEHInfo.ID, gtVEHInfo.UnitName, cActivity, _
                                gtVEHInfo.Current_Location, "", "", gtTPCState.Latitude, gtTPCState.Longitude, _
                                gtRMIInfo.ID, gcMyInits, gtVEHInfo.Agency_ID, gtVEHInfo.Jurisdiction_ID, _
                                gtVEHInfo.CurrentDivision_ID, gtVEHInfo.Battalion_ID, gtVEHInfo.Post_Station_ID, _
                                gcComputerName, gtVEHInfo.UnitName)
    
    If Trim(gtRVAInfo.Fixed_Time_Enroute) = "" Then
        lIsFirstUpdate = True
    Else
        lIsFirstUpdate = False
    End If
    
    cCalcElapsedTime = Trim(FFElapsed(CDate(gtRVAInfo.Time_Assigned), CDate(gtTPCState.StateTime), , True))
    
    ' update RESPONSE_MASTER_INCIDENT & RESPONSE_VEHICLES_ASSIGNED
    Call MDTUpdateRMIRVA_RESPONDING(gtRMIInfo.ID, gtRVAInfo.ID, lIsFirstUpdate, K_CAD_RESPONDING, _
                                    gtTPCState.StateTime, cCalcElapsedTime, gcMyName, _
                                    gtVEHInfo.Current_Location, gtTPCState.Longitude, _
                                    gtTPCState.Latitude, gtTPCState.StateTime)
                                    
    cXMLMsg = MDTBuildXML244(gtVEHInfo.ID, nFromStatusID, K_CAD_RESPONDING, gtRMIInfo.ID, gtTPCState.StateTime, gtVEHInfo.Post_Station_ID)
    
    If gUseXMLPort Then
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_MASTER_INCIDENT, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_VEHICLEASSIGN, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_LATE, Trim(gtRMIInfo.ID), False, False)
    Else
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_MASTER_INCIDENT, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_VEHICLEASSIGN, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_LATE, Trim(gtRMIInfo.ID), False, False)
    End If

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDT_RESPONDING (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description & " [Activity=" & cActivity & "]", True)

End Function
'Function MDT_STAGED(nFromStatusID As Long, cActivity As String)
'Dim cFixedTimeStaged As String
'
'    On Error GoTo ErrorHandler
'
'    ' * STAGED *
'    ' update VEHICLE
'    Call MDTUpdateVEH_STAGED(gtVEHInfo.ID, gtTPCState.StateTime, K_CAD_RESPONDING, nFromStatusID, _
'                            gtTPCState.Latitude, gtTPCState.Longitude)
'
'    ' insert ACTIVITY_LOG
'    Call MDTUpdateActivityLog(gtTPCState.StateTime, gtVEHInfo.ID, gtVEHInfo.UnitName, cActivity, _
'                                gtVEHInfo.Current_Location, "", "", gtTPCState.Latitude, gtTPCState.Longitude, _
'                                gtRMIInfo.ID, gcMyInits, gtVEHInfo.Agency_ID, gtVEHInfo.Jurisdiction_ID, _
'                                gtVEHInfo.CurrentDivision_ID, gtVEHInfo.Battalion_ID, gtVEHInfo.Post_Station_ID, _
'                                gcComputerName, gtVEHInfo.UnitName)
'
'    If gtRVAInfo.Fixed_Time_Staged = "" Then
'        cFixedTimeStaged = gtTPCState.StateTime
'    Else
'        cFixedTimeStaged = gtRVAInfo.Fixed_Time_Staged
'    End If
'
'    ' update RESPONSE_VEHICLES_ASSIGNED
'    Call MDTUpdateRVA_STAGED(gtRVAInfo.ID, gtTPCState.StateTime, gcMyName, cFixedTimeStaged, _
'                                gtTPCState.StateTime, K_CAD_STAGED)
'
'    Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, True)
'    Call goCADServer.SendMessage(NP_READ_RESPONSE_VEHICLEASSIGN, Trim(gtRMIInfo.ID), False, True)
'    Call goCADServer.SendMessage(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(gtRMIInfo.ID), False, True)
'
'    Exit Function
'
'ErrorHandler:
'
'    Call AVLDebug("ERROR - Function MDT_STAGED (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
'
'End Function

Function MDT_ATSCENE(nFromStatusID As Long, cActivity As String)
Dim lIsFirstUpdate As Boolean
Dim cCalcElapsedTime As String
Dim cXMLMsg As String

    On Error GoTo ErrorHandler

    ' * AT SCENE *
    ' update VEHICLE
    Call MDTUpdateVEH_ATSCENE(gtVEHInfo.ID, gtTPCState.StateTime, K_CAD_ATSCENE, nFromStatusID, gtTPCState.Latitude, _
                                gtTPCState.Longitude, gtRMIInfo.Address, gtRMIInfo.City)
                                
    ' insert ACTIVITY_LOG
    Call MDTUpdateActivityLog(gtTPCState.StateTime, gtVEHInfo.ID, gtVEHInfo.UnitName, cActivity, _
                                gtVEHInfo.Current_Location, "", "", gtTPCState.Latitude, gtTPCState.Longitude, _
                                gtRMIInfo.ID, gcMyInits, gtVEHInfo.Agency_ID, gtVEHInfo.Jurisdiction_ID, _
                                gtVEHInfo.CurrentDivision_ID, gtVEHInfo.Battalion_ID, gtVEHInfo.Post_Station_ID, _
                                gcComputerName, gtVEHInfo.UnitName)
    
    If gtRVAInfo.Fixed_Time_ArrivedAtScene = "" Then
        lIsFirstUpdate = True
    Else
        lIsFirstUpdate = False
    End If
    
    Call AVLDebug("AT SCENE, First Update=" & Trim(lIsFirstUpdate), gDetailedDebugging)
    
    cCalcElapsedTime = Trim(FFElapsed(CDate(gtRVAInfo.Time_Assigned), CDate(gtTPCState.StateTime), , True))
    
    ' update RESPONSE_MASTER_INCIDENT & RESPONSE_VEHICLES_ASSIGNED
    Call MDTUpdateRMIRVA_ATSCENE(gtRMIInfo.ID, gtRVAInfo.ID, lIsFirstUpdate, K_CAD_ATSCENE, gtTPCState.StateTime, _
                                    cCalcElapsedTime, gcMyName, gtTPCState.StateTime, gtTPCState.StateTime)
                                    
    cXMLMsg = MDTBuildXML244(gtVEHInfo.ID, nFromStatusID, K_CAD_ATSCENE, gtRMIInfo.ID, gtTPCState.StateTime, gtVEHInfo.Post_Station_ID)
    
    If gUseXMLPort Then
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_MASTER_INCIDENT, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_VEHICLEASSIGN, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_LATE, Trim(gtRMIInfo.ID), False, False)
    Else
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_MASTER_INCIDENT, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_VEHICLEASSIGN, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_LATE, Trim(gtRMIInfo.ID), False, False)
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDT_ATSCENE (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDT_DEPARTSCENE(nFromStatusID As Long, cActivity As String, Optional lIsSched As Boolean = False)
Dim cFixedTimeDepart As String
Dim nPatientInfoID As Long
Dim cXMLMsg As String

    On Error GoTo ErrorHandler

    ' * DEPART SCENE *

    ' update VEHICLE
    If lIsSched Then
        Call MDTUpdateVEH_DEPARTSCENE(gtVEHInfo.ID, gtTPCState.StateTime, K_CAD_DEPARTSCENE, nFromStatusID, _
                                    gtRTRInfo.Latitude, gtRTRInfo.Longitude, gtRTRInfo.Location_Name, _
                                    gtRTRInfo.City)
    Else
        Call MDTUpdateVEH_DEPARTSCENE(gtVEHInfo.ID, gtTPCState.StateTime, K_CAD_DEPARTSCENE, nFromStatusID, _
                                    gtCallForm.DestLatitude, gtCallForm.DestLongitude, gtCallForm.DestLocationCAD, _
                                    gtCallForm.DestCity)
    End If

    ' insert ACTIVITY_LOG
    Call MDTUpdateActivityLog(gtTPCState.StateTime, gtVEHInfo.ID, gtVEHInfo.UnitName, cActivity, _
                                gtVEHInfo.Current_Location, "", "", gtTPCState.Latitude, gtTPCState.Longitude, _
                                gtRMIInfo.ID, gcMyInits, gtVEHInfo.Agency_ID, gtVEHInfo.Jurisdiction_ID, _
                                gtVEHInfo.CurrentDivision_ID, gtVEHInfo.Battalion_ID, gtVEHInfo.Post_Station_ID, _
                                gcComputerName, gtVEHInfo.UnitName)
                                
    If gtRTRInfo.Fixed_Time_Depart_Scene = "" Then
        cFixedTimeDepart = gtTPCState.StateTime
    Else
        cFixedTimeDepart = gtRTRInfo.Fixed_Time_Depart_Scene
    End If
    
    ' *CHANGE*
    ' Patient info ID will come from scheduled calls
    nPatientInfoID = 0
    
    ' Here we have to check if this is a prescheduled call, indicated by a record
    ' in Response_PreScheduled_Info.  If there is a record, we want to UPDATE
    ' response_transports, not INSERT
    If lIsSched Then
    
        Call AVLDebug("DEPART SCENE - PreSched UPDATE only", gDetailedDebugging)
        ' update RESPONSE_TRANSPORTS
        Call MDTUpdateRTR_DEPARTSCENE_Sched(gtRTRInfo.ID, gtRVAInfo.ID, gtCallForm.DestPhone, gtCallForm.ResponseGrid, "", _
                                gtTPCState.StateTime, gcMyName, cFixedTimeDepart, gtRTRInfo.City, _
                                gtRTRInfo.Location_Name, gtRTRInfo.Address, gtRTRInfo.Apartment, _
                                gtRTRInfo.Latitude, gtRTRInfo.Longitude, gtRTRInfo.State, _
                                gtRTRInfo.Postal_Code, gtRTRInfo.County, gtCallForm.TransportPriority, gtCallForm.TransportMode, _
                                gtRTRInfo.Building, gtCallForm.NumOfPatients)
    
    Else
    
        Call AVLDebug("DEPART SCENE - Emerg INSERT", gDetailedDebugging)
        ' insert REPONSE_TRANSPORTS
        Call MDTUpdateRTR_DEPARTSCENE(gtRMIInfo.ID, gtRVAInfo.ID, nPatientInfoID, gtCallForm.DestPhone, gtCallForm.ResponseGrid, "", _
                                gtTPCState.StateTime, gcMyName, cFixedTimeDepart, gtCallForm.DestCity, _
                                gtCallForm.DestLocationCAD, gtCallForm.DestAddress, gtCallForm.DestApartment, _
                                gtCallForm.DestLatitude, gtCallForm.DestLongitude, "", gtCallForm.DestState, _
                                gtCallForm.DestZipCode, gtCallForm.DestCounty, gtCallForm.TransportPriority, gtCallForm.TransportMode, _
                                gtCallForm.DestBuilding, gtCallForm.NumOfPatients)
                                
    End If
    
    ' update RESPONSE_VEHICLES_ASSIGNED
    Call MDTUpdateRVA_DEPARTSCENE(gtRVAInfo.ID, gtCallForm.NumOfPatients, gtTPCState.StateTime, K_CAD_DEPARTSCENE)
    
    ' Here we need to send TruckPC the destination lat/long for display on their map
    ' Call BLAH!
    
    cXMLMsg = MDTBuildXML244(gtVEHInfo.ID, nFromStatusID, K_CAD_DEPARTSCENE, gtRMIInfo.ID, gtTPCState.StateTime, gtVEHInfo.Post_Station_ID)
    
    If gUseXMLPort Then
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_TRANSPORT, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_TRANSPORTLEGS, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_VEHICLEASSIGN, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_LATE, Trim(gtRMIInfo.ID), False, False)
    Else
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_TRANSPORT, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_TRANSPORTLEGS, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_VEHICLEASSIGN, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_LATE, Trim(gtRMIInfo.ID), False, False)
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDT_DEPARTSCENE (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDT_ATDESTINATION(nFromStatusID As Long, cActivity As String)
Dim cFixedTimeArriveDest As String
Dim cXMLMsg As String

    On Error GoTo ErrorHandler

    ' * AT DESTINATION *
    ' update VEHICLE
    Call MDTUpdateVEH_ATDESTINATION(gtVEHInfo.ID, gtTPCState.StateTime, K_CAD_ATDESTINATION, nFromStatusID, _
                                    gtTPCState.Latitude, gtTPCState.Longitude, gtRTRInfo.Location_Name, _
                                    gtRTRInfo.City)
    
    ' insert ACTIVITY_LOG
    Call MDTUpdateActivityLog(gtTPCState.StateTime, gtVEHInfo.ID, gtVEHInfo.UnitName, cActivity, _
                                gtVEHInfo.Current_Location, "", "", gtTPCState.Latitude, gtTPCState.Longitude, _
                                gtRMIInfo.ID, gcMyInits, gtVEHInfo.Agency_ID, gtVEHInfo.Jurisdiction_ID, _
                                gtVEHInfo.CurrentDivision_ID, gtVEHInfo.Battalion_ID, gtVEHInfo.Post_Station_ID, _
                                gcComputerName, gtVEHInfo.UnitName)
                                
    If gtRTRInfo.Fixed_Time_Arrive_Destination = "" Then
        cFixedTimeArriveDest = gtTPCState.StateTime
    Else
        cFixedTimeArriveDest = gtRTRInfo.Fixed_Time_Arrive_Destination
    End If
    
    ' update RESPONSE_TRANSPORTS
    Call MDTUpdateRTR_ATDESTINATION(gtRMIInfo.ID, gtRVAInfo.ID, gtTPCState.StateTime, gcMyName, _
                                    cFixedTimeArriveDest)
                                    
    ' update RESPONSE_VEHICLES_ASSIGNED
    Call MDTUpdateRVA_ATDESTINATION(gtRVAInfo.ID, gtTPCState.StateTime, K_CAD_ATDESTINATION)
        
    cXMLMsg = MDTBuildXML244(gtVEHInfo.ID, nFromStatusID, K_CAD_ATDESTINATION, gtRMIInfo.ID, gtTPCState.StateTime, gtVEHInfo.Post_Station_ID)
        
    If gUseXMLPort Then
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_VEHICLEASSIGN, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_TRANSPORT, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_TRANSPORTLEGS, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_LATE, Trim(gtRMIInfo.ID), False, False)
    Else
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_VEHICLEASSIGN, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_TRANSPORT, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_TRANSPORTLEGS, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_LATE, Trim(gtRMIInfo.ID), False, False)
    End If
    
    ' Here we write out the page log file
    ' Call MDTSendAtDestPage

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDT_ATDESTINATION (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDT_DELAYEDAVAILABLE(nFromStatusID As Long, cActivity As String)
Dim cFixedTimeDelayAvail As String
Dim cXMLMsg As String

    On Error GoTo ErrorHandler

    ' * DELAYED AVAILABLE / CODE 5 *
    ' update VEHICLE
    Call MDTUpdateVEH_DELAYEDAVAILABLE(gtVEHInfo.ID, gtTPCState.StateTime, K_CAD_DELAYEDAVAILABLE, nFromStatusID)

    ' insert ACTIVITY_LOG
    Call MDTUpdateActivityLog(gtTPCState.StateTime, gtVEHInfo.ID, gtVEHInfo.UnitName, cActivity, _
                                gtVEHInfo.Current_Location, "", "", gtTPCState.Latitude, gtTPCState.Longitude, _
                                gtRMIInfo.ID, gcMyInits, gtVEHInfo.Agency_ID, gtVEHInfo.Jurisdiction_ID, _
                                gtVEHInfo.CurrentDivision_ID, gtVEHInfo.Battalion_ID, gtVEHInfo.Post_Station_ID, _
                                gcComputerName, gtVEHInfo.UnitName)

    If gtRTRInfo.Fixed_Time_DelayedAvailability = "" Then
        cFixedTimeDelayAvail = gtTPCState.StateTime
    Else
        cFixedTimeDelayAvail = gtRTRInfo.Fixed_Time_DelayedAvailability
    End If

    ' update RESPONSE_TRANSPORTS
    Call MDTUpdateRTR_DELAYEDAVAILABLE(gtRMIInfo.ID, gtRVAInfo.ID, gtTPCState.StateTime, _
                                        gcMyName, cFixedTimeDelayAvail)

    If gtRVAInfo.Fixed_Time_DelayedAvailable = "" Then
        cFixedTimeDelayAvail = gtTPCState.StateTime
    Else
        cFixedTimeDelayAvail = gtRVAInfo.Fixed_Time_DelayedAvailable
    End If

    ' update RESPONSE_VEHICLES_ASSIGNED
    Call MDTUpdateRVA_DELAYEDAVAILABLE(gtRVAInfo.ID, gtTPCState.StateTime, gcMyName, cFixedTimeDelayAvail, _
                                        gtTPCState.StateTime, K_CAD_DELAYEDAVAILABLE)

    cXMLMsg = MDTBuildXML244(gtVEHInfo.ID, nFromStatusID, K_CAD_DELAYEDAVAILABLE, gtRMIInfo.ID, gtTPCState.StateTime, gtVEHInfo.Post_Station_ID)
    
    If gUseXMLPort Then
        Call goCADServer.SendMessageXML(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_TRANSPORT, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_TRANSPORTLEGS, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_RESPONSE_VEHICLEASSIGN, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessageXML(NP_XML244, Trim(cXMLMsg), False, False)
    Else
        Call goCADServer.SendMessage(NP_READ_VEHICLE, Trim(gtVEHInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_TRANSPORT, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_TRANSPORTLEGS, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_RESPONSE_VEHICLEASSIGN, Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_READ_MULTI_ASSIGN_VEHICLE, Trim(gtVEHInfo.ID) & ";" & Trim(gtRMIInfo.ID), False, False)
        Call goCADServer.SendMessage(NP_XML244, Trim(cXMLMsg), False, False)
    End If
    
    Exit Function

ErrorHandler:

    Call AVLDebug("ERROR - Function MDT_DELAYEDAVAILABLE (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateVEH_Enr2Post(nVehID As Long, cDateTime As String, nToStatusID As Long, nFromStatusID As Long)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler
   
    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE VEHICLE SET STATUS_ID = " & Trim(nToStatusID) & ", Elapsed_Time = '" & cDateTime & "', " & _
                "Saved_Status_ID = Status_ID " & _
            "WHERE ID = " & Trim(nVehID) & " AND Status_ID = " & Trim(nFromStatusID) & " " & _
            "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
                
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    Call AVLDebug("UpdateVEH_Enr2Post  Rows=" & Trim(nRows) & "  ID=" & Trim(nID), gDetailedDebugging)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateVEH_Enr2Post (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateLocalArea(nVehID As Long, cDateTime As String, nToStatusID As Long, nFromStatusID As Long)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler
   
    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE VEHICLE SET STATUS_ID = " & Trim(nToStatusID) & ", Elapsed_Time = '" & cDateTime & "', " & _
                "Destination_Location = NULL, Destination_City = NULL, Destination_Lon = 0, Destination_Lat = 0, " & _
                "Current_Lat = " & Str(gtTPCState.Latitude) & ", Current_Lon = " & Str(gtTPCState.Longitude) & ", " & _
                "Current_Location = Destination_Location, Current_City = Destination_City, " & _
                "Saved_Status_ID = Status_ID " & _
            "WHERE ID = " & Trim(nVehID) & " AND Status_ID = " & Trim(nFromStatusID) & " " & _
            "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
                
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    Call AVLDebug("UpdateLocalArea Rows=" & Trim(nRows) & "  ID=" & Trim(nID), gDetailedDebugging)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateLocalArea (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateVEH_ATPOST(nVehID As Long, cDateTime As String, nToStatusID As Long, nFromStatusID As Long)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler
   
    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE VEHICLE SET STATUS_ID = " & Trim(nToStatusID) & ", Elapsed_Time = '" & cDateTime & "', " & _
                "Destination_Location = NULL, Destination_City = NULL, Destination_Lon = 0, Destination_Lat = 0, " & _
                "Current_Lat = " & Str(gtTPCState.Latitude) & ", Current_Lon = " & Str(gtTPCState.Longitude) & ", " & _
                "Current_Location = Destination_Location, Current_City = Destination_City, " & _
                "Saved_Status_ID = Status_ID " & _
            "WHERE ID = " & Trim(nVehID) & " AND Status_ID = " & Trim(nFromStatusID) & " " & _
            "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
                
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    Call AVLDebug("UpdateVEH_ATPOST  Rows=" & Trim(nRows) & "  ID=" & Trim(nID), gDetailedDebugging)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateVEH_ATPOST (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateVEH_RESPONDING(nVehID As Long, cDateTime As String, nToStatusID As Long, nFromStatusID As Long)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler
   
    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE VEHICLE SET STATUS_ID = " & Trim(nToStatusID) & ", Elapsed_Time = '" & cDateTime & "', " & _
                "Saved_Status_ID = Status_ID " & _
            "WHERE ID = " & Trim(nVehID) & " AND Status_ID = " & Trim(nFromStatusID) & " " & _
            "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
                
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateVEH_RESPONDING (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function
'Function MDTUpdateVEH_STAGED(nVEHID As Long, cDateTime As String, nToStatusID As Long, nFromStatusID As Long, _
'                                nCurLat As Long, nCurLon As Long)
'Dim cSQL As String
'Dim nRows As Long
'Dim nID As Long
'
'    On Error GoTo ErrorHandler
'
'' BEGIN TRANSACTION UPDATE VEHICLE SET STATUS_ID = 9, Elapsed_Time='Feb 09 2002 19:48:06',
'' Current_Lon=79216427, Current_Lat=43774405, Saved_Status_ID = Status_ID
'' WHERE ID=82 AND STATUS_ID =7 IF @@error =0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION
'
'    cSQL = "BEGIN TRANSACTION " & _
'            "UPDATE VEHICLE SET STATUS_ID = " & Trim(nToStatusID) & ", Elapsed_Time = '" & cDateTime & "', " & _
'                "Current_Lon = " & Trim(nCurLon) & ", Current_Lat = " & Trim(nCurLat) & ", " & _
'                "Saved_Status_ID = Status_ID " & _
'            "WHERE ID = " & Trim(nVEHID) & " AND Status_ID = " & Trim(nFromStatusID) & " " & _
'            "IF @@error =0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
'
'    nRows = goCADServer.UpdateSQL(cSQL, nID)
'
'    Exit Function
'
'ErrorHandler:
'
'    Call AVLDebug("ERROR - Function MDTUpdateVEH_STAGED (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
'
'End Function

Function MDTUpdateVEH_ATSCENE(nVehID As Long, cDateTime As String, nToStatusID As Long, nFromStatusID As Long, _
                                nCurLat As Long, nCurLon As Long, _
                                cCurLocation As String, cCurCity As String)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler

    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE VEHICLE SET STATUS_ID = " & Trim(nToStatusID) & ", Elapsed_Time = '" & cDateTime & "', " & _
                "Destination_Lat = 0, Destination_Lon = 0, " & _
                "Current_Location = '" & Left(cCurLocation, 30) & "', Current_City = '" & Left(cCurCity, 20) & "', " & _
                "Current_Lon = " & Trim(nCurLon) & ", Current_Lat = " & Trim(nCurLat) & ", " & _
                "Destination_Location = NULL, Destination_City = NULL, " & _
                "Saved_Status_ID = Status_ID " & _
            "WHERE ID = " & Trim(nVehID) & " AND Status_ID = " & Trim(nFromStatusID) & " " & _
            "IF @@error =0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"

    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateVEH_ATSCENE (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateVEH_DEPARTSCENE(nVehID As Long, cDateTime As String, nToStatusID As Long, nFromStatusID As Long, _
                                nDestLat As Long, nDestLon As Long, _
                                cDestLocation As String, cDestCity As String)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler
    
    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE VEHICLE SET STATUS_ID = " & Trim(nToStatusID) & ", Elapsed_Time = '" & cDateTime & "', " & _
                "Destination_Lat = " & Trim(nDestLat) & ", Destination_Lon = " & Trim(nDestLon) & ", " & _
                "Destination_Location = '" & Trim(cDestLocation) & "', Destination_City = '" & Trim(cDestCity) & "', " & _
                "Saved_Status_ID = Status_ID " & _
            "WHERE ID = " & Trim(nVehID) & " AND Status_ID = " & Trim(nFromStatusID) & " " & _
            "IF @@error =0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"

    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateVEH_DEPARTSCENE (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateVEH_ATDESTINATION(nVehID As Long, cDateTime As String, nToStatusID As Long, nFromStatusID As Long, _
                                nCurLat As Long, nCurLon As Long, _
                                cCurLocation As String, cCurCity As String)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler

    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE VEHICLE SET STATUS_ID = " & Trim(nToStatusID) & ", Elapsed_Time = '" & cDateTime & "', " & _
                "Current_Location = '" & Left(cCurLocation, 30) & "', Current_City = '" & Left(cCurCity, 20) & "', " & _
                "Current_Lon = " & Trim(nCurLon) & ", Current_Lat = " & Trim(nCurLat) & ", " & _
                "Destination_Lat = 0, Destination_Lon = 0, " & _
                "Destination_Location = NULL, Destination_City = NULL, " & _
                "Saved_Status_ID = Status_ID " & _
            "WHERE ID = " & Trim(nVehID) & " AND Status_ID = " & Trim(nFromStatusID) & " " & _
            "IF @@error =0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"

    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateVEH_ATDESTINATION (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateVEH_DELAYEDAVAILABLE(nVehID As Long, cDateTime As String, nToStatusID As Long, nFromStatusID As Long)
Dim cSQL As String
Dim nRows As Long
Dim aArrayPut As Variant
Dim nID As Long

    On Error GoTo ErrorHandler

    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE VEHICLE SET STATUS_ID = " & Trim(nToStatusID) & ", Elapsed_Time = '" & cDateTime & "', " & _
                "Saved_Status_ID = Status_ID " & _
            "WHERE ID = " & Trim(nVehID) & " AND Status_ID = " & Trim(nFromStatusID) & " " & _
            "IF @@error =0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"

    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    Exit Function

ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateVEH_DELAYEDAVAILABLE (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateActivityLog(cDate_Time As String, nVehicle_ID As Long, cRadio_Name As String, _
                            cActivity As String, cLocation As String, cComment As String, _
                            cPersonnel_ID As String, nLatitude As Long, nLongitude As Long, _
                            nMaster_Incident_ID As Long, cDispatcher_Init As String, _
                            cAgency_ID As String, cJurisdiction_ID As String, _
                            cDivision_ID As String, cBattalion_ID As String, cStation_ID As String, _
                            cTerminal As String, cRadio_Code As String)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler

    cSQL = "BEGIN TRANSACTION " & _
            "INSERT INTO Activity_Log (Date_Time, Vehicle_ID, Radio_Name, Activity, Location, " & _
                "Comment, Personnel_ID, Latitude, Longitude, Master_Incident_id, Dispatcher_Init, " & _
                "Agency_ID, Jurisdiction_ID, Division_ID, Battalion_ID, Station_ID, Terminal, Radio_Code) " & _
            "VALUES ('" & cDate_Time & "'," & _
                    nVehicle_ID & "," & _
                    "'" & cRadio_Name & "'," & _
                    "'" & Left(cActivity, 20) & "'," & _
                    "'" & Left(Replace(cLocation, "'", "''"), 40) & "'," & _
                    "'" & Left(cComment, 80) & "'," & _
                    "NULL" & "," & _
                    nLatitude & "," & _
                    nLongitude & ","
                    
    If nMaster_Incident_ID = 0 Then
        cSQL = cSQL & "NULL"
    Else
        cSQL = cSQL & Trim(nMaster_Incident_ID)
    End If
    
    cSQL = cSQL & "," & _
            "'" & cDispatcher_Init & "'," & _
            cAgency_ID & "," & _
            cJurisdiction_ID & "," & _
            cDivision_ID & "," & _
            cBattalion_ID & "," & _
            cStation_ID & "," & _
            "'" & cTerminal & "'," & _
            "'" & cRadio_Code & "') " & _
        "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION "
    
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateActivityLog (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateRMIRVA_RESPONDING(nRMIID As Long, nRVAID As Long, lIsFirstUpdate As Boolean, _
                                    nStatusID As Long, cEnRouteTime As String, _
                                    cCalcElapsedTime As String, cEnRouteBy As String, _
                                    cLocAtAssign As String, _
                                    nLonAtAssign As Long, nLatAtAssign As Long, _
                                    cTimeStatusChanged As String)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler

    cSQL = "BEGIN TRANSACTION "

    ' See if we need to check for first update and populate
    ' the "one time" fields if so
    If lIsFirstUpdate Then
        cSQL = cSQL & "UPDATE Response_Master_Incident " & _
                        "SET Time_First_Unit_Enroute = '" & cEnRouteTime & "', " & _
                            "Elapsed_Assigned2FirstEnroute='" & cCalcElapsedTime & "' " & _
                            "WHERE ID=" & nRMIID & " "
    End If
    
    cSQL = cSQL & "UPDATE Response_Vehicles_Assigned SET Time_Enroute='" & cEnRouteTime & "'," & _
                        "Enroutetoscene_Performed_By='" & cEnRouteBy & "'"
                        
'                        "Odometer_Enroute=" & Trim(gtTPCState.odometer) & ", "
                        
    If lIsFirstUpdate Then
        cSQL = cSQL & ", Fixed_Time_Enroute='" & cEnRouteTime & "', " & _
                        "Location_At_Assign_Time='" & cLocAtAssign & "', " & _
                        "Longitude_At_Assign_Time=" & nLonAtAssign & ", " & _
                        "Latitude_At_Assign_Time=" & nLatAtAssign
    End If
    
    If glIsCommand Then
                        
        cSQL = cSQL & ", TimeStatusChanged='" & cTimeStatusChanged & "'," & _
                        "Status_ID = " & Trim(nStatusID)
    End If
    
    cSQL = cSQL & " WHERE ID=" & nRVAID & " IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
    
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateRMIRVA_RESPONDING (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateRMIRVA_ATSCENE(nRMIID As Long, nRVAID As Long, lIsFirstUpdate As Boolean, _
                                    nStatusID As Long, cArrivedTime As String, _
                                    cCalcElapsedTime As String, cArrivedBy As String, _
                                    cFixedArrivedTime As String, cTimeStatusChanged As String)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler
    
    cSQL = "BEGIN TRANSACTION "

    ' See if we need to check for first update and populate
    ' the "one time" fields if so
    If lIsFirstUpdate Then
        cSQL = cSQL & "UPDATE Response_Master_Incident " & _
                        "SET Time_First_Unit_Arrived = '" & cArrivedTime & "'," & _
                            "Elapsed_Enroute2FirstAtScene='" & cCalcElapsedTime & "' " & _
                            "WHERE ID=" & nRMIID & " "
    End If
    
    cSQL = cSQL & "UPDATE Response_Vehicles_Assigned SET Time_ArrivedAtScene='" & cArrivedTime & "'," & _
                        "ArrivedAtScene_Performed_By='" & cArrivedBy & "'"
                        
    If lIsFirstUpdate Then
        cSQL = cSQL & ",Fixed_Time_ArrivedAtScene='" & cArrivedTime & "'"
    End If
    
    If glIsCommand Then
                        
        cSQL = cSQL & ",TimeStatusChanged='" & cTimeStatusChanged & "'," & _
                        "Status_ID = " & Trim(nStatusID)
    End If
    
    cSQL = cSQL & " WHERE ID=" & nRVAID & " IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
    
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateRMIRVA_ATSCENE (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function
' NOTE: If more than 1 patient is being transported, we need to insert one record for each
' patient
Function MDTUpdateRTR_DEPARTSCENE(nRMIID As Long, nRVAID As Long, nPatientInfoID As Long, _
                                    cPhone As String, _
                                    cMapInfo As String, cAssistedBy As String, _
                                    cTimeDepartScene As String, cDepartBy As String, _
                                    cFixedTimeDepartScene As String, cCity As String, _
                                    cLocationName As String, cAddress As String, _
                                    cApartment As String, nDestLatitude As Long, _
                                    nDestLongitude As Long, cTimePickupPromised As String, _
                                    cState As String, cPostalCode As String, _
                                    cCounty As String, cTransportProtocol As String, _
                                    cTransportMode As String, cBuilding As String, nNumPatients As Integer)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant
Dim x As Integer

    On Error GoTo ErrorHandler
    
    cSQL = "BEGIN TRANSACTION "
    
    For x = 1 To nNumPatients
        
        cSQL = cSQL & "INSERT INTO Response_Transports (Patient_Info_ID,Phone,Map_Info,Assisted_By,Vehicle_Assigned_ID," & _
                    "Time_Depart_Scene,Departure_Performed_By,Fixed_Time_Depart_Scene,City,Location_Name," & _
                    "Address,Apartment,Latitude,Longitude,Time_PickupPromised,State,Postal_Code,County," & _
                    "Transport_Protocol,Transport_Mode,Master_Incident_ID,Building,Odometer_At_Scene) " & _
                "VALUES ("
                
        If Val(nPatientInfoID) = 0 Then
            cSQL = cSQL & "NULL"
        Else
            cSQL = cSQL & Trim(nPatientInfoID)
        End If
        
        cSQL = cSQL & ",'" & cPhone & "','" & cMapInfo & "','" & cAssistedBy & "'," & nRVAID & "," & _
                    "'" & cTimeDepartScene & "','" & cDepartBy & "','" & cFixedTimeDepartScene & "'," & _
                    "'" & cCity & "','" & cLocationName & "','" & cAddress & "','" & cApartment & "'," & _
                    "'" & Trim(nDestLatitude) & "','" & Trim(nDestLongitude) & "','" & cTimePickupPromised & "'," & _
                    "'" & cState & "','" & cPostalCode & "','" & cCounty & "','" & cTransportProtocol & "'," & _
                    "'" & cTransportMode & "'," & nRMIID & ",'" & cBuilding & "'," & Trim(gtTPCState.odometer) & ") "
                    
    Next x
                
    cSQL = cSQL & "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
        
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateRTR_DEPARTSCENE (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateRTR_DEPARTSCENE_Sched(nRTRID As Long, nRVAID As Long, _
                                    cPhone As String, _
                                    cMapInfo As String, cAssistedBy As String, _
                                    cTimeDepartScene As String, cDepartBy As String, _
                                    cFixedTimeDepartScene As String, cCity As String, _
                                    cLocationName As String, cAddress As String, _
                                    cApartment As String, nDestLatitude As Long, _
                                    nDestLongitude As Long, _
                                    cState As String, cPostalCode As String, _
                                    cCounty As String, cTransportProtocol As String, _
                                    cTransportMode As String, cBuilding As String, nNumPatients As Integer)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant
                                    
    On Error GoTo ErrorHandler

    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE Response_Transports " & _
                "SET Time_Depart_Scene = '" & cTimeDepartScene & "', " & _
                    "Departure_Performed_By = '" & cDepartBy & "', " & _
                    "Fixed_Time_Depart_Scene = '" & cFixedTimeDepartScene & "', " & _
                    "City = '" & cCity & "', " & _
                    "Location_Name = '" & cLocationName & "', " & _
                    "Address = '" & cAddress & "', " & _
                    "Apartment = '" & cApartment & "', " & _
                    "Latitude = '" & Trim(nDestLatitude) & "', " & _
                    "Longitude = '" & Trim(nDestLongitude) & "', " & _
                    "State = '" & cState & "', " & _
                    "Postal_Code = '" & cPostalCode & "', " & _
                    "County = '" & cCounty & "', " & _
                    "Phone = '" & cPhone & "', " & _
                    "Map_Info = '" & cMapInfo & "', " & _
                    "Transport_Protocol = '" & cTransportProtocol & "', " & _
                    "Transport_Mode = '" & cTransportMode & "', " & _
                    "Assisted_By = '" & cAssistedBy & "', " & _
                    "Building = '" & cBuilding & "', " & _
                    "Odometer_At_Scene = " & Trim(gtTPCState.odometer) & ", " & _
                    "Vehicle_Assigned_ID = " & Trim(nRVAID) & " " & _
            "WHERE ID = " & Trim(nRTRID) & " " & _
            "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
            
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateRTR_DEPARTSCENE_Sched (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
            
End Function
' NOTE: This will update all patient transport records (if there is more than 1)
Function MDTUpdateRTR_ATDESTINATION(nRMIID As Long, nRVAID As Long, _
                                    cTimeArriveDest As String, cArriveBy As String, _
                                    cFixedTimeArriveDest As String)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant
    
    On Error GoTo ErrorHandler
    
    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE Response_Transports SET Time_Arrive_Destination = '" & cTimeArriveDest & "'," & _
                "Fixed_Time_Arrive_Destination='" & cFixedTimeArriveDest & "'," & _
                "ArrivedAtDest_Performed_By='" & cArriveBy & "'," & _
                "Odometer_At_Destination=" & Trim(gtTPCState.odometer) & "," & _
                "Transport_Mileage=" & Trim(gtTPCState.odometer) & " - Odometer_At_Scene " & _
            "WHERE Master_Incident_ID = " & nRMIID & " AND Vehicle_Assigned_ID = " & nRVAID & " " & _
            "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
        
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateRTR_ATDESTINATION (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

' NOTE: This will update all patient transport records (if there is more than 1)
Function MDTUpdateRTR_DELAYEDAVAILABLE(nRMIID As Long, nRVAID As Long, _
                                    cTimeDelayAvail As String, cDelayAvailBy As String, _
                                    cFixedTimeDelayAvail As String)
Dim cSQL As String
Dim nRows As Long
Dim aArrayPut As Variant
Dim nID As Long

    On Error GoTo ErrorHandler

    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE Response_Transports SET Time_Delayed_Availability='" & cTimeDelayAvail & "'," & _
                "Fixed_Time_DelayedAvailability='" & cFixedTimeDelayAvail & "'," & _
                "DelayedAvailable_Performed_By='" & cDelayAvailBy & "' " & _
            "WHERE Master_Incident_ID = " & nRMIID & " AND Vehicle_Assigned_ID = " & nRVAID & " " & _
            "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"

    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    
    Exit Function

ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateRTR_DELAYEDAVAILABLE (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

'Function MDTUpdateRVA_STAGED(nRVAID As Long, cTimeStaged As String, _
'                                cStagedBy As String, cFixedTimeStaged As String, _
'                                cTimeStatusChanged As String, nStatusID As Long)
'Dim cSQL As String
'Dim nRows As Long
'' Dim cDateTime As String
'Dim nID As Long
'
'    On Error GoTo ErrorHandler
'
'
'    cSQL = "BEGIN TRANSACTION " & _
'            "UPDATE Response_Vehicles_Assigned SET Time_Staged='" & cTimeStaged & "'," & _
'                "Staging_Performed_By='" & cStagedBy & "',Fixed_Time_Staged='" & cFixedTimeStaged & "'"
'
'    If glIsCommand Then
'        cSQL = cSQL & ",TimeStatusChanged='" & cTimeStatusChanged & "',Status_ID=" & nStatusID
'    End If
'
'    cSQL = cSQL & " WHERE ID = " & nRVAID & " IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
'
'    nRows = goCADServer.UpdateSQL(cSQL, nID)
'
'    Exit Function
'
'ErrorHandler:
'
'    Call AVLDebug("ERROR - Function MDTUpdateRVA_STAGED (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)
'
'End Function
Function MDTUpdateRVA_DEPARTSCENE(nRVAID As Long, nNumberOfPatients As Integer, _
                                    cTimeStatusChanged As String, nStatusID As Long)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler

    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE Response_Vehicles_Assigned SET Number_Of_Victims_Seen=" & Trim(nNumberOfPatients) & "," & _
                    "Odometer_AtScene=" & Trim(gtTPCState.odometer)
            
    If glIsCommand Then
        cSQL = cSQL & ",TimeStatusChanged='" & cTimeStatusChanged & "',Status_ID=" & nStatusID
    End If
    
    cSQL = cSQL & " WHERE ID = " & nRVAID & " IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
    
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateRVA_DEPARTSCENE (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateRVA_ATDESTINATION(nRVAID As Long, cTimeStatusChanged As String, _
                                    nStatusID As Long)
Dim cSQL As String
Dim nRows As Long
Dim nID As Long
Dim aArrayPut As Variant

    On Error GoTo ErrorHandler

    If glIsCommand Then
        
        cSQL = "BEGIN TRANSACTION " & _
                "UPDATE Response_Vehicles_Assigned " & _
                    "SET TimeStatusChanged='" & cTimeStatusChanged & "',Status_ID=" & nStatusID & "," & _
                    "Odometer_BackInQtrs=" & Trim(gtTPCState.odometer) & " " & _
                "WHERE ID = " & nRVAID & " " & _
                "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
        
        nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
        
    End If

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateRVA_ATDESTINATION (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTUpdateRVA_DELAYEDAVAILABLE(nRVAID As Long, cTimeDelayAvail As String, _
                                    cDelayBy As String, cFixedTimeDelayAvail As String, _
                                    cTimeStatusChanged As String, nStatusID As Long)
Dim cSQL As String
Dim nRows As Long
Dim aArrayPut As Variant
Dim nID As Long

    On Error GoTo ErrorHandler

    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE Response_Vehicles_Assigned SET Time_Delayed_Availability='" & cTimeDelayAvail & "'," & _
                "Delayed_Available_Performed_By='" & cDelayBy & "',Fixed_Time_DelayedAvailable='" & cFixedTimeDelayAvail & "'"

    If glIsCommand Then
        cSQL = cSQL & ",TimeStatusChanged='" & cTimeStatusChanged & "',Status_ID=" & nStatusID
    End If

    cSQL = cSQL & " WHERE ID = " & nRVAID & " IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"

    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)

    Exit Function

ErrorHandler:

    Call AVLDebug("ERROR - Function MDTUpdateRVA_DELAYEDAVAILABLE (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTBuildXML244(nVehID As Long, nFromStatus As Long, nToStatus As Integer, _
                        nRMIID As Long, cTimeStamp As String, cSTNID As String) As String
Dim cXML As String

    On Error GoTo ErrorHandler

    cXML = "<Vehicle ID=" & Chr(34) & Trim(nVehID) & Chr(34) & ">"
    cXML = cXML & "<FromStatus>" & Trim(nFromStatus) & "</FromStatus>"
    cXML = cXML & "<ToStatus>" & Trim(nToStatus) & "</ToStatus>"
    cXML = cXML & "<Master_Incident_ID>" & Trim(nRMIID) & "</Master_Incident_ID>"
    cXML = cXML & "<TimeStamp>" & cTimeStamp & "</TimeStamp>"
    cXML = cXML & "<StationID>" & Trim(cSTNID) & "</StationID>"
    cXML = cXML & "</Vehicle>"
    
    MDTBuildXML244 = cXML

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTBuildXML244 (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTBuildXML245(nVehID As Long, nCurLat As Long, nCurLon As Long, cCurLocation As String, _
                        cCurCity As String, cLastAVLTime As String, _
                        nSpeed As Integer, cHeading As String, nAltitude As Integer) As String
    
    On Error GoTo ErrorHandler
    
' <Vehicle ID= "82">
' <Current_Lat>43653019</Current_Lat><Current_Lon>79377562</Current_Lon>
' <Current_Location>HO STM</Current_Location><Current_City>TORONTO</Current_City>
' <LastAVLUpdate>Feb 25 2002 19:20:21</LastAVLUpdate><Speed>0</Speed><Heading>   </Heading>
' <Altitude>0</Altitude>
' </Vehicle>

    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTBuildXML245 (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function MDTDemoMode(nMSTID As Long) As Boolean
    
    MDTDemoMode = False
    
    If gcDemoTrucks <> "" Then
        If InStr(1, gcDemoTrucks, Trim(nMSTID)) = 0 Then
            ' We didn't find it in the demo list, ignore
            MDTDemoMode = True
            Exit Function
        End If
    End If

End Function
Function MDTClearGlobalTypes()

    gtVEHInfo.ID = 0 'Long
    gtVEHInfo.Name = "" 'String
    gtVEHInfo.Current_Location = "" 'String
    gtVEHInfo.Current_City = "" 'String
    gtVEHInfo.Destination_Location = "" 'String
    gtVEHInfo.Destination_City = "" 'String
    gtVEHInfo.Status_ID = "" 'String
    gtVEHInfo.Status_Code = "" 'String
    gtVEHInfo.Current_Lat = 0 'Long
    gtVEHInfo.Current_Lon = 0 'Long
    gtVEHInfo.Destination_Lat = 0 'Long
    gtVEHInfo.Destination_Lon = 0 'Long
    gtVEHInfo.IsActive = False 'Boolean
    gtVEHInfo.MDTEnabled = False 'Boolean
    gtVEHInfo.AVL_ID = 0 'Long
    gtVEHInfo.UnitName_ID = "" 'String
    gtVEHInfo.CurrentDivision_ID = "" ' String
    gtVEHInfo.Master_Incident_ID = 0 ' Long
    gtVEHInfo.MST_MDT_ID = 0 ' Long
    gtVEHInfo.Last_Station_ID = "" ' String
    gtVEHInfo.UnitName = "" ' String
    gtVEHInfo.Post_Station_ID = "" ' String
    gtVEHInfo.Battalion_ID = "" ' String
    gtVEHInfo.Agency_ID = "" ' String
    gtVEHInfo.Jurisdiction_ID = "" ' String
    
    gtRMIInfo.ID = 0 'Long
    gtRMIInfo.Master_Incident_Number = "" 'String
    gtRMIInfo.Response_Date = "" 'String
    gtRMIInfo.Agency_Type = "" 'String
    gtRMIInfo.Jurisdiction = "" 'String
    gtRMIInfo.Division = "" 'String
    gtRMIInfo.Battalion = "" 'String
    gtRMIInfo.Station = "" 'String
    gtRMIInfo.Response_Area = "" 'String
    gtRMIInfo.Response_Time_Criteria = "" 'String
    gtRMIInfo.Response_Plan = "" 'String
    gtRMIInfo.Incident_Type = "" 'String
    gtRMIInfo.Problem = "" 'String
    gtRMIInfo.Priority_Number = "" 'String
    gtRMIInfo.Priority_Description = "" 'String
    gtRMIInfo.Location_Name = "" 'String
    gtRMIInfo.Address = "" 'String
    gtRMIInfo.Apartment = "" 'String
    gtRMIInfo.City = "" 'String
    gtRMIInfo.State = "" 'String
    gtRMIInfo.Postal_Code = "" 'String
    gtRMIInfo.County = "" 'String
    gtRMIInfo.Location_Type = "" 'String
    gtRMIInfo.Longitude = "" 'String
    gtRMIInfo.Latitude = "" 'String
    gtRMIInfo.Map_Info = "" 'String
    gtRMIInfo.Cross_Street = "" 'String
    gtRMIInfo.Time_First_Unit_Enroute = "" 'String
    gtRMIInfo.Time_First_Unit_Arrived = "" 'String
    gtRMIInfo.Elapsed_Assigned2FirstEnroute = "" 'String
    gtRMIInfo.Elapsed_Enroute2FirstAtScene = "" 'String
    gtRMIInfo.Determinant = "" 'String
    gtRMIInfo.ProQa_CaseNumber = "" 'String
    gtRMIInfo.ANI_Number = "" 'String
    gtRMIInfo.Confirmation_Number = "" 'String
    gtRMIInfo.Incident_Name = "" 'String
    gtRMIInfo.Building = "" 'String
    gtRMIInfo.Base_Response_Number = "" 'String
    
    gtRVAInfo.ID = 0 'Long
    gtRVAInfo.Master_Incident_ID = 0 ' Long
    gtRVAInfo.Agency_Type = "" ' String
    gtRVAInfo.Jurisdiction = "" ' String
    gtRVAInfo.Division = "" ' String
    gtRVAInfo.Battalion = "" ' String
    gtRVAInfo.Station = "" ' String
    gtRVAInfo.Radio_Name = "" ' String
    gtRVAInfo.Vehicle_ID = 0 ' Long
    gtRVAInfo.Time_Assigned = "" ' String
    gtRVAInfo.Time_Enroute = "" ' String
    gtRVAInfo.Time_ArrivedAtScene = "" ' String
    gtRVAInfo.Time_Staged = "" ' String
    gtRVAInfo.Time_Delayed_Availability = "" ' String
    gtRVAInfo.Elapsed_Assigned_2_Enroute = "" ' String
    gtRVAInfo.Elapsed_Enroute_2_Arrival = "" ' String
    gtRVAInfo.Fixed_Time_Enroute = "" ' String
    gtRVAInfo.Fixed_Time_ArrivedAtScene = "" ' String
    gtRVAInfo.Fixed_Time_Staged = "" ' String
    gtRVAInfo.Fixed_Time_DelayedAvailable = "" ' String
    gtRVAInfo.EnrouteToScene_Performed_By = "" ' String
    gtRVAInfo.ArrivedAtScene_Performed_By = "" ' String
    gtRVAInfo.Staging_Performed_By = "" ' String
    gtRVAInfo.Odometer_Enroute = "" ' String
    gtRVAInfo.Odometer_AtScene = "" ' String
    gtRVAInfo.Location_At_Assign_Time = "" ' String
    gtRVAInfo.Longitude_At_Assign_Time = "" ' String
    gtRVAInfo.Latitude_At_Assign_Time = "" ' String
    gtRVAInfo.Vehicle_Info_ID = "" ' String
    gtRVAInfo.Number_Of_Victims_Seen = "" ' String
    gtRVAInfo.Refusal_Number = "" ' String
    gtRVAInfo.Time_Avail_AtScene = "" ' String
    gtRVAInfo.Fixed_Time_Avail_AtScene = "" ' String
    gtRVAInfo.Avail_AtScene_Performed_By = "" ' String
    gtRVAInfo.Response_Number = "" ' String
    gtRVAInfo.Status_ID = "" ' String
    gtRVAInfo.TimeStatusChanged = "" ' String
    
    gtRTRInfo.ID = 0 ' Long
    gtRTRInfo.Master_Incident_ID = 0 ' Long
    gtRTRInfo.Vehicle_Assigned_ID = 0 ' Long
    gtRTRInfo.Location_Name = "" ' String
    gtRTRInfo.Address = "" ' String
    gtRTRInfo.Apartment = "" ' String
    gtRTRInfo.City = "" ' String
    gtRTRInfo.State = "" ' String
    gtRTRInfo.Postal_Code = "" ' String
    gtRTRInfo.County = "" ' String
    gtRTRInfo.Longitude = 0 ' Long
    gtRTRInfo.Latitude = 0 ' Long
    gtRTRInfo.Time_Depart_Scene = "" ' String
    gtRTRInfo.Time_Arrive_Destination = "" ' String
    gtRTRInfo.Fixed_Time_Depart_Scene = "" ' String
    gtRTRInfo.Fixed_Time_Arrive_Destination = "" ' String
    gtRTRInfo.Fixed_Time_DelayedAvailability = "" ' String
    gtRTRInfo.Departure_Performed_By = "" ' String
    gtRTRInfo.ArrivedAtDest_Performed_By = "" ' String
    gtRTRInfo.Odometer_At_Scene = "" ' String
    gtRTRInfo.Odometer_At_Destination = "" ' String
    gtRTRInfo.Transport_Mileage = "" ' String
    gtRTRInfo.Transport_Mode = "" ' String
    gtRTRInfo.Transport_Protocol = "" ' String
    gtRTRInfo.Assisted_By = "" ' String
    gtRTRInfo.Phone = "" ' String
    gtRTRInfo.Map_Info = "" ' String
    gtRTRInfo.Building = "" ' String
    gtRTRInfo.Time_PickupPromised = "" ' String

    gtTPCState.stateID = 0 ' Long
    gtTPCState.StateTime = "" ' String
    gtTPCState.userID = 0 ' Integer
    gtTPCState.JobID = 0 ' Long
    gtTPCState.State = 0 ' Integer
    gtTPCState.stateType = 0 ' Integer
    gtTPCState.stateName = "" ' String
    gtTPCState.Latitude = 0 ' Long
    gtTPCState.Longitude = 0 ' Long
    gtTPCState.bHasForm = False ' Boolean
    gtTPCState.formID = 0 ' Long
    gtTPCState.odometer = 0 ' Single
    gtTPCState.RunNumber = "" ' String
    gtTPCState.VehID = 0 ' Long
    gtTPCState.FormName = "" ' String
    
    gtCallForm.RunNumber = 0 ' Long
    gtCallForm.ResponseGrid = "" ' String
    gtCallForm.Priority = "" ' String
    gtCallForm.Address = "" ' String
    gtCallForm.City = "" ' String
    gtCallForm.LocationName = "" ' String
    gtCallForm.ZipCode = "" ' String
    gtCallForm.Apartment = "" ' String
    gtCallForm.ProblemType = "" ' String
    gtCallForm.PatientSSN = "" ' String
    gtCallForm.PatientName = "" ' String
    gtCallForm.PatientDOB = "" ' String
    gtCallForm.TranDestName = "" ' String
    gtCallForm.TranDestAddress = "" ' String
    gtCallForm.Hospital = "" ' String
    gtCallForm.NumOfPatients = 0 ' Integer
    gtCallForm.TransportMode = "" ' String
    gtCallForm.DestLocationCAD = "" ' String
    gtCallForm.DestAddress = "" ' String
    gtCallForm.DestCity = "" ' String
    gtCallForm.DestApartment = "" ' String
    gtCallForm.DestBuilding = "" ' String
    gtCallForm.DestPhone = "" ' String
    gtCallForm.DestZipCode = "" ' String
    gtCallForm.DestState = "" ' String
    gtCallForm.DestCounty = "" ' String
    gtCallForm.DestCrossStreet = "" ' String
    gtCallForm.DestLatitude = 0 ' Long
    gtCallForm.DestLongitude = 0 ' Long

End Function
Function MDTSendAtDestPage()
Dim nHandle As Integer
Dim cTempFile As String
Dim cNewFile As String
Dim cPersonnelIDs As String

    On Error GoTo ErrorHandler
    
    cPersonnelIDs = MDTGetRosteredIDs(gtVEHInfo.ID)

    cTempFile = FFCreateTempFile("MDT_", Left(gSystemDirectory, 2) & "\tritech\visicad\data\page")
    
    nHandle = FreeFile
    
    Open (cTempFile) For Append Access Write Lock Read Write As (nHandle)
    
    Print #(nHandle), "[Message Header]"
    Print #(nHandle), "AgencyTypeID="
    Print #(nHandle), "JurisdictionID=" & Trim(gtVEHInfo.Jurisdiction_ID) ' Trim(gtRMIInfo.Jurisdiction)
    Print #(nHandle), "Vendor_ID="
    Print #(nHandle), "PIN="
    Print #(nHandle), "Message_Priority="
    Print #(nHandle), "Date_Time=" & Format(Date, "MM/DD/YYYY") & " " & Format(Time(), "HH:NN:SS")
    Print #(nHandle), "VehicleID=" & Trim(gtVEHInfo.ID)
    Print #(nHandle), "UnitID="
    Print #(nHandle), "StationID="
    Print #(nHandle), "LocationID="
    Print #(nHandle), "LocationPersonnelID="
    Print #(nHandle), "PersonnelID=" & Trim(cPersonnelIDs)
    Print #(nHandle), "PagerGroupID="
    Print #(nHandle), "MessageType=17"
    Print #(nHandle), "ResponseMasterIncidentID=" & Trim(gtRMIInfo.ID)
    Print #(nHandle), "VehiclesAssignedID=" & Trim(gtRVAInfo.ID)
    Print #(nHandle), "DivisionID="
    Print #(nHandle), "PageLogID=0"
    Print #(nHandle), "MachineName=" & gcComputerName
    Print #(nHandle), "RequestedBy="
    Print #(nHandle), "ResponseComments_ID="
    Print #(nHandle), "QuickPage=0"
    Print #(nHandle), "[Message Text]"
    Print #(nHandle), "MessageText="
    
    Close (nHandle)
    
    cNewFile = Left(cTempFile, Len(cTempFile) - 3) & "APF"
    
    ' Rename the file
    Name (cTempFile) As (cNewFile)
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function MDTSendAtDestPage (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

