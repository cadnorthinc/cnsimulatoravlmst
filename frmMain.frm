VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "TruckPC AVL Interface"
   ClientHeight    =   4470
   ClientLeft      =   1395
   ClientTop       =   765
   ClientWidth     =   9825
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4470
   ScaleWidth      =   9825
   Begin MSComctlLib.StatusBar stbMain 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   9
      Top             =   4095
      Width           =   9825
      _ExtentX        =   17330
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   9878
            MinWidth        =   9878
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Text            =   "**"
            TextSave        =   "**"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            TextSave        =   "11:14 AM"
         EndProperty
      EndProperty
   End
   Begin VB.ListBox lstMessages 
      Height          =   255
      Left            =   60
      TabIndex        =   8
      Top             =   3900
      Width           =   5175
   End
   Begin MSWinsockLib.Winsock sktTCP 
      Left            =   9120
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4008
      Left            =   36
      TabIndex        =   0
      Top             =   36
      Width           =   9660
      _ExtentX        =   17039
      _ExtentY        =   7091
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   420
      TabCaption(0)   =   "AVL Activity"
      TabPicture(0)   =   "frmMain.frx":0442
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblHeader"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lstLog"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Status / Debug Messages"
      TabPicture(1)   =   "frmMain.frx":045E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lblUpSince"
      Tab(1).Control(1)=   "lblTotal"
      Tab(1).Control(2)=   "lblSaved"
      Tab(1).Control(3)=   "lstDebug"
      Tab(1).Control(4)=   "txtTPCClientDDE"
      Tab(1).Control(5)=   "sktServer(0)"
      Tab(1).Control(6)=   "tmrMessage"
      Tab(1).ControlCount=   7
      Begin VB.Timer tmrMessage 
         Left            =   -66480
         Top             =   480
      End
      Begin MSWinsockLib.Winsock sktServer 
         Index           =   0
         Left            =   -66000
         Top             =   480
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   393216
      End
      Begin VB.TextBox txtTPCClientDDE 
         Height          =   288
         Left            =   -74856
         TabIndex        =   7
         Text            =   "txtTPCClientDDE"
         Top             =   576
         Visible         =   0   'False
         Width           =   2856
      End
      Begin VB.ListBox lstDebug 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2760
         Left            =   -74892
         TabIndex        =   3
         Top             =   936
         Width           =   9444
      End
      Begin VB.ListBox lstLog 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3300
         Left            =   108
         TabIndex        =   1
         Top             =   576
         Width           =   9444
      End
      Begin VB.Label lblSaved 
         Caption         =   "Saved : 0"
         Height          =   192
         Left            =   -67116
         TabIndex        =   6
         Top             =   324
         Width           =   1596
      End
      Begin VB.Label lblTotal 
         Caption         =   "Processed : 0"
         Height          =   192
         Left            =   -69996
         TabIndex        =   5
         Top             =   324
         Width           =   2532
      End
      Begin VB.Label lblUpSince 
         Caption         =   "Up Since : DD/MMM/YYYY  HH:NN:SS"
         Height          =   192
         Left            =   -74820
         TabIndex        =   4
         Top             =   324
         Width           =   4692
      End
      Begin VB.Label lblHeader 
         Caption         =   "Time      AVL ID   Unit     Status   Latitude    Longitude   Cross Street"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   7.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   228
         Left            =   144
         TabIndex        =   2
         Top             =   360
         Width           =   9408
      End
   End
   Begin VB.Menu mnuEnvList 
      Caption         =   "&Environment"
      Begin VB.Menu mnuEnvProduction 
         Caption         =   "&Production"
      End
      Begin VB.Menu mnuEnvSimulation 
         Caption         =   "&Simulation"
      End
   End
   Begin VB.Menu mnuDebug 
      Caption         =   "&Debugging"
      Begin VB.Menu mnuDetailed 
         Caption         =   "&Detailed"
      End
   End
   Begin VB.Menu mnu_About 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long

Dim gProcessingTCP As Boolean
Dim gPartialTCPMessage As String
Dim gReceivedWhileProcessing As String
Dim gPriorityMsgCount As Long

Const LVS_EX_FULLROWSELECT = &H20
Const LVM_FIRST = &H1000
Const LVM_GETEXTENDEDLISTVIEWSTYLE = LVM_FIRST + &H37
Const LVM_SETEXTENDEDLISTVIEWSTYLE = LVM_FIRST + &H36
Const K_AVLUPDATE_VEHID = 5

Private Sub Form_Load()

    gSocket = 0

    frmMain.Caption = FFBuildAppCaption()
        
    frmMain.lblHeader.Caption = FFPadR("Time", 10) & AVLFormatLogLine("AVL ID", "Unit", "Status", "Latitude", "Longitude", "Cross Street")
    
    frmMain.lblUpSince = "Up Since : " & Format(Date, "DD/MMM/YYYY") & "  " & Format(Time(), "HH:NN:SS")
    
    frmMain.lstMessages.Visible = False
    frmMain.tmrMessage.Enabled = False
    frmMain.tmrMessage.Interval = 200                           '   every 1/5th of a second, check for a message to process

    mnuDetailed.Checked = gDetailedDebugging
    
    Call AVLDebug("Detailed Debugging " & IIf(gDetailedDebugging, "ON", "OFF"), True)
    
    Call StartListening
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
    Call StopListening
    
    Select Case UnloadMode
    Case vbFormControlMenu
    
        Call AVLShutdown
    
    Case vbAppWindows       ' Windows is shutting down/restarting
    
        Call AVLShutdown("Windows Shutdown/Restart", False)
    
    Case vbAppTaskManager   ' Task manager is closing the app
        
        Call AVLShutdown("Task Manager Kill", False)
        
    Case Else
    
        Call AVLShutdown
    
    End Select

End Sub

Private Sub mnu_About_Click()
    Load frmAbout

    ' Here we set the information that the user will see in the about box.
    ' If it's a "free" application, you can set the AppLicenceBrief to whatever
    ' you want, something like "Compliments of CAD North Inc."
    
    frmAbout.AppLicenceBrief = "CAD North Simulator AVL-MDT Component"

    ' Set the information the the user will see when viewing licence details.
    ' If there is no licence info (for a free program), then do not set
    ' AppLicenceFull.
'    cLicence = cLicence & "LICENCE INFORMATION" & vbCrLf & vbCrLf
'    cLicence = cLicence & "    COMPANY : " & tLicenceInfo.Company & vbCrLf
'    cLicence = cLicence & "    CONTACT : " & tLicenceInfo.Name & vbCrLf
'    cLicence = cLicence & "    EXPIRES : " & IIf(Trim(tLicenceInfo.Expire) = "", "<never>", tLicenceInfo.Expire) & vbCrLf
'
'    cLicence = cLicence & "  LIC. TYPE : " & tLicenceInfo.TypeString & vbCrLf
'
'    frmAbout.AppLicenceFull = cLicence
'
    ' If you want the About box to close automatically after a specified
    ' number of seconds, uncomment the next line (example shows 10 seconds)
    ' frmAbout.AboutAutoCloseTime = 10

    ' Now we show the about box, making it modal to the application
    
    frmAbout.Show vbModal

End Sub

Private Sub mnuDetailed_Click()
    Dim clsINIFile As clsIniTFiles
    
    If mnuDetailed.Checked Then
        mnuDetailed.Checked = False
    Else
        mnuDetailed.Checked = True
    End If
    
    gDetailedDebugging = mnuDetailed.Checked
    
    Set clsINIFile = New clsIniTFiles

    clsINIFile.InitFileName = gSystemDirectory & "\Simulator.ini"

    clsINIFile.SectionName = "AVL Settings"
    
    Call clsINIFile.WriteKey("DetailedDebugging", IIf(gDetailedDebugging, "1", "0"))
    
    Call AVLDebug("Detailed Debugging: " & IIf(gDetailedDebugging, "ON", "OFF"), True)
    
End Sub

Private Sub mnuEnvProduction_Click()
    Dim clsINIFile As clsIniTFiles
    Dim cLogMsg As String
    
    mnuEnvSimulation.Checked = False
    mnuEnvProduction.Checked = True
    
    gProductionEnvironment = mnuEnvProduction.Checked

    Set clsINIFile = New clsIniTFiles

    clsINIFile.InitFileName = gSystemDirectory & "\Simulator.ini"
    clsINIFile.SectionName = "SystemDefaults"
    Call clsINIFile.WriteKey("PRODUCTION_ENV", IIf(gProductionEnvironment, "1", "0"))

    cLogMsg = "[SETUP CHANGE] -- Environment = " & IIf(gProductionEnvironment, "PRODUCTION - AVL and MDT OVERRIDES DISABLED", "SIMULATION - AVL and MDT OVERRIDES ENABLED")
    Call AVLDebug(cLogMsg, True)
End Sub

Private Sub mnuEnvSimulation_Click()
    Dim clsINIFile As clsIniTFiles
    Dim cLogMsg As String
    
    mnuEnvProduction.Checked = False
    mnuEnvSimulation.Checked = True

    gProductionEnvironment = mnuEnvProduction.Checked

    Set clsINIFile = New clsIniTFiles

    clsINIFile.InitFileName = gSystemDirectory & "\Simulator.ini"
    clsINIFile.SectionName = "SystemDefaults"
    Call clsINIFile.WriteKey("PRODUCTION_ENV", IIf(gProductionEnvironment, "1", "0"))

    cLogMsg = "[SETUP CHANGE] -- Environment = " & IIf(gProductionEnvironment, "PRODUCTION - AVL and MDT OVERRIDES DISABLED", "SIMULATION - AVL and MDT OVERRIDES ENABLED")
    Call AVLDebug(cLogMsg, True)
End Sub

Private Sub sktTCP_Connect()

    ' sktTCP.SendData Chr(2) & Chr(200) & UCase(sktTCP.LocalHostName) & "_VisiSnoop" & Chr(3)
    glTCPConnected = True

End Sub

Private Sub sktServer_ConnectionRequest(Index As Integer, ByVal requestID As Long)
    Call AVLDebug("Incoming connection from " & sktServer(Index).RemoteHostIP & ":" & sktServer(Index).RemotePort, gDetailedDebugging)
    If Index = 0 Then
        gSocket = gSocket + 1
        Load sktServer(gSocket)
        sktServer(gSocket).Accept requestID
        Call AVLDebug("Connected to Simulator: " & sktServer(gSocket).RemoteHostIP & " on socket" & Str(gSocket), gDetailedDebugging)
    End If
    frmMain.tmrMessage.Enabled = True           '   start checking for messages to process
End Sub

Private Sub sktServer_SendComplete(Index As Integer)
'    Call AVLDebug("sktServer" & Str(Index) & ": SendComplete event")
End Sub

Private Sub sktServer_SendProgress(Index As Integer, ByVal bytesSent As Long, ByVal bytesRemaining As Long)
'    Call AVLDebug("sktServer" & Str(Index) & ": SendProgress event - " & Str(bytesSent) & ", " & Str(bytesRemaining))
End Sub

Private Sub sktServer_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Dim sTemp As String
    Dim aMessage As Variant
    Dim msgLen As Integer
    Dim sReceived As String
    Dim sMessageBuffer As String
    Dim cASCII As String
    Dim n As Integer, i As Integer
    Dim aAVLMDTMessage As Variant
    
    On Error GoTo TCPError
    
    sktServer(Index).GetData sReceived              '   individual messages are formatted: <ASCII 2><Message Body><ASCII 3>
                                                    '   sReceived will contain one or more messages and may contain partial
                                                    '   messages in the first and last positions, so we have to test for them
                                                    
    gPartialTCPMessage = gPartialTCPMessage & sReceived     '   prepend any messages already received
    
    frmMain.stbMain.Panels(3).Text = Str(bytesTotal)        '   update size received

    Call AVLDebug("[DEBUG] DataArrival =" & Str(bytesTotal), gDetailedDebugging)

    If Not gProcessingTCP Then
        
        gProcessingTCP = True
    
        sMessageBuffer = gPartialTCPMessage                     '   prepend any partial message left over from last time
        gPartialTCPMessage = ""
        
        aMessage = Split(sMessageBuffer, Chr(vcIP_EOM))        '   split buffer into separate messages at the termination character ASCII 3
                                                        '   the last element of this array will either be blank or contain a partial message
                                                        '   so skip it during the loop and save it for later
        Do While UBound(aMessage, 1) > 0
        
            For n = 0 To UBound(aMessage) - 1
                msgLen = Len(aMessage(n))
                aMessage(n) = Right(aMessage(n), msgLen - 1)    '   strip off the leading chr(2) character
                
                Call AVLDebug("[Receiving] " & aMessage(n), True)
        
                aAVLMDTMessage = Split(aMessage(n), "|")
                
                Select Case aAVLMDTMessage(0)
                    Case "0001"         '   AVL Update Message
                        gAVLUpdateHash(Val(aAVLMDTMessage(K_AVLUPDATE_VEHID))) = aMessage(n)            '   insert the AVL update into the hash table where nVehID is the index into the table
                        '   frmMain.lstMessages.AddItem aMessage(n) & Chr(3) & Format(Now, "MMM DD YYYY HH:NN:SS")
                    Case Else           '   Any other message is a High Priority Message
                        frmMain.lstMessages.AddItem aMessage(n) & Chr(3) & Format(Now, "MMM DD YYYY HH:NN:SS")                      '   add the priority message
                        '   gPriorityMsgCount = gPriorityMsgCount + 1                                                                   '   to the end of the priority message in the queue
                End Select
                
                '   DoEvents
                
            Next n
        
            sMessageBuffer = aMessage(n) & gPartialTCPMessage
            gPartialTCPMessage = ""
            
            aMessage = Split(sMessageBuffer, Chr(vcIP_EOM))
            
        Loop
        
        If UBound(aMessage, 1) >= 0 Then
            gPartialTCPMessage = aMessage(UBound(aMessage, 1))
        Else
            gPartialTCPMessage = ""
        End If
        
        gProcessingTCP = False
    
    Else            '   we've received data while we are still in the process loop (during a recursive call to DataArrival)
    
        Call AVLDebug("[DEBUG] Receiving data in sktServer while gProcessingTCP = True. Buffer size = " & Len(gPartialTCPMessage), gDetailedDebugging)
        
    End If
    
    '   **Change NOTE**:    This design may cause messages to be handled out of sequence in a busy system.
    '                       Especially when the possibility exists for the event handler to be called again
    '                       before it has finished processing the earlier set of messages.
    '
    '                       Elimination of all DoEvents that are subordinate to this procedure will ensure that
    '                       messages are handled in sequence and there is no possibility of having code executed
    '                       re-entrantly, but this is not an efficient means of managing the interprocess messaging.
    '
    '              11-11-03 Changed architecture to include a listbox to buffer messages from the Sim Manager.
    '                       Messages are added to the listbox and the tmrMessage timer routine fires every
    '                       1/10th of a second to process any messages that have been received. As messages are
    '                       processed, they are deleted from the listbox and then the processing statistics are
    '                       updated.
    '
    '              02-10-12 Changed architecture to use a hash table to store the AVL update messages for processing.
    '                       Each row in the hash table represents a vehicle and holds the most recent AVL update from
    '                       that vehicle (old, unprocessed messages are overwritten by more recently received messages).
    '                       While this discards AVL updates, it allows the AVL interface to maintain pace during very heavy
    '                       loading.
    '
    '                       *NOTE* This new method of message handling will not loose messages or process messages out of order.
    '
    
    Exit Sub
    
TCPError:

    gProcessingTCP = False
    
    Call AVLDebug("[ERROR] in sktServer_DataArrival: " & Str(Err.Number) & " - " & Err.Description, True)
    Call AVLDebug("         bytestotal = " & Str(bytesTotal), True)
    Call AVLDebug("          sReceived = " & sReceived, True)
    Call AVLDebug(" gPartialTCPMessage = " & gPartialTCPMessage, True)

End Sub

Private Sub tmrMessage_Timer()
    Dim aMessage As Variant
    Dim bQueueReport As Boolean
    Dim n As Long
                                                                                '   Timer routine called every 1/10th of a second to process messages
    frmMain.tmrMessage.Enabled = False                                          '   no recursive calls allowed

    Call AVLDebug("[DEBUG] Processing HIGH Priority updates List Count =" & Str(frmMain.lstMessages.ListCount))
    Do While frmMain.lstMessages.ListCount > 0                                  '   if there are high priority messages to process, do them first
    
        aMessage = Split(frmMain.lstMessages.List(0), Chr(3))                   '   prepare the oldest message for processing
        frmMain.lstMessages.RemoveItem (0)                                      '   and delete it from the buffer (listbox control)
        
        If gPriorityMsgCount > 0 Then                                           '   if gPriorityMsgCount > 0 there are priority messages to process
            gPriorityMsgCount = gPriorityMsgCount - 1
        End If
        
                                                                                '   Need to change from using a listbox as a message queue to using an actual managed queue
                                                                                '   because there is a limit of 32K queued messages
                                                                                '   (which should way be more than enough, actually, but apparently not) :(
        
        Call ProcessAVLMessage(aMessage(0))                                     '   process next queued priority the message
        
        frmMain.stbMain.Panels(2).Text = Str(frmMain.lstMessages.ListCount) _
                & "/" & Trim(Str(DateDiff("S", CDate(aMessage(1)), Now)))       '   update panel stats
                
        '    If Not bQueueReport Then
        '        Call AVLDebug("[QUEUE] " & Str(frmMain.lstMessages.ListCount) _
        '            & "/" & Trim(Str(DateDiff("S", CDate(aMessage(1)), Now))), gDetailedDebugging)
        '        bQueueReport = True
        '    End If
                
        DoEvents
        
    Loop

    Call AVLDebug("[DEBUG] Processing AVL updates starting at Hash Counter =" & Str(gAVLUpdateCount))
    Do While gAVLUpdateCount <= UBound(gAVLUpdateHash, 1) And frmMain.lstMessages.ListCount = 0         '   process the AVL update list until we are through it or there is a HP message to process
        If gAVLUpdateHash(gAVLUpdateCount) <> "" Then
            Call bsiAVLProcessData(gAVLUpdateHash(gAVLUpdateCount))                            '   process the AVL update for this hash entry
            gAVLUpdateHash(gAVLUpdateCount) = ""                                               '   and delete it from the hash table
        End If
        gAVLUpdateCount = gAVLUpdateCount + 1
        DoEvents
    Loop
    
    If gAVLUpdateCount > UBound(gAVLUpdateHash, 1) Then gAVLUpdateCount = LBound(gAVLUpdateHash, 1)
    
    frmMain.tmrMessage.Enabled = True                                           '   re-enable timer
    
End Sub

Private Sub ProcessAVLMessage(ByVal aMessage As String)
    Dim aAVLMDTMessage As Variant
    Dim aParams() As String
    Dim nVehID As Long
    Dim nIncID As Long
    Dim nHashIndex As Long
    
    On Error GoTo ErrHandler
    
    aAVLMDTMessage = Split(aMessage, "|")
    
    Select Case aAVLMDTMessage(0)
        Case "0001"         '   AVL Update Message
            nHashIndex = Val(aAVLMDTMessage(K_AVLUPDATE_VEHID))                     '   look up the most current AVL message for this unit in the hash table
            If gAVLUpdateHash(nHashIndex) <> "" Then                                '   if a message is available, then process it
                Call AVLDebug("[AVL MESSAGE] >" & gAVLUpdateHash(nHashIndex) & "<", gDetailedDebugging)
                Call bsiAVLProcessData(gAVLUpdateHash(nHashIndex))
                gAVLUpdateHash(nHashIndex) = ""                                     '   and delete it from the hash table
            End If
        Case "0002"         '   STATUS Update Message
            Call AVLDebug("[STATUS MESSAGE] >" & aMessage & "<", gDetailedDebugging)
            Call bsiMDTProcessMDTButtonPush(aAVLMDTMessage)
        Case "0003"         '   INCIDENT Update Message
            Call AVLDebug("[INCIDENT MESSAGE] >" & aMessage & "<", gDetailedDebugging)
            Call INCMain(aAVLMDTMessage)
        Case "0004"         '   TRANSPORT Update Message
            Call AVLDebug("[TRANSPORT MESSAGE] >" & aMessage & "<", gDetailedDebugging)
            Call bsiMDTProcessMDTButtonPush(aAVLMDTMessage)
        Case Format(EV_E911, "0000")         '   Create E911 ANIALI from Response_Master_Incident Message
            Call AVLDebug("[RMI ANIALI MESSAGE] >" & aMessage & "<", gDetailedDebugging)
            Call bsiMDTProcessANIALI(aAVLMDTMessage(1), False)
        Case Format(EV_ANIALI, "0000")         '   Create E911 ANIALI from Response_ANIALI Message
            Call AVLDebug("[RA ANIALI MESSAGE] >" & aMessage & "<", gDetailedDebugging)
            Call bsiMDTProcessANIALI(aAVLMDTMessage(1), True)
        Case "0007"         '   Set All Unites in Agency Off Duty (Part of Scenario Initialization)
            Call AVLDebug("[INIT UNITS (OffDuty) MESSAGE] >" & aMessage & "<", True)
            Call bsiSetUnitsOffDuty(Val(aAVLMDTMessage(1)))
        Case "0008"         '   Set All Unites in Agency ON Duty (Part of Scenario Initialization)
            Call AVLDebug("[INIT UNITS (ONDuty) MESSAGE] >" & aMessage & "<", True)
            Call bsiSetUnitsOnDuty(aAVLMDTMessage)
        Case "0009"         '   Create Incidents and Unit Assignments at Startup
            Call AVLDebug("[INIT INCIDENTS MESSAGE] >" & aMessage & "<", True)
            Call bsiCreateActivityContext(CDate(aAVLMDTMessage(1)))
            Call bsiCreateStartUpContext(CDate(aAVLMDTMessage(1)), CDate(aAVLMDTMessage(2)))
            Call AVLDebug("bsiCreateStartUpContext: Sending NP_BSI_INITIALIZATION_COMPLETE", True)
            If gUseXMLPort Then
                Call goCADServer.SendMessageXML(NP_BRIMAC_MESSAGE_CLASS, Format(NP_BSI_INITIALIZATION_COMPLETE, "0000"), False, False)
            Else
                Call goCADServer.SendMessage(NP_BRIMAC_MESSAGE_CLASS, Format(NP_BSI_INITIALIZATION_COMPLETE, "0000"), False, False)
            End If
        Case "0012"         '   assign unit to incident (message:   RMIncID;VehID)
            Call AVLDebug("[LOADTEST] > Assign Unit to Incident - >" & aMessage & "<", gDetailedDebugging)
            aParams = Split(aAVLMDTMessage(1), ";")
            nIncID = Val(aParams(0))
            nVehID = Val(aParams(1))
            Call AssignUnits(nIncID, nVehID, Now)
        Case "0013"         '   Set Unit to Available Status (message:  VehID)
            Call AVLDebug("[LOADTEST] > Set Unit to Available Status - >" & aMessage & "<", gDetailedDebugging)
            nVehID = aAVLMDTMessage(1)
            Call SetUnitAvailableStatus(nVehID)
        Case "0014"         '   Post Unit to Home Station   (message:   VehID)
            Call AVLDebug("[LOADTEST] > Post Unit to Home Station - >" & aMessage & "<", gDetailedDebugging)
            nVehID = aAVLMDTMessage(1)
            Call SetUnitAssignedToPost(nVehID)
        Case "9998"         '   Stop Message
            Call AVLDebug("[STOP MESSAGE] > Closing sockets - Stop Message Received from Simulator", gDetailedDebugging)
            Call StopListening
        Case "9999"         '   Kill Message
            Call AVLDebug("[KILL MESSAGE] > Shutting Down - Kill Message Received from Simulator", True)
            Call Unload(frmMain)
        Case Else           '   Something Else Message
            Call AVLDebug("[ODD THING] >" & aMessage & "< Unknown Message", True)
    End Select
    
    Exit Sub
    
ErrHandler:
    Call AVLDebug("ERROR in ProcessAVLMessage >" & aMessage & "<", True)
    Call AVLDebug("ERROR: " & Str(Err.Number) & " " & Err.Description, True)
End Sub

Private Sub StartListening()
    gSocket = 0
    sktServer(gSocket).LocalPort = Val(gSimulatorListenPort)
    Call AVLDebug("Listening on " & sktServer(gSocket).LocalIP & ":" & sktServer(gSocket).LocalPort, True)
    sktServer(gSocket).Listen
End Sub

Private Sub StopListening()
    frmMain.tmrMessage.Enabled = False          '   stop trying to process messages
    While gSocket >= 0
        Call AVLDebug("Closing sktServer" & Str(gSocket) & " on " & sktServer(gSocket).LocalIP & ":" & sktServer(0).LocalPort, gDetailedDebugging)
        sktServer(gSocket).Close
        If gSocket > 0 Then Unload sktServer(gSocket)
        gSocket = gSocket - 1
    Wend
End Sub

Private Function GetWinSockState(WinSock As Object)
    ' sckClosed             0 Default. Closed
    ' sckOpen               1 Open
    ' sckListening          2 Listening
    ' sckConnectionPending  3 Connection pending
    ' sckResolvingHost      4 Resolving host
    ' sckHostResolved       5 Host resolved
    ' sckConnecting         6 Connecting
    ' sckConnected          7 Connected
    ' sckClosing            8 Peer is closing the connection
    ' sckError              9 Error
    Select Case WinSock.State
        Case sckClosed                  ' 0
            GetWinSockState = "Closed"
        Case sckOpen                    ' 1
            GetWinSockState = "Open"
        Case sckListening               ' 2
            GetWinSockState = "Listening"
        Case sckConnectionPending       ' 3
            GetWinSockState = "Connection pending"
        Case sckResolvingHost           ' 4
            GetWinSockState = "Resolving host"
        Case sckHostResolved            ' 5
            GetWinSockState = "Host resolved"
        Case sckConnecting              ' 6
            GetWinSockState = "Connecting"
        Case sckConnected               ' 7
            GetWinSockState = "Connected"
        Case sckClosing                 ' 8
            GetWinSockState = "Peer is closing the connection"
        Case sckError                   ' 9
            GetWinSockState = "Error"
    End Select
End Function

