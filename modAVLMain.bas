Attribute VB_Name = "modAVLMain"
Option Explicit

Declare Function GetCurrentProcessId& Lib "kernel32" ()
Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Public goCADServer As Object
Public hConnect As Long

Public gcComputerName As String

Public gDetailedDebugging As Boolean
Public gUseXMLPort As Boolean
Public gForceAVL As Boolean
Public gForceMST As Boolean
Public gProductionEnvironment As Boolean

'   Public Const INIPath = "Q:\Tritech\VisiCAD\Data\System"
Public Const C_INIPath = "C:\Tritech\VisiCAD\Data\System"
Public Const Q_INIPath = "Q:\Tritech\VisiCAD\Data\System"

Public gSocket As Integer
Public gSimulatorListenPort As Integer
Public gDefaultTransPriority As String
Public gSystemDBName As String
Public gStreetsDBName As String
Public gSystemDirectory As String

Public CitiesList() As typCity

Public Type typCity
    CityID As Long
    CityName As String
End Type

Public Type typCoord
    Lat As Double
    Long As Double
End Type

Public Type typStreetInfo
    Name As String
    Speed As Integer
    OneWay As Integer
End Type

Public Type typCrossStreetInfo
    CrossStreet As String
    City As String
    County As String
    State As String
    Street1 As typStreetInfo
    Street2 As typStreetInfo
End Type

Public Type typVEHInfo
    ID As String
    Name As String
    Current_Location As String
    Current_City As String
    Destination_Location As String
    Destination_City As String
    Status_ID As String
    Status_Code As String
    Current_Lat As String
    Current_Lon As String
    Destination_Lat As String
    Destination_Lon As String
    IsActive As String
    AVLEnabled As String
    MDTEnabled As String
    AVL_ID As String
    PrimaryResource_ID As String
    UnitName_ID As String
    CurrentDivision_ID As String
    Master_Incident_ID As String
    MST_MDT_ID As String
    Last_Station_ID As String
    LastAVLUpdate As Date
    UnitName As String
    Post_Station_ID As String
    Battalion_ID As String
    Agency_ID As String
    Jurisdiction_ID As String
End Type

Public glTPCBusy As Boolean
Public glTCPConnected As Boolean
Public glTCPError As Boolean
Public gcTCPError As String

Public gnTotMsgs As Long
Public gnSaved As Long
Public glIs110 As Boolean
Public glIsCD As Boolean
Public glIsNewFOURVersion As Boolean

Public gcSQLServerDT As String
Public gcSQLServerCAD As String

Public gcDTSQLLogin As String
Public gcDTSQLPassword As String

Public gcUDPSwitchAddress As String
Public gnUDPSwitchPort As Integer
Public gcNoAVLStatus As String
Public glLogActivity As Boolean

Public ghErrorLog As Integer
Public gcCADVersion As String

Public hTPCConnect As Long

Public gAVLUpdateHash() As String
Public gAVLUpdateCount As Long


Sub Main()
Dim x As Integer
Dim cLogLine As String
Dim nSize As Long
Dim cLogMsg As String
Dim cLogPath As String                      '   BM 2021.10.28 - Added alternate log path to accommodate clients with restrictions on writing to application folder
Dim cLogName As String
Dim vi As VersionInfo
Dim cDefaultUID As String
Dim cDefaultPWD As String

Dim clsINIFile As clsIniTFiles

    Dim bContinue As Boolean
    Dim aCommandArgs() As String
    Dim aArg() As String
    Dim cShareLoc As String
    
    bContinue = False
    aCommandArgs = Split(Command$, " ")
    
    If UBound(aCommandArgs, 1) >= 0 Then                '   there are commandline arguments
        aArg = Split(aCommandArgs(0), "=")
        If Trim(UCase(aArg(0))) = "SHARE" Then
            cShareLoc = Trim(UCase(aArg(1)))
            If cShareLoc = "Q:" Then
                gSystemDirectory = Q_INIPath
            ElseIf cShareLoc = "C:" Then
                gSystemDirectory = C_INIPath
            Else                                        '   incorrect parameter specified on command line
                Call MsgBox("TriTech directory structure location missing." & vbCrLf & "Specify SHARE=Q: or SHARE=C: as a command line parameter." & vbCrLf & "Simulator cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP")
            End If
        Else                                            '   incorrect parameter specified on command line
            Call MsgBox("TriTech directory structure location missing." & vbCrLf & "Specify SHARE=Q: or SHARE=C: as a command line parameter." & vbCrLf & "Simulator cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP")
        End If
    ElseIf UBound(aCommandArgs, 1) < 0 Then             '   there are NO commandline arguments
        '   get them from the local share.ini
        Set clsINIFile = New clsIniTFiles
        clsINIFile.InitFileName = App.Path & "\Share.INI"
        clsINIFile.SectionName = "MAIN"
        cShareLoc = clsINIFile.ReadKey("SHARE", "Q:")
        If cShareLoc = "Q:" Then
            gSystemDirectory = Q_INIPath
        ElseIf cShareLoc = "C:" Then
            gSystemDirectory = C_INIPath
        Else                                        '   incorrect parameter specified on command line
            Call MsgBox("TriTech directory structure location missing." & vbCrLf & "Specify SHARE=Q: or SHARE=C: as a command line parameter." & vbCrLf & "Simulator cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP")
        End If
    Else
        gSystemDirectory = Q_INIPath                    '   default to Q: share
    End If
    
    If Dir(gSystemDirectory & "\System.INI") <> "" Then
        bContinue = True
    Else                                            '   incorrect parameter specified on command line
        Call MsgBox("TriTech directory structure location missing." & vbCrLf & "Specify SHARE=Q: or SHARE=C: as a command line parameter." & vbCrLf & "Simulator cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP")
    End If
    
    Set clsINIFile = New clsIniTFiles

    clsINIFile.InitFileName = gSystemDirectory & "\Simulator.ini"

    clsINIFile.SectionName = "AVL Settings"
    cDefaultUID = clsINIFile.ReadKey("DefaultUID", "tritech")
    cDefaultPWD = clsINIFile.ReadKey("DefaultPWD", "europa")
    
'    clsINIFile.SectionName = "Simulator"
    
    gSimulatorListenPort = clsINIFile.ReadKey("AVLPort", "19191")
    
    clsINIFile.SectionName = "SystemDefaults"
    gDefaultTransPriority = clsINIFile.ReadKey("DefaultTransportPriority", "")
    gUseXMLPort = IIf(clsINIFile.ReadKey("UseXMLPort", "1") = "1", True, False)
    gForceAVL = IIf(clsINIFile.ReadKey("ForceAVL", "0") = "1", True, False)
    gForceMST = IIf(clsINIFile.ReadKey("ForceMST", "0") = "1", True, False)
    gProductionEnvironment = IIf(clsINIFile.ReadKey("PRODUCTION_ENV", "0") = "1", True, False)       '   defaults to Simulation Environment
    frmMain.mnuEnvProduction.Checked = gProductionEnvironment
    frmMain.mnuEnvSimulation.Checked = Not frmMain.mnuEnvProduction.Checked
    
    clsINIFile.SectionName = "LOGGING"                      '   BM 2021.10.28 - Alternate log path
    cLogPath = clsINIFile.ReadKey("ALTERNATEPATH", "***")
    If cLogPath = "***" Then                                '   In case alternate path is not set, use default log path
        cLogPath = App.Path & "\logs"
    End If
    
    If App.PrevInstance Then
        End
    End If

    glTCPConnected = False
    glTCPError = False
    glIsCommand = True

    nSize = 25
    gcComputerName = String(nSize + 1, 0)
    Call GetComputerName(gcComputerName, nSize)
    gcComputerName = Left(gcComputerName, nSize)
    
    Set vi = New VersionInfo

    vi.FileName = cShareLoc & "\tritech\visicad\bin\visicad.exe"

    gcCADVersion = Trim(vi.FileVersion)
    
    '   cLogName = App.Path & "\logs\" & App.EXEName & "_ERROR - " & Format(Now, "YYYYMMDD") & ".LOG"
    cLogName = cLogPath & "\" & App.EXEName & "_ERROR - " & Format(Now, "YYYYMMDD") & ".LOG"
    
    ' Open error log
    ghErrorLog = FFLogOpen(cLogName, True, False)
    
    cLogMsg = "* START PROGRAM * -- " & FFBuildAppCaption(True) & "  [" & gcComputerName & "]  (VISICAD.EXE v" & gcCADVersion & ")"
    
    Call FFLogWrite(ghErrorLog, cLogMsg)
    
    Call ConfirmDSNConfiguration

    Call FFADOSetDefaults("142.142.163.150", "SYSTEM_923", cDefaultUID, cDefaultPWD)

    cLogMsg = "[STARTUP] -- ADO Defaults Set -- UID:=" & cDefaultUID & ",    PWD:=" & cDefaultPWD
    
    Call FFLogWrite(ghErrorLog, cLogMsg)
    
    DoEvents

    Call AVLReadSettings
    
    cLogMsg = "[STARTUP] -- UseXMLPort = " & IIf(gUseXMLPort, "TRUE", "FALSE")
    Call AVLDebug(cLogMsg, True)
    cLogMsg = "[STARTUP] -- ForceAVL = " & IIf(gProductionEnvironment, "FALSE", IIf(gForceAVL, "TRUE", "FALSE"))
    Call AVLDebug(cLogMsg, True)
    cLogMsg = "[STARTUP] -- ForceMST = " & IIf(gProductionEnvironment, "FALSE", IIf(gForceMST, "TRUE", "FALSE"))
    Call AVLDebug(cLogMsg, True)
    cLogMsg = "[STARTUP] -- Environment = " & IIf(gProductionEnvironment, "PRODUCTION - AVL and MDT OVERRIDES DISABLED", "SIMULATION - AVL and MDT OVERRIDES ENABLED")
    Call AVLDebug(cLogMsg, True)
    
    Call LoadCities
    
    glIs110 = Check110Version()
    glIsCD = CheckVersion()
    glIsNewFOURVersion = CheckNewFOURVersion()
    
    Call InitAVLHashTable
    
    ' Call FFFifoInit
    
'    glIs110 = True
'    glIsCD = True

    cLogMsg = "[STARTUP] -- Loading cnCADServer"
    
    Call FFLogWrite(ghErrorLog, cLogMsg)

    DoEvents

    Set goCADServer = CreateObject("CNCADServer.CNCADServerLite")
    Call goCADServer.Connect(GetCurrentProcessId(), (App.EXEName))
    
    cLogMsg = "[STARTUP] -- cnCADServer Loaded"
    
    Call FFLogWrite(ghErrorLog, cLogMsg)
    
    DoEvents

    Load frmMain
    
    frmMain.Show
    
    DoEvents
    
    Call AVLDebug("VisiCAD SQL Server = " & gcSQLServerCAD, True)
    Call AVLDebug("DriverTech SQL Server = " & gcSQLServerDT, True)
    Call AVLDebug("AVL Updates ignored for status = " & gcNoAVLStatus, True)
    Call AVLDebug("AVL Activity Logging = " & Trim(glLogActivity), True)
    
End Sub

Private Sub LoadCities()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Integer
    Dim cLogMsg As String
    
    cSQL = "Select City_ID, City_Name from Cities"
    
    cLogMsg = "[STARTUP] -- Loading Cities - query='" & cSQL & "'"
    
    Call FFLogWrite(ghErrorLog, cLogMsg)

    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gStreetsDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
        ReDim CitiesList(0 To nRows - 1)
        
        For n = 0 To nRows - 1
            CitiesList(n).CityID = aSQL(0, n)
            CitiesList(n).CityName = aSQL(1, n)
        Next n
    End If
    
    cLogMsg = "[STARTUP] -- Loading Cities - found" & Str(nRows) & "."
    
    Call FFLogWrite(ghErrorLog, cLogMsg)

    Call AVLDebug("[LoadCities] Loaded " & Trim(Str(nRows)) & " city names.", gDetailedDebugging)

End Sub

Private Function CheckVersion() As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    
    On Error GoTo ErrHandler
    
    CheckVersion = True
    
    cSQL = "select COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH " _
            & " From INFORMATION_SCHEMA.Columns " _
            & " WHERE TABLE_NAME = 'response_master_incident' " _
            & " and Column_Name = 'HomeSectorID' "
    
    '    cSQL = "Select CurrentSectorID from Response_Master_Incident"
    
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows <= 0 Then
        CheckVersion = False
        Call AVLDebug("CheckVersion: Non-Controlling Dispatch System detected", gDetailedDebugging)
    Else
        Call AVLDebug("CheckVersion: Controlling Dispatch System detected", gDetailedDebugging)
    End If
    
    Exit Function

ErrHandler:
        CheckVersion = False
        Call AVLDebug("ERROR in CheckVersion: Non-Controlling Dispatch System detected", True)
End Function

Private Function Check110Version() As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    
    On Error GoTo ErrHandler
    
    Check110Version = True
    
    cSQL = "select COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH " _
            & " From INFORMATION_SCHEMA.Columns " _
            & " WHERE TABLE_NAME = 'response_master_incident' " _
            & " and Column_Name = 'HomeSectorID' "
    
    '    cSQL = "Select UnreadIncident from Response_Master_Incident"
    
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows <= 0 Then
        Check110Version = False
        Call AVLDebug("CheckVersion: Pre-1.10 or 1.10 Non-Controlling Dispatch System detected", gDetailedDebugging)
    Else
        Call AVLDebug("CheckVersion: Command 1.10 Controlling Dispatch System detected", gDetailedDebugging)
    End If
    
    Exit Function

ErrHandler:
        Check110Version = False
        Call AVLDebug("ERROR in CheckVersion: Pre-1.10 or 1.10 Non-Controlling Dispatch System detected", True)
End Function

Private Function CheckNewFOURVersion() As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    
    On Error GoTo ErrHandler
    
    CheckNewFOURVersion = True
    
    cSQL = "select COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH " _
            & " From INFORMATION_SCHEMA.Columns " _
            & " WHERE TABLE_NAME = 'response_master_incident' " _
            & " and Column_Name = 'DispatchLevel' "
    
    '    cSQL = "Select CurrentSectorID from Response_Master_Incident"
    
    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows <= 0 Then
        CheckNewFOURVersion = False
        Call AVLDebug("CheckNewFOURVersion: Old Version FOUR Dispatch System detected", gDetailedDebugging)
    Else
        Call AVLDebug("CheckNewFOURVersion: Newer Version FOUR Dispatch System detected", gDetailedDebugging)
    End If
    
    Exit Function

ErrHandler:
        CheckNewFOURVersion = False
        Call AVLDebug("ERROR in CheckNewFOURVersion: Old Version FOUR Dispatch System detected", True)
End Function

Private Function FindCity(ByVal nCityID As Long) As String
    Dim n As Long
    
    On Error GoTo FindCity_Error
    
    FindCity = "UNKNOWN"
    
    For n = 0 To UBound(CitiesList, 1)
        If CitiesList(n).CityID = nCityID Then
            FindCity = CitiesList(n).CityName
            Exit For
        End If
    Next n
    
    Exit Function
    
FindCity_Error:
    Call AVLDebug("ERROR in Function FindCity(" & Str(nCityID) & "): " & Err.Number & " - " & Err.Description, True)
End Function

Private Sub InitAVLHashTable()
    Dim aSQL As Variant
    Dim cSQL As String
    Dim nRows As Long
    
    cSQL = "SELECT MIN(ID), MAX(ID), COUNT(*) from Vehicle"                    '   find the lowest and highest ID so that we can use the ID as the index into the AVL Hash table

    nRows = FFADOGet(cSQL, aSQL, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
        ReDim gAVLUpdateHash(Val(aSQL(0, 0)) To Val(aSQL(1, 0)))
        Call FFLogWrite(ghErrorLog, "Setting AVL Hash Table to " & Str(Val(aSQL(1, 0)) - Val(aSQL(0, 0))) & " rows.")
        gAVLUpdateCount = LBound(gAVLUpdateHash, 1)
    Else
        Call FFLogWrite(ghErrorLog, "[ERROR] Unable to read from Vehicle Table in Sub InitAVLHashTable.")
    End If
    
End Sub

Public Function AVLDebug(cText As String, _
                                    Optional lWriteLog As Boolean = False, _
                                    Optional lCritical As Boolean = False)
                                    
    
    If lWriteLog Then
        Call FFAddToListBox(frmMain.lstDebug, (cText), , True, 100)
        Call FFLogWrite(ghErrorLog, (cText))
    End If
    
    If lCritical Then
        Call MsgBox("CRITICAL ERROR" & vbCrLf & _
                        cText & vbCrLf & _
                        "Program stopping, refer to Error Log for details.")
        Call AVLShutdown
    End If

End Function

Public Function AVLShutdown(Optional cReason As String, Optional lUnloadForm As Boolean = True)
Dim cLogMsg As String

    On Error Resume Next
    
    cLogMsg = "* END PROGRAM * -- " & IIf(cReason <> "", "(" & cReason & ") ", "") & vbCrLf & vbCrLf
    
    Call AVLDebug(cLogMsg, True)

    frmMain.sktTCP.Close
    DoEvents
    
    If lUnloadForm Then
        Unload frmMain
    End If
    
    Set frmMain = Nothing

    End

End Function

Public Function AVLLookupIntersect(tCoord As typCoord, nAVLID As Long) As typCrossStreetInfo
Dim cSQL As String
Dim tHigh As typCoord
Dim tLow As typCoord
Dim nRows As Long
Dim aArray As Variant
Dim tSeg1 As typSegment
Dim tSeg2 As typSegment
Dim tLineTest As typSegment
Dim tPoint As typPoint
Dim nReturn As Integer
Dim nLength As Double
Dim nShortest As Double
Dim nStreet1 As Integer
Dim nStreet2 As Integer
Dim nSave As Integer
Dim tResult As typCrossStreetInfo
Dim x As Integer
Dim y As Integer
Dim cErrMsg As String
Dim nTicks1 As Single

    On Error GoTo ErrorHandler

    nShortest = 10 ^ 8
    nStreet1 = -1
    nStreet2 = -1

    '
    ' Create our search boundrys
    ' 1500 is equivilant to ~600ft (183meters)
    '
    tHigh.Lat = tCoord.Lat + 1500
    tHigh.Long = tCoord.Long + 1500
    tLow.Lat = tCoord.Lat - 1500
    tLow.Long = tCoord.Long - 1500
    
    ' Original query
    ' SELECT SEG.ID,SEG.From_Latitude,SEG.From_Longitude,SEG.To_Latitude,SEG.To_Longitude,
    ' SEG.SpeedLimit,SEG.One_Way_Direction,BLC.Block_ID,BLC.From_Address_Right,
    ' BLC.From_Address_Left,BLC.To_Address_Right,BLC.To_Address_Left,STR.Street_ID,
    ' STR.Street_Name,STR.City_ID,STR.County_ID,STR.State_ID From Segments SEG (NOLOCK),
    ' Blocks BLC (NOLOCK),Streets STR (NOLOCK) Where (((SEG.From_Latitude between
    ' 43668778 and 43671778) and (SEG.From_Longitude between 79499889 and 79502889)) or
    ' ((SEG.To_Latitude between 43668778 and 43671778) and (SEG.To_Longitude between
    ' 79499889 and 79502889))) and SEG.ID = BLC.Segment_ID AND BLC.Street_ID = STR.Street_ID
    ' AND SEG.SegmentType NOT IN (51,58,60)
    '
    ' Returns an array(col,row), columns are:
    ' 0 = SEG.ID
    ' 1 = SEG.From_Latitude
    ' 2 = SEG.From_Longitude
    ' 3 = SEG.To_Latitude
    ' 4 = SEG.To_Longitude
    ' 5 = SEG.SpeedLimit
    ' 6 = SEG.One_Way_Direction
    ' 7 = BLC.Block_ID
    ' 8 = BLC.From_Address_Right
    ' 9 = BLC.From_Address_Left
    ' 10 = BLC.To_Address_Right
    ' 11 = BLC.To_Address_Left
    ' 12 = STR.Street_ID
    ' 13 = STR.Street_Name
    ' 14 = STR.City_ID
    ' 15 = STR.County_ID
    ' 16 = STR.State_ID
    '
    ' We should make the SegmentType exclusions configurable by the user
    '
    cSQL = "SELECT SEG.ID, SEG.From_Latitude, SEG.From_Longitude, SEG.To_Latitude, SEG.To_Longitude, " & _
                "SEG.SpeedLimit, SEG.One_Way_Direction, BLC.Block_ID, BLC.From_Address_Right, " & _
                "BLC.From_Address_Left, BLC.To_Address_Right, BLC.To_Address_Left, STR.Street_ID, " & _
                "STR.Street_Name, STR.City_ID, STR.County_ID, STR.State_ID " & _
            "FROM Segments SEG (NOLOCK),Blocks BLC (NOLOCK),Streets STR (NOLOCK) " & _
            "WHERE (((SEG.From_Latitude BETWEEN " & Trim(tLow.Lat) & " AND " & Trim(tHigh.Lat) & ") " & _
                "AND (SEG.From_Longitude BETWEEN " & Trim(tLow.Long) & " AND " & Trim(tHigh.Long) & ")) " & _
                "OR ((SEG.To_Latitude BETWEEN " & Trim(tLow.Lat) & " AND " & Trim(tHigh.Lat) & ") " & _
                "AND (SEG.To_Longitude BETWEEN " & Trim(tLow.Long) & " AND " & Trim(tHigh.Long) & "))) " & _
                "AND SEG.ID = BLC.Segment_ID AND BLC.Street_ID = STR.Street_ID" ' AND SEG.SegmentType NOT IN (51,58,60)"
                
    ' Do the query
    cErrMsg = "Starting query #1"
    
    nTicks1 = Timer
    
    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gStreetsDBName, , , False, , ghErrorLog)
    
    nTicks1 = Timer - nTicks1
    
    nTicks1 = Timer
    
    ' Check for no records.  No records means no segments within boundries
    ' therefore no intersection
    
    Select Case nRows
    Case -1  ' Query had an error, most likely a timeout
        
        Call AVLDebug("ERROR - AVLLookupIntersect - SQL Error, probably timeout, see text above", True)
    
    Case 0    ' No segments returned
        
        Call AVLDebug("WARNING - AVLLookupIntersect - No rows returned for Lat=" & Trim(tCoord.Lat) & " Lon=" & Trim(tCoord.Long) & "  AVLID=" & Trim(nAVLID), True)
        tResult.CrossStreet = "Nothing Found"
    
    Case 1    ' If there is only 1 segment, then make it the cross street by itself
        
        cErrMsg = "One Row Returned"
        
        Debug.Print "1 row returned only"
        tResult.Street1.Name = aArray(13, 0)
        tResult.Street1.Speed = Val(aArray(5, 0))
        tResult.Street1.OneWay = Val(aArray(6, 0))
        tResult.CrossStreet = tResult.Street1.Name
        tResult.City = FindCity(aArray(14, 0))        '   this was a bug. It inserts the City_ID, not the city.
        tResult.County = aArray(15, 0)
        tResult.State = aArray(16, 0)
        
    Case Else  ' We have multiple segments
    
        cErrMsg = "Rows Returned:" & Str(nRows)
        
        ' Ok, now we loop through and find all points of intersection
        For x = 0 To nRows - 2
        
            ' Go forward only if street name isn't empty
            If Trim(aArray(13, x)) <> "" Then
            
                ' Here we may want to compute the distance of the passed coords to every
                ' segment by projecting the coords onto the segment, then computing the
                ' distance between the coords and the projected point.  The segment name
                ' of the closest segment SHOULD match one of the cross streets.  If it
                ' doesn't, then we should use the segment found here as the single street
                ' in the cross street.  See example2.gif for visual example.
                
            
                ' We save a street index here because we know index 'x' is
                ' not blank.  We only need this if there ARE rows returned
                ' but NO intersection found, so we default to a single street
                ' as the cross street.  Any non-blank will do, with this
                ' line, it will be the last non-blank in the rows returned
                nSave = x
            
                For y = x + 1 To nRows - 1
                
                    ' If street names match, don't compare them
                    ' (street can't intersect with itself)
                    ' Also check to make sure street name isn't empty by making
                    ' sure it is longer than 1 character (there are some '0' names
                    ' in the database)
                    If Trim(aArray(13, x)) <> Trim(aArray(13, y)) And Len(Trim(aArray(13, y))) > 1 Then
                
                        tSeg1.A.y = aArray(1, x) ' Latitude
                        tSeg1.A.x = aArray(2, x) ' Longitude
                        tSeg1.B.y = aArray(3, x) ' Lat
                        tSeg1.B.x = aArray(4, x) ' Long
                        
                        tSeg2.A.y = aArray(1, y) ' Latitude
                        tSeg2.A.x = aArray(2, y) ' Longitude
                        tSeg2.B.y = aArray(3, y) ' Lat
                        tSeg2.B.x = aArray(4, y) ' Long
                    
                        nReturn = FFIntersectLine(tSeg1, tSeg2, tPoint)
                        
                        ' Check to see if they intersect at all
                        If nReturn = 1 Or nReturn = 2 Then
                            
                            tLineTest.A.x = tPoint.x
                            tLineTest.A.y = tPoint.y
                            tLineTest.B.x = tCoord.Long
                            tLineTest.B.y = tCoord.Lat
                            
                            nLength = FFLengthOfLine(tLineTest)
                            
                            If nLength < nShortest Then
                                nStreet1 = x
                                nStreet2 = y
                                nShortest = nLength
                            End If
                            
                        End If
                        
                    End If
                    
                Next y
                
            End If
        
        Next x
        
        ' Check to make sure we have a valid intersection (Street2 is not -1)
        If nStreet2 <> -1 Then
        
            cErrMsg = "Found intersection - " & aArray(13, nStreet1) & "/" & aArray(13, nStreet2)
            
            tResult.Street1.Name = aArray(13, nStreet1)
            tResult.Street1.Speed = Val(aArray(5, nStreet1))
            tResult.Street1.OneWay = Val(aArray(6, nStreet1))
            tResult.Street2.Name = aArray(13, nStreet2)
            tResult.Street2.Speed = Val(aArray(5, nStreet2))
            tResult.Street2.OneWay = Val(aArray(6, nStreet2))
            
            ' Check to see which street has higher speed limit, list it first
            If tResult.Street1.Speed > tResult.Street2.Speed Then
                tResult.CrossStreet = tResult.Street1.Name & "/" & tResult.Street2.Name
                tResult.City = FindCity(aArray(14, nStreet1))        '   this was a bug. It inserts the City_ID, not the city.
                tResult.County = aArray(15, nStreet1)
                tResult.State = aArray(16, nStreet1)
            Else
                tResult.CrossStreet = tResult.Street2.Name & "/" & tResult.Street1.Name
                tResult.City = FindCity(aArray(14, nStreet2))        '   this was a bug. It inserts the City_ID, not the city.
                tResult.County = aArray(15, nStreet2)
                tResult.State = aArray(16, nStreet2)
            End If
            
        Else
        
            cErrMsg = Str(nRows) & " found, but no intersections"
            
            Debug.Print Str(nRows) & " found, but no intersections"
        
            ' By default, just pick the saved row as the intersection
            ' since everything else failed.
            tResult.Street1.Name = aArray(13, nSave)
            tResult.Street1.Speed = Val(aArray(5, nSave))
            tResult.Street1.OneWay = Val(aArray(6, nSave))
            tResult.CrossStreet = tResult.Street1.Name
            tResult.City = FindCity(aArray(14, nSave))        '   this was a bug. It inserts the City_ID, not the city.
            tResult.County = aArray(15, nSave)
            tResult.State = aArray(16, nSave)
            
        End If
            
    End Select
    
    AVLLookupIntersect = tResult
    
    nTicks1 = Timer - nTicks1
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - AVLLookupIntersect - Err#" & Trim(Err.Number) & " - " & Err.Description & " - " & cErrMsg, True)

End Function

Function AVLUpdateVehicle(nAVLID As Long, cCurLat As String, cCurLon As String, tCross As typCrossStreetInfo, cUpdateString As String)
Dim tVEHInfo As typVEHInfo
Dim cSQL As String
Dim nRows As Long
Dim aArray As Variant
Dim aArrayPut As Variant
Dim cDateTime As String
Dim cLogLine As String
Dim nID As Long
Dim cErrMsg As String
Dim cXML As String
Dim aAVLUpdate() As String

    On Error GoTo ErrorHandler
    
'                   0      1      2        3         4          5          6       7
'   cUpdateString = MsgNum|AVL_ID|Latitude|Longitude|UpdateTime|Vehicle_ID|Heading|Speed
    aAVLUpdate = Split(cUpdateString, "|")

    cDateTime = Format(CDate(aAVLUpdate(4)), "MM/DD/YYYY HH:NN:SS")

    cSQL = "SELECT VEH.ID, VEH.Name, VEH.Current_Location, VEH.Current_City, " & _
                "VEH.Destination_Location, VEH.Destination_City, VEH.Status_ID, " & _
                "VEH.Current_Lat, VEH.Current_Lon, VEH.Destination_Lat, VEH.Destination_Lon, " & _
                "VEH.IsActive, VEH.AVLEnabled, VEH.MDTEnabled, VEH.PrimaryResource_ID, " & _
                "VEH.UnitName_ID, VEH.CurrentDivision_ID, VEH.HomeDivisionID, VEH.Master_Incident_ID, " & _
                "VEH.MST_MDT_ID, VEH.Last_Station_ID, VEH.LastAVLUpdate, " & _
                "VEH.Post_Station_ID, UNN.Code, UNN.AgencyTypeID, STA.Code " & _
                "FROM Vehicle VEH " & _
                "LEFT JOIN Unit_Names UNN ON VEH.UnitName_ID = UNN.ID " & _
                "LEFT JOIN Status STA ON STA.ID = VEH.Status_ID " & _
                "WHERE " & IIf(gProductionEnvironment, "VEH.AVL_ID", IIf(gForceAVL, "VEH.ID", "VEH.AVL_ID")) & " = " & Trim(nAVLID)

    cErrMsg = "Starting query #1"
    
    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows <= 0 Then
        Call AVLDebug("WARNING : No rows returned for AVLID = " & Trim(nAVLID), gDetailedDebugging)
        Exit Function
    End If

    cErrMsg = "Starting assignment #1"
    tVEHInfo.ID = aArray(0, 0)
    tVEHInfo.Name = aArray(1, 0)
    tVEHInfo.Current_Location = aArray(2, 0)
    tVEHInfo.Current_City = aArray(3, 0)
    tVEHInfo.Destination_Location = aArray(4, 0)
    tVEHInfo.Destination_City = aArray(5, 0)
    tVEHInfo.Status_ID = aArray(6, 0)
    tVEHInfo.Status_Code = aArray(24, 0)
    tVEHInfo.Current_Lat = cCurLat  ' aArray(7, 0)
    tVEHInfo.Current_Lon = cCurLon  ' aArray(8, 0)
    tVEHInfo.Destination_Lat = aArray(9, 0)
    tVEHInfo.Destination_Lon = aArray(10, 0)
    tVEHInfo.IsActive = aArray(11, 0)
    tVEHInfo.AVLEnabled = aArray(12, 0)
    tVEHInfo.MDTEnabled = aArray(13, 0)
    tVEHInfo.AVL_ID = nAVLID
    tVEHInfo.PrimaryResource_ID = aArray(14, 0)
    tVEHInfo.UnitName_ID = aArray(15, 0)
    
    If aArray(16, 0) <> "" Then
        tVEHInfo.CurrentDivision_ID = aArray(16, 0)
    Else
        tVEHInfo.CurrentDivision_ID = aArray(17, 0)
    End If
    
    tVEHInfo.Master_Incident_ID = aArray(18, 0)
    tVEHInfo.MST_MDT_ID = aArray(19, 0)
    tVEHInfo.Last_Station_ID = aArray(20, 0)
    cErrMsg = "Asgn LastAVLUpdate [" & aArray(21, 0) & "]"
    If IsDate(aArray(21, 0)) Then
        tVEHInfo.LastAVLUpdate = aArray(21, 0)
    Else
        tVEHInfo.LastAVLUpdate = cDateTime
    End If
    tVEHInfo.Post_Station_ID = aArray(22, 0)
    tVEHInfo.UnitName = aArray(23, 0)
    tVEHInfo.Agency_ID = aArray(24, 0)
    

    ' Possibly read the Agency, Jurisdiction and Division tables at
    ' start of program, eliminating the need for this query on what
    ' is almost certainly static data
    cSQL = "SELECT JUR.ID, AGN.ID FROM Division DIV " & _
            "LEFT JOIN Jurisdiction JUR ON JUR.ID = DIV.JurisdictionID " & _
            "LEFT JOIN AgencyTypes AGN ON AGN.ID = JUR.AgencyID " & _
            "WHERE DIV.ID = " & Trim(tVEHInfo.CurrentDivision_ID)
    
    cErrMsg = "Starting query #2"
    
    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    tVEHInfo.Jurisdiction_ID = aArray(0, 0)
    tVEHInfo.Agency_ID = aArray(1, 0)
    
    ' Figure out a way to get rid of this query too, maybe add it to first query
    cSQL = "SELECT STN.BattalionID " & _
                "FROM StationBattalionList STN " & _
                "WHERE STN.StationID = " & Trim(tVEHInfo.Post_Station_ID)

    cErrMsg = "Starting query #3"
    
    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)

    tVEHInfo.Battalion_ID = aArray(0, 0)

    ' Should separate out the update to Vehicle from the Activity Log (AL) update
    ' The AL should be updated all the time, no matter what the vehicle status is,
    ' but we probably only want to update the vehicle if it's in a mobile state
    ' of some kind.
    cSQL = "BEGIN TRANSACTION " & _
            "UPDATE Vehicle SET LastAVLUpdate = '" & cDateTime & "', " & _
            "Current_Lat = " & tVEHInfo.Current_Lat & ", " & _
            "Current_Lon = " & tVEHInfo.Current_Lon & " WHERE ID = " & tVEHInfo.ID & " " & _
            "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
    
    cErrMsg = "Starting update #1"
    
    nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
    cErrMsg = "Finished update #1"
    
    ' Only update the AVL log if the INI file says so or we are running on the production system.
    If glLogActivity Or gProductionEnvironment Then
    
        '    cSQL = "BEGIN TRANSACTION INSERT INTO Activity_Log (Date_Time, Vehicle_ID, Radio_Name, " & _
        '                "Activity, Location, Comment, Personnel_ID, Latitude, " & _
        '                "Longitude, Master_Incident_id, Dispatcher_Init, " & _
        '                "Agency_ID, Jurisdiction_ID, Division_ID, Battalion_ID, " & _
        '                "Station_ID, Terminal, Radio_Code) " & _
        '            "VALUES ('" & cDateTime & "'," & tVEHInfo.ID & ",'" & tVEHInfo.UnitName & "'," & _
        '                "NULL,'" & Left(Replace(tCross.CrossStreet, "'", "''"), 40) & "',NULL,NULL," & _
        '                tVEHInfo.Current_Lat & "," & tVEHInfo.Current_Lon & ",NULL,NULL," & _
        '                tVEHInfo.Agency_ID & "," & tVEHInfo.Jurisdiction_ID & "," & _
        '                tVEHInfo.CurrentDivision_ID & "," & tVEHInfo.Battalion_ID & "," & tVEHInfo.Post_Station_ID & _
        '                ",'" & gcComputerName & "','" & tVEHInfo.UnitName & "') " & _
        '            "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
        '   AVL Table   -   replacing the update of the Activity_Log table
        '
        '    [ID] [int] IDENTITY(1,1) NOT NULL,
        '    [Vehicle_ID] [int] NOT NULL,
        '    [Date_Time] [datetime] NOT NULL,
        '    [Latitude] [int] NOT NULL,
        '    [Longitude] [int] NOT NULL,
        '    [Radio_Name] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
        '    [Location] [varchar](165) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        '    [Agency_ID] [int] NOT NULL,
        '    [Jurisdiction_ID] [int] NOT NULL,
        '    [Division_ID] [int] NOT NULL,
        '    [Master_Incident_ID] [int] NULL,
        '    [Radio_Code] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
        '    [Battalion_ID] [int] NOT NULL,
        '    [Station_ID] [int] NOT NULL,
        '    [VehicleName] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
        '    [Heading] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        '    [Speed] [tinyint] NULL,
        '    [Altitude] [int] NULL,
        '    [AVLQuality] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        '    [City] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        
        cSQL = "BEGIN TRANSACTION INSERT INTO AVL (Vehicle_ID, Date_Time, Latitude, " _
                & "Longitude, Radio_Name, Location, Agency_ID, Jurisdiction_ID, Division_ID, " _
                & "Master_Incident_id, Radio_Code, Battalion_ID, " _
                & "Station_ID, VehicleName, Heading, Speed, City) " _
            & "VALUES (" & tVEHInfo.ID & ",'" & cDateTime & "'," & tVEHInfo.Current_Lat & "," & tVEHInfo.Current_Lon & "," _
                & "'" & tVEHInfo.UnitName & "','" & Left(Replace(tCross.CrossStreet, "'", "''"), 40) & "'," _
                & tVEHInfo.Agency_ID & "," & tVEHInfo.Jurisdiction_ID & "," & tVEHInfo.CurrentDivision_ID & "," _
                & IIf(tVEHInfo.Master_Incident_ID <> "", tVEHInfo.Master_Incident_ID, "NULL") & ",'" & tVEHInfo.UnitName & "'," & tVEHInfo.Battalion_ID & "," _
                & tVEHInfo.Post_Station_ID & ",'" & tVEHInfo.Name & "','" & aAVLUpdate(6) & "'," & Int(Val(aAVLUpdate(7))) & ",'" & tVEHInfo.Current_City & "') " _
            & "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
        
        cErrMsg = "Starting update #2"
        
        nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
        
    End If
    
    If Trim(tCross.CrossStreet) <> "" Then
        
        cSQL = "BEGIN TRANSACTION UPDATE Vehicle SET Current_Location = '" & Left(Replace(tCross.CrossStreet, "'", "''"), 30) & "', " & _
                "Current_City = '" & Left(tCross.City, 20) & "', Current_Lat = " & tVEHInfo.Current_Lat & ", Current_Lon = " & tVEHInfo.Current_Lon & " " & _
                "WHERE ID = " & tVEHInfo.ID & " " & _
                "IF @@error = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION"
        
        cErrMsg = "Starting update #3"
        
        nRows = FFADOPut(cSQL, aArrayPut, nID, gcSQLServerCAD, gSystemDBName, , , False, False, ghErrorLog)
        
        If gUseXMLPort Then
            Call goCADServer.SendMessageXML(20, tVEHInfo.ID, False, False)
            Call goCADServer.SendMessageXML(245, bsiFormatXMLVehPosition(tVEHInfo), False, False)
        Else
            Call goCADServer.SendMessage(20, tVEHInfo.ID, False, False)
            Call goCADServer.SendMessage(245, bsiFormatXMLVehPosition(tVEHInfo), False, False)
        End If
        
        cLogLine = FFPadR(Format(Time(), "HH:NN:SS"), 10) & AVLFormatLogLine(IIf(gForceAVL, "V" & tVEHInfo.ID, tVEHInfo.AVL_ID), tVEHInfo.UnitName, tVEHInfo.Status_Code, cCurLat, cCurLon, tCross.CrossStreet)
        
        Call FFAddToListBox(frmMain.lstLog, cLogLine)
        
    End If
    
    Exit Function
        
ErrorHandler:

    Call AVLDebug("ERROR - AVLUpdateVehicle - Err#" & Trim(Err.Number) & " - " & Err.Description & " - " & cErrMsg, True)

End Function

Function AVLGetTruckPCInfo(nKey As Long, ByRef nLat As Double, ByRef nLon As Double) As Long
Dim cSQL As String
Dim aArray As Variant
Dim nRows As Long
Dim cMsg As String
Dim x As Integer

    On Error Resume Next
    
    gnTotMsgs = gnTotMsgs + 1
    
    frmMain.lblTotal.Caption = "Processed : " & Trim(gnTotMsgs)

    ' Changed .address to .addr   28/JUN/2002
    cSQL = "SELECT LOC.Addr, LOC.Time, LOC.Latitude, LOC.Longitude, STA.TruckNumber " & _
            "FROM Location LOC LEFT JOIN Status STA ON STA.virtualAddress = LOC.Addr " & _
            "WHERE LOC.gpsID = " & Trim(nKey)
            
    Call AVLDebug("[AVLGetTruckPCInfo] Start DT Query", gDetailedDebugging)
    
    nRows = FFADOGet(cSQL, aArray, gcSQLServerDT, "DriverTech", gcDTSQLLogin, gcDTSQLPassword, False, , ghErrorLog)
    
    Call AVLDebug("[AVLGetTruckPCInfo] Finish DT Query", gDetailedDebugging)
    
    Select Case nRows
    Case -1  ' Error
    
        nLat = 0
        nLon = 0
        AVLGetTruckPCInfo = -1
    
    Case 0
        
        Call AVLDebug("[AVLGetTruckPCInfo] No rows returned!", gDetailedDebugging)
        nLat = 0
        nLon = 0
        AVLGetTruckPCInfo = 0
    
    Case Else
    
        cMsg = "VAddress = " & aArray(0, 0) & "  Time = " & aArray(1, 0) & "  Lat = " & Trim(aArray(2, 0)) & "  Lon = " & Trim(aArray(3, 0))
    
        nLat = aArray(2, 0)
        nLon = aArray(3, 0)
        
        ' This is a fudge for now.  We really want the AVL_ID from the STA.TruckNumber field,
        ' but it's not populated yet.  So for now use last 3 digits of VAddress
        AVLGetTruckPCInfo = CLng(Right(aArray(0, 0), 3))
    
    End Select

End Function

Function AVLFormatLogLine(cAVLID As String, cUnitName As String, cUnitStatus As String, cLat As String, cLong As String, cCrossStreet As String) As String

    AVLFormatLogLine = FFPadR(cAVLID, 9) & FFPadR(cUnitName, 9) & FFPadR(cUnitStatus, 9) & _
                        FFPadR(cLat, 12) & FFPadR(cLong, 12) & FFPadR(cCrossStreet, 35)
    
End Function

Function AVLReadSettings() As Boolean
Dim clsINIFile As clsIniTFiles
Dim cFilename As String
Dim cLogMsg As String

    Set clsINIFile = New clsIniTFiles

    clsINIFile.InitFileName = gSystemDirectory & "\Simulator.ini"

    clsINIFile.SectionName = "AVL Settings"
    gcNoAVLStatus = clsINIFile.ReadKey("NoAVLStatus", "02")
    glLogActivity = IIf(clsINIFile.ReadKey("LogActivity", "0") = 1, True, False)
    gDetailedDebugging = IIf(clsINIFile.ReadKey("DetailedDebugging", "0") = 1, True, False)
    
'    clsINIFile.SectionName = "Simulator"
    
    gSimulatorListenPort = clsINIFile.ReadKey("AVLPort", "19191")
    
    clsINIFile.SectionName = "SystemDefaults"
    gDefaultTransPriority = clsINIFile.ReadKey("DefaultTransportPriority", "")
    gUseXMLPort = IIf(clsINIFile.ReadKey("UseXMLPort", "1") = "1", True, False)
    gForceAVL = IIf(clsINIFile.ReadKey("ForceAVL", "0") = "1", True, False)
    gForceMST = IIf(clsINIFile.ReadKey("ForceMST", "0") = "1", True, False)
    
    clsINIFile.InitFileName = gSystemDirectory & "\System.ini"

    clsINIFile.SectionName = "SystemPreferences"
    gcSQLServerCAD = clsINIFile.ReadKey("SQLServerName", "")
    gStreetsDBName = clsINIFile.ReadKey("StreetDBName", "")
    gSystemDBName = clsINIFile.ReadKey("SystemDBName", "")
    
    cLogMsg = "[STARTUP] -- AVL Settings;" & vbCrLf _
                & "    SQLServerName = " & gcSQLServerCAD & vbCrLf _
                & "    StreetDBName  = " & gStreetsDBName & vbCrLf _
                & "    SystemDBName  = " & gSystemDBName & vbCrLf
    
    Call FFLogWrite(ghErrorLog, cLogMsg)
    
    AVLReadSettings = True
    
    ' We check to see if the TriTech VisiCAD INI file exists, and if
    ' so, we grab the SQL server from there
'    cFilename = "Q:\tritech\visicad\data\system\system.ini"
'    If Len(Dir(cFilename)) <> 0 Then
'        clsINIFile.InitFileName = cFilename
'        clsINIFile.SectionName = "SystemPreferences"
'        gcSQLServerCAD = clsINIFile.ReadKey("SQLServerName", "")
'        AVLReadSettings = True
'    Else
'        MsgBox "ERROR - Cannot find" & vbCrLf & cFilename
'        AVLReadSettings = False
'    End If
    
End Function

Function AVLTCPConnect() As Boolean
'Dim clsFile As clsIniTFiles
'Dim nPort As Integer
'Dim cHost As String
'
'    Set clsFile = New clsIniTFiles
'
'    clsFile.InitFileName = App.Path & "\switch.ini"
'    clsFile.SectionName = "Connection"
'    nPort = Val(clsFile.ReadKey("serverPort", "1234"))  '6789
'    cHost = "127.0.0.1"   ' localhost
    
    If frmMain.sktTCP.State <> sckClosed Then
        frmMain.sktTCP.Close
        DoEvents
    End If
    
    glTCPConnected = False
    glTCPError = False
    ' frmMain.tmrConnect.Enabled = True
    
    Call AVLDebug("Connecting to " & gcUDPSwitchAddress & ":" & Trim(gnUDPSwitchPort), gDetailedDebugging)
        
    frmMain.sktTCP.RemoteHost = gcUDPSwitchAddress
    frmMain.sktTCP.RemotePort = gnUDPSwitchPort
    frmMain.sktTCP.Connect
    
    Do Until glTCPConnected Or glTCPError
        
        DoEvents
    
    Loop
    
    If glTCPError Then
    
        frmMain.sktTCP.Close
        DoEvents
        AVLTCPConnect = False
        
        frmMain.stbMain.Panels(2).Text = " NOT CONNECTED "
        
        Call AVLDebug(gcTCPError, True)
        
    Else
    
        AVLTCPConnect = True
        
        frmMain.stbMain.Panels(2).Text = " CONNECTED "
        
        Call AVLDebug("CONNECTED", gDetailedDebugging)
        
    End If
    
    'frmMain.tmrConnect.Enabled = False

End Function

Function bsiAVLProcessData(cUpdateString As String)
Dim cMsg As String
Dim cStatus As String
Dim x As Integer
Dim tCrossStreets As typCrossStreetInfo
Dim tCoord As typCoord
Dim nAVLID As Long
Dim nTicks1 As Single
Dim nTicks2 As Single
Dim nQueueSize As Integer
Dim aAVLUpdate() As String

    On Error GoTo ErrorHandler
    
'                   0      1      2        3         4          5          6       7
'   cUpdateString = MsgNum|AVL_ID|Latitude|Longitude|UpdateTime|Vehicle_ID|Heading|Speed
    aAVLUpdate = Split(cUpdateString, "|")

    nAVLID = Val(aAVLUpdate(1))
    
    If nAVLID > 0 Then
    
        ' Get the vehicle info
        cStatus = AVLGetVEHStatus(nAVLID)

        If InStr(1, gcNoAVLStatus, cStatus) <> 0 And cStatus <> "" Then
            cMsg = "Ignore AVL Update for AVLID = " & Trim(nAVLID) & "(" & cStatus & ") in (" & gcNoAVLStatus & ")"
            Call AVLDebug(cMsg, True)
            Exit Function
        End If

        tCoord.Lat = Int(Abs(Val(aAVLUpdate(2))) * 1000000)
        tCoord.Long = Int(Abs(Val(aAVLUpdate(3))) * 1000000)

        cMsg = "bsiAVLProcessData: Lookup AVLID = " & Trim(nAVLID) & " at " & Trim(tCoord.Lat) & ", " & Trim(tCoord.Long) & "   (TruckPC Key="         '   & Trim(nKey) & ")"
        Call AVLDebug(cMsg, gDetailedDebugging)
'
'        nTicks1 = Timer
        tCrossStreets = AVLLookupIntersect(tCoord, nAVLID)
'        nTicks1 = Timer - nTicks1
'
        cMsg = "bsiAVLProcessData: Update Vehicle - AVLID = " & Trim(nAVLID) & " at " & tCrossStreets.CrossStreet
        Call AVLDebug(cMsg, gDetailedDebugging)
'
'        nTicks2 = Timer
        Call AVLUpdateVehicle(nAVLID, (tCoord.Lat), (tCoord.Long), tCrossStreets, cUpdateString)
'        nTicks2 = Timer - nTicks2
        
'        Call AVLDebug("<DEBUG> Before DoEvents", True)
        DoEvents
'        Call AVLDebug("<DEBUG> After DoEvents", True)
    Else
    
    End If
    
    Exit Function
    
ErrorHandler:

    Call AVLDebug("ERROR - Function bsiAVLProcessData (" & Err.Source & "), #" & Trim(Err.Number) & " - " & Err.Description, True)

End Function

Function AVLGetVEHStatus(nAVLID As Long) As String
Dim nRows As Long
Dim cSQL As String
Dim aArray As Variant

    cSQL = "SELECT VEH.Status_ID " & _
                "FROM Vehicle VEH " & _
                "WHERE " & IIf(gProductionEnvironment, "VEH.AVL_ID", IIf(gForceAVL, "VEH.ID", "VEH.AVL_ID")) & " = " & Trim(nAVLID)
        
    nRows = FFADOGet(cSQL, aArray, gcSQLServerCAD, gSystemDBName, , , False, , ghErrorLog)
    
    If nRows > 0 Then
    
        AVLGetVEHStatus = Format(Val(aArray(0, 0)), "00")

    End If

End Function

Function bsiFormatXMLVehPosition(tVehUpd As typVEHInfo) As String

    bsiFormatXMLVehPosition = "<Vehicle ID=" & Chr(34) & Trim(tVehUpd.ID) & Chr(34) & "  ReadVehicleSent= " & Chr(34) & "-1" & Chr(34) & ">" _
                    & "<Current_Lat>" & Trim(tVehUpd.Current_Lat) & "</Current_Lat>" _
                    & "<Current_Lon>" & Trim(tVehUpd.Current_Lon) & "</Current_Lon>" _
                    & "<Current_Location>" & Trim(Replace(tVehUpd.Current_Location, "&", "&amp;")) & "</Current_Location>" _
                    & "<Current_City>" & Trim(tVehUpd.Current_City) & "</Current_City>" _
                    & "<LastAVLUpdate>" & Format(tVehUpd.LastAVLUpdate, "MMM dd YYYY HH:NN:SS") & "</LastAVLUpdate>" _
                    & "<Speed>0</Speed>" _
                    & "<Heading></Heading>" _
                    & "<Altitude>0</Altitude>" _
                    & "</Vehicle>"

End Function

Sub ConfirmDSNConfiguration()
    Dim fDirection As Integer
    Dim szDSN As String * 1024
    Dim cbDSNMax As Integer
    Dim pcbDSN As Integer
    Dim szDescription As String * 1024
    Dim cbDescriptionMax As Integer
    Dim pcbDescription As Integer
    Dim Item As String
    Dim i As Integer
    Dim En As rdoEnvironment
    Dim cLogMsg As String
    
    Set En = rdoEnvironments(0)
    
    fDirection = SQL_FETCH_NEXT
    cbDSNMax = 1023
    cbDescriptionMax = 1023
    
    cLogMsg = "[STARTUP] -- Machine ODBC Settings:" & vbCrLf

    While i = SQL_SUCCESS
        szDSN = String(1024, " ")
        szDescription = String(1024, " ")
        i = SQLDataSources(En.henv, fDirection, szDSN, _
            cbDSNMax, pcbDSN, szDescription, _
            cbDescriptionMax, pcbDescription)
        
        If InStr(1, szDSN, Chr(0)) > 0 Then
            cLogMsg = cLogMsg & "    " & Left(szDSN, InStr(1, szDSN, Chr(0)) - 1) & " - " & Left(szDescription, InStr(1, szDescription, Chr(0)) - 1) & vbCrLf
        End If

    Wend

    Call FFLogWrite(ghErrorLog, cLogMsg)

End Sub
